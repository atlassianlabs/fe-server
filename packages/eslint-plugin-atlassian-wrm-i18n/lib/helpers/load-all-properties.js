const fs = require('fs');

const filenamesThatExist = (providedFilenames) =>
  providedFilenames.filter((filename) => {
    if (!fs.existsSync(filename)) {
      console.warn(`Couldn't find the file: ${filename}`);
      return false;
    }

    return true;
  });

const splitKeyFromLine = (line) => {
  const separatorPositions = ['=', ':'].map((sep) => line.indexOf(sep)).filter((index) => index > -1);
  const splitIndex = Math.min(...separatorPositions);
  return line.substring(0, splitIndex).trim();
};

const addKey = (line, keys, filename) => {
  try {
    const key = splitKeyFromLine(line);
    if (key !== '') {
      keys.add(key);
    }
  } catch (e) {
    console.error(`Failed to parse a line when reading the properties. Filename: ${filename} Line: ${line}`, e);
  }
};

const isCommentLine = (line) => line.match(/^\s*[!#]/);

const readAllLinesInFile = (filename) => fs.readFileSync(filename).toString().split(/\r?\n/);

/**
 * @param {string[]} providedFilenames of .properties files to retrieve keys from
 * @returns {any[]} a list of all property keys
 */
const getAllPropertiesKeys = (providedFilenames) => {
  // Inspired by the java-properties node module: https://github.com/mattdsteele/java-properties/blob/master/src/index.ts
  // unfortunately couldn't use due to this long-standing bug: https://github.com/mattdsteele/java-properties/issues/25

  const keys = new Set();

  const uniqueFilenames = Array.from(new Set(providedFilenames));

  filenamesThatExist(uniqueFilenames).forEach((filename) => {
    try {
      const rawCommentlessLines = readAllLinesInFile(filename).filter((rawLine) => !isCommentLine(rawLine));

      for (let i = 0; i < rawCommentlessLines.length; i++) {
        // Join together key,property pairs that are broken up over multiple raw lines into a single virtual line
        let line = rawCommentlessLines[i];
        while (line.substring(line.length - 1) === '\\') {
          line = line.slice(0, -1);
          let nextLine = rawCommentlessLines[i + 1];
          line = line + nextLine.trim();
          i++;
        }

        addKey(line, keys, filename);
      }
    } catch (e) {
      console.error(`Failed to read: ${filename} when attempting to read all the property keys`, e);
    }
  });

  return Array.from(keys);
};

module.exports = {
  getAllPropertiesKeys,
};
