/**
 * @fileoverview Checks there's a matching i18n key in the specified properties files
 * @author Michael Kemp
 */
'use strict';

const { getI18nGetTextArguments, isAStringLiteral } = require('../common/i18n-get-text-argument');

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
  meta: {
    type: 'problem',
    docs: {
      description: "Checks there's a matching I18n key in the specified properties files",
      recommended: true,
      url: 'https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript/#limitations',
    },
    messages: {
      noMatchingKey: "Couldn't find a match for the I18n key: {{ i18nKey }} in the properties files",
    },
    hasSuggestions: false,
    fixable: null,
    schema: [
      {
        type: 'array',
        uniqueItems: true,
        items: {
          type: 'string',
        },
      },
    ],
  },

  create(context) {
    // variables should be defined here

    // If the properties files aren't configured, we should save the users some ESlint processing time
    if (!context.options || context.options.length < 1) {
      console.warn('"matching-i18n-key-in-properties-file" rule has not been configured');
      return;
    }

    //----------------------------------------------------------------------
    // Helpers
    //----------------------------------------------------------------------

    const i18nKeyIsPresent = (i18nKeys, i18nKey) => {
      // lazily convert to a set since that has a cost
      return new Set(i18nKeys).has(i18nKey);
    };

    //----------------------------------------------------------------------
    // Public
    //----------------------------------------------------------------------

    return {
      CallExpression(callExpression) {
        const callArguments = getI18nGetTextArguments(callExpression);

        if (!callArguments || callArguments.length < 1) {
          // We don't want to bother with undefined, null (possible return value), or an empty array
          return;
        }

        const firstArgument = callArguments[0];
        if (!firstArgument || !isAStringLiteral(firstArgument)) {
          return;
        }

        const i18nKey = firstArgument.value;
        if (i18nKeyIsPresent(context.options[0], i18nKey)) {
          return;
        }

        context.report({
          node: firstArgument,
          messageId: 'noMatchingKey',
          data: { i18nKey },
        });
      },
    };
  },
};
