/**
 * @fileoverview Checks the I18n key starts with the specified project prefix
 * @author Michael Kemp
 */
'use strict';

const { getI18nGetTextArguments, isAStringLiteral } = require('../common/i18n-get-text-argument');

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
  meta: {
    type: 'suggestion',
    docs: {
      description: 'Checks the I18n key starts with the specified project prefix',
      recommended: false,
      url: 'https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript/#limitations',
    },
    messages: {
      missingProjectPrefix:
        'I18n key "{{ i18nKey }}" is missing one of specified project prefixes {{ projectPrefixes }}',
    },
    hasSuggestions: true,
    fixable: 'code',
    schema: [
      {
        type: 'object',
        properties: {
          defaultPrefix: { type: 'string' },
          prefixes: {
            type: 'array',
            uniqueItems: true,
            items: {
              type: 'string',
            },
          },
        },
        required: ['defaultPrefix'],
        additionalProperties: false,
      },
    ],
  },

  create(context) {
    // variables should be defined here

    // If the prefix isn't configured, we should save the users some ESlint processing time
    if (!context.options || context.options.length < 1) {
      console.warn('"i18n-key-starts-with-project-prefix" rule has not been configured');
      return;
    }

    if (context.options[0].length < 1) {
      console.warn('No project prefixes have been specific to the rule "i18n-key-starts-with-project-prefix"');
      return;
    }

    const defaultPrefix = context.options[0].defaultPrefix;
    let projectPrefixes = context.options[0].prefixes || [];

    if (!projectPrefixes.includes(defaultPrefix)) {
      projectPrefixes = [defaultPrefix, ...projectPrefixes];
    }

    //----------------------------------------------------------------------
    // Helpers
    //----------------------------------------------------------------------

    const startsWithAPrefix = (i18nKey) => projectPrefixes.some((prefix) => i18nKey.startsWith(prefix));

    //----------------------------------------------------------------------
    // Public
    //----------------------------------------------------------------------

    return {
      CallExpression(callExpression) {
        const callArguments = getI18nGetTextArguments(callExpression);

        if (!callArguments) {
          // We don't want to bother with undefined, null (possible return value), or an empty array
          return;
        }

        const firstArgument = callArguments[0];

        if (!firstArgument || !isAStringLiteral(firstArgument)) {
          return;
        }

        const i18nKey = firstArgument.value;

        if (startsWithAPrefix(i18nKey)) {
          return;
        }

        context.report({
          node: firstArgument,
          messageId: 'missingProjectPrefix',
          data: {
            i18nKey: i18nKey || '',
            projectPrefixes,
          },
          suggest: [
            {
              messageId: 'missingProjectPrefix',
              data: {
                i18nKey: i18nKey || '',
                projectPrefixes,
              },
              fix: function (fixer) {
                const quoteMark = firstArgument.raw[0];
                return fixer.replaceText(firstArgument, `${quoteMark}${defaultPrefix}${i18nKey}${quoteMark}`);
              },
            },
          ],
        });
      },
    };
  },
};
