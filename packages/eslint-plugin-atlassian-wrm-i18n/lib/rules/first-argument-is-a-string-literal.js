/**
 * @fileoverview Checks the first argument is a string literal, which is all the WRM I18n system supports
 * @author Michael Kemp
 */
'use strict';

const { getI18nGetTextArguments, isAStringLiteral } = require('../common/i18n-get-text-argument');

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
  meta: {
    type: 'problem',
    docs: {
      description: 'Checks the first argument is a string literal, which is all the WRM I18n system supports',
      recommended: true,
      url: 'https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript/#limitations',
    },
    messages: {
      onlySupportLiterals:
        'I18n.getText() ONLY supports string literals (no template strings!) for the first property ID. Unexpected' +
        ' first argument: {{ firstArgument }}',
      concatLiterals: 'Replace concatenation of string literals with a single string literal',
    },
    hasSuggestions: true,
    fixable: 'code',
    schema: [],
  },

  create(context) {
    // variables should be defined here

    //----------------------------------------------------------------------
    // Helpers
    //----------------------------------------------------------------------

    const recurseToBottomOfBinaryExpression = (node) => {
      if (node.operator !== '+') {
        return null;
      }

      if (node.left.type === 'BinaryExpression') {
        return recurseToBottomOfBinaryExpression(node.left);
      }

      if (node.right.type === 'BinaryExpression') {
        return recurseToBottomOfBinaryExpression(node.right);
      }

      return node;
    };

    //----------------------------------------------------------------------
    // Public
    //----------------------------------------------------------------------

    return {
      CallExpression(callExpression) {
        const callArguments = getI18nGetTextArguments(callExpression);

        if (!callArguments) {
          // We don't want to bother with undefined, null (possible return value), or an empty array
          return;
        }

        const firstArgument = callArguments[0];

        if (!firstArgument || isAStringLiteral(firstArgument)) {
          return;
        }

        if (firstArgument.type === 'BinaryExpression') {
          const binaryExpNode = recurseToBottomOfBinaryExpression(firstArgument);

          if (binaryExpNode && isAStringLiteral(binaryExpNode.left) && isAStringLiteral(binaryExpNode.right)) {
            context.report({
              node: firstArgument,
              messageId: 'onlySupportLiterals',
              data: { firstArgument: firstArgument.name || '' },
              suggest: [
                {
                  messageId: 'concatLiterals',
                  fix: function (fixer) {
                    const quoteMark = binaryExpNode.left.raw[0];
                    return fixer.replaceText(
                      binaryExpNode,
                      `${quoteMark}${binaryExpNode.left.value}${binaryExpNode.right.value}${quoteMark}`,
                    );
                  },
                },
              ],
            });
            return;
          }
        }

        context.report({
          node: firstArgument,
          messageId: 'onlySupportLiterals',
          data: { firstArgument: firstArgument.name || '' },
        });
      },
    };
  },
};
