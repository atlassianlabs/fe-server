/**
 * @fileoverview Finds invalid usages of Atlassian webresource-manager's I18n system
 * @author Michael Kemp
 */
'use strict';

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const requireIndex = require('requireindex');

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------

// import all rules in lib/rules
module.exports.rules = requireIndex(__dirname + '/rules');
module.exports.getAllPropertiesKeys = require('./helpers/load-all-properties').getAllPropertiesKeys;
