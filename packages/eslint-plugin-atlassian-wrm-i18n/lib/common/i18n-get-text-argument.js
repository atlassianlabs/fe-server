//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------

const isGetTextCall = (callee) => {
  if (callee.type === 'MemberExpression') {
    const { property } = callee;

    return property.type === 'Identifier' && property.name === 'getText';
  }

  return false;
};

const isOnI18nObject = (callee) => {
  const { object } = callee;

  // e.g I18n.getText(
  if (isI18nIdentifier(object)) {
    return true;
  }

  if (object.type === 'MemberExpression') {
    // e.g window.namespace.WRM.I18n.getText(
    if (isI18nIdentifier(object.property)) {
      return true;
    }

    // e.g WRM["I18n"].getText(
    const { property: objectProperty } = object;
    if (objectProperty.type === 'Literal' && objectProperty.value === 'I18n') {
      return true;
    }
  }

  return false;
};

const isI18nIdentifier = (node) => node.type === 'Identifier' && node.name === 'I18n';

//----------------------------------------------------------------------
// Exported
//----------------------------------------------------------------------

/**
 * @param callExpression a call which could be `I18n.getText`
 * @returns {null|*} returns null if this isn't an `I18n.getText` call, else the arguments
 */
const getI18nGetTextArguments = (callExpression) => {
  const { callee, arguments: callArguments } = callExpression;

  if (!isGetTextCall(callee) || !isOnI18nObject(callee)) {
    return null;
  }

  return callArguments;
};

/**
 * Determines if a node is a string literal (e.g "I'm a string literal", 'I am a string literal', `I'm not a string literal`
 * @param node Any AST node
 */
const isAStringLiteral = (node) => node.type === 'Literal' && typeof node.value === 'string';

module.exports = {
  getI18nGetTextArguments,
  isAStringLiteral,
};
