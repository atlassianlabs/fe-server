# The first argument to `I18n.getText` is a string literal (first-argument-is-a-string-literal)

This rule aims to find invalid usages of `I18n.getText` where the first argument is not a string literal, as covered by the docs here:
[https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript/#limitations](https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript/#limitations)

Examples of **incorrect** code for this rule:

```js
WRM.I18n.getText(methodUsageIsInvalid());
WRM.I18n.getText(CONSTANT_USAGE_IS_INVALID);
WRM.I18n.getText(variableUsageIsInvalid);
WRM.I18n.getText(WRM.I18n.getText('the.outer.usage.is.invalid'));
WRM.I18n.getText(`using template strings is invalid`);
WRM.I18n.getText('concatentating' + 'i18n' + 'keys' + 'is' + 'invalid');
```

Examples of **correct** code for this rule:

```js
WRM.I18n.getText('com.example.plugin.fruit.basket.delete.button', 'some inserted value');
WRM.I18n.getText('com.example.plugin.fruit.basket.delete.button');
AJS.I18n.getText('com.example.plugin.fruit.basket.delete.button');
```
