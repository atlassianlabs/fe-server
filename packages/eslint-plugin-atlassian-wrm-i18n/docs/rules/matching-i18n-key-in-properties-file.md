# Checks there's a matching i18n key in the specified properties files (matching-i18n-key-in-properties-file)

This rule aims to ensure that there is a matching I18n key in the specified properties files

Given a properties file like (see the [README](../../README.md) for specifying properties files):

```properties
matching.i18n.key=some translation
```

An example of **incorrect** code for this rule:

```js
WRM.I18n.getText('some.other.i18n.key');
```

An example of **correct** code for this rule:

```js
WRM.I18n.getText('matching.i18n.key');
```
