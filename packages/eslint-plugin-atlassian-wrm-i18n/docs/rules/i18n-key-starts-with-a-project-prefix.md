# Checks the I18n key starts with the project prefix (i18n-key-starts-with-project-prefix)

This rule aims to ensure that all I18n keys starts with the project prefix to avoid collisions on the same key. The WRM I18n system can only
support one translation per key (i.e. they must be globally unique).
[https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript/#limitations](https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript/#limitations)

Given a project prefix of `'atl.audit.'` (see the [README](../../README.md) for specifying the project prefix).

An example of **incorrect** code for this rule:

```js
WRM.I18n.getText('example.button.desc');
```

An example of **correct** code for this rule:

```js
WRM.I18n.getText('atl.audit.example.button.desc');
```
