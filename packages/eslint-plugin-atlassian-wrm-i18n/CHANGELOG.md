# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 0.3.0 (2024-04-05)

### ⚠ BREAKING CHANGES

- SPFE-891: Support multiple project prefixes
- SPFE-891: Extract loading of property keys

### Features

- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Add ESlint rule to check for matching i18n keys in properties files ([9dce905](https://bitbucket.org/atlassianlabs/fe-server/commits/9dce905beabe6ac98e36c6731a6d2d778075c973))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Add project prefix rule ([cb1ea61](https://bitbucket.org/atlassianlabs/fe-server/commits/cb1ea61489d11154087662aaed87eccf60df97b7))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Add suggestion to join string concatenation of i18n keys ([31e028e](https://bitbucket.org/atlassianlabs/fe-server/commits/31e028e0e747ad3bb8cf3bc530ca84f37491418b))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Support multiple project prefixes ([5ec5ce3](https://bitbucket.org/atlassianlabs/fe-server/commits/5ec5ce3de800110d395de96f9430197f7f7ee7a7))
- Add ESlint plugin for WRM I18n ([2dd6927](https://bitbucket.org/atlassianlabs/fe-server/commits/2dd6927ad395516adf5db3a46f8052267a574eb8))

### Bug Fixes

- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Auto-enable I18n first getText argument must be a string literal ([4485967](https://bitbucket.org/atlassianlabs/fe-server/commits/44859675e31aade73dd459d521ea0d67b7fb3a9a))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Correct eslint rule name in README ([2734826](https://bitbucket.org/atlassianlabs/fe-server/commits/2734826c51b464bbe8e524ee5c826e351138e55e))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Extract loading of property keys ([2c1011e](https://bitbucket.org/atlassianlabs/fe-server/commits/2c1011e3ba733782a9e6921c43a25f278a497e9c))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): re-use quote marks specified when suggesting i18n key project prefix fix ([201a846](https://bitbucket.org/atlassianlabs/fe-server/commits/201a846a358bf4268f42c70ecbd9ade89b906cc2))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): type-guard checking for `WRM["I18n"].getText` ([f7f3c50](https://bitbucket.org/atlassianlabs/fe-server/commits/f7f3c50f1fa07b9de385283107103186910d4a07))

### [0.2.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/eslint-plugin-atlassian-wrm-i18n@0.2.1..eslint-plugin-atlassian-wrm-i18n@0.2.0) (2023-02-07)

### Bug Fixes

- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Correct eslint rule name in README ([2734826](https://bitbucket.org/atlassianlabs/fe-server/commits/2734826c51b464bbe8e524ee5c826e351138e55e))

## 0.2.0 (2022-08-09)

### ⚠ BREAKING CHANGES

- SPFE-891: Support multiple project prefixes
- SPFE-891: Extract loading of property keys

### Features

- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Add ESlint rule to check for matching i18n keys in properties files ([9dce905](https://bitbucket.org/atlassianlabs/fe-server/commits/9dce905beabe6ac98e36c6731a6d2d778075c973))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Add project prefix rule ([cb1ea61](https://bitbucket.org/atlassianlabs/fe-server/commits/cb1ea61489d11154087662aaed87eccf60df97b7))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Add suggestion to join string concatenation of i18n keys ([31e028e](https://bitbucket.org/atlassianlabs/fe-server/commits/31e028e0e747ad3bb8cf3bc530ca84f37491418b))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Support multiple project prefixes ([5ec5ce3](https://bitbucket.org/atlassianlabs/fe-server/commits/5ec5ce3de800110d395de96f9430197f7f7ee7a7))
- Add ESlint plugin for WRM I18n ([2dd6927](https://bitbucket.org/atlassianlabs/fe-server/commits/2dd6927ad395516adf5db3a46f8052267a574eb8))

### Bug Fixes

- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Auto-enable I18n first getText argument must be a string literal ([4485967](https://bitbucket.org/atlassianlabs/fe-server/commits/44859675e31aade73dd459d521ea0d67b7fb3a9a))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): Extract loading of property keys ([2c1011e](https://bitbucket.org/atlassianlabs/fe-server/commits/2c1011e3ba733782a9e6921c43a25f278a497e9c))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): re-use quote marks specified when suggesting i18n key project prefix fix ([201a846](https://bitbucket.org/atlassianlabs/fe-server/commits/201a846a358bf4268f42c70ecbd9ade89b906cc2))
- [SPFE-891](https://ecosystem.atlassian.net/browse/SPFE-891): type-guard checking for `WRM["I18n"].getText` ([f7f3c50](https://bitbucket.org/atlassianlabs/fe-server/commits/f7f3c50f1fa07b9de385283107103186910d4a07))

## 0.1.0 (2022-08-03)

### Features

- Add ESlint plugin for WRM I18n ([2dd6927](https://bitbucket.org/atlassianlabs/fe-server/commits/2dd6927ad395516adf5db3a46f8052267a574eb8))
