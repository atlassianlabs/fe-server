# eslint-plugin-atlassian-wrm-i18n

![node version](https://img.shields.io/node/v/@atlassian/i18n-properties-loader.svg)
![webpack peer dependency version](https://img.shields.io/npm/dependency-version/@atlassian/i18n-properties-loader/peer/webpack.svg)
![npm downloads](https://img.shields.io/npm/dt/@atlassian/i18n-properties-loader.svg)

Helps with using the Atlassian webresource-manager's I18n system

## Installation

You'll first need to install [ESLint](https://eslint.org/):

```sh
npm i eslint --save-dev
```

Next, install `eslint-plugin-atlassian-wrm-i18n`:

```sh
npm install eslint-plugin-atlassian-wrm-i18n --save-dev
```

## Usage

Add `atlassian-wrm-i18n` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```js
module.exports = {
  // ...
  plugins: ['atlassian-wrm-i18n'],
};
```

Then configure the rules you want to use under the rules section.

```js
const fg = require('fast-glob');

const { getAllPropertiesKeys } = require('eslint-plugin-atlassian-wrm-i18n');

// The more specific the pattern, the slower finding the files will take
// On a large project it's better to manually define each file
const i18nKeys = getAllPropertiesKeys(
  fg.sync('**/src/**/*.properties', { dot: false, caseSensitiveMatch: false, onlyFiles: true }),
);

module.exports = {
  // ...
  rules: {
    'atlassian-wrm-i18n/first-argument-is-a-string-literal': 'error',
    'atlassian-wrm-i18n/i18n-key-starts-with-a-project-prefix': [
      'error',
      {
        defaultPrefix: 'jira.gh.', // Will be suggested
        prefixes: ['jira.frontend.'], // (optional) Additional valid prefixes
      },
    ],
    'atlassian-wrm-i18n/matching-i18n-key-in-properties-file': ['error', i18nKeys],
  },
};
```

## Supported Rules

- [first-argument-is-a-string-literal](docs/rules/first-argument-is-a-string-literal.md)
- [i18n-key-starts-with-project-prefix](docs/rules/i18n-key-starts-with-a-project-prefix.md)
- [matching-i18n-key-in-properties-file](docs/rules/matching-i18n-key-in-properties-file.md)

## Minimum requirements

This plugin is tested against, and only supports:

- ESlint 8+
- Node 14+
