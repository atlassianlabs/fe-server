/**
 * @fileoverview Checks the first argument is a string literal, which is all the WRM I18n system supports
 * @author Michael Kemp
 */
'use strict';

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require('../../../lib/rules/i18n-key-starts-with-a-project-prefix');
const RuleTester = require('eslint').RuleTester;

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parserOptions: {
    ecmaVersion: 6, // Required to be able to parse template strings
  },
});

ruleTester.run('i18n-key-starts-with-project-prefix', rule, {
  valid: [
    {
      code: 'WRM.I18n.getText("i18n.key")',
      options: [{ defaultPrefix: '' }],
    },
    {
      code: 'WRM.I18n.getText("example.prefix.i18n.key")',
      options: [{ defaultPrefix: 'example.prefix.' }],
    },
    {
      code: 'I18n.getText("example.prefix.i18n.key")',
      options: [{ defaultPrefix: 'example.prefix.' }],
    },
    {
      code: 'I18n.getText(methodUsageIsInvalid())',
      options: [{ defaultPrefix: 'example.prefix.' }],
    },
    {
      code: 'WRM.I18n.getText(BAD_CONSTANT_USAGE)',
      options: [{ defaultPrefix: 'example.prefix.' }],
    },
    {
      code: 'WRM.I18n.getText(`Since 2003 the globe has seen SARS-CoV-1, MERS-CoV, Ebola, and H1N1 yet many governments were unprepared for SARS-CoV-2`)',
      options: [{ defaultPrefix: 'example.prefix.' }],
    },
    {
      code: "WRM.I18n.getText('first.part.of.an.i18n.key' + 'second.part.of.an.i18n.key')",
      options: [{ defaultPrefix: 'example.prefix.' }],
    },
    {
      code: 'WRM.I18n.getText("example.prefix.i18n.key")',
      options: [{ defaultPrefix: 'default.prefix.', prefixes: ['example.prefix.'] }],
    },
  ],

  invalid: [
    {
      code: 'WRM.I18n.getText("i18n.key")',
      options: [{ defaultPrefix: 'example.prefix.' }],
      errors: [
        {
          suggestions: [
            {
              messageId: 'missingProjectPrefix',
              data: {
                i18nKey: 'i18n.key',
                projectPrefixes: ['example.prefix.'],
              },
              output: 'WRM.I18n.getText("example.prefix.i18n.key")',
            },
          ],
        },
      ],
    },
    {
      code: "WRM.I18n.getText('i18n.key')",
      options: [{ defaultPrefix: 'example.prefix.' }],
      errors: [
        {
          suggestions: [
            {
              messageId: 'missingProjectPrefix',
              data: {
                i18nKey: 'i18n.key',
                projectPrefixes: ['example.prefix.'],
              },
              output: "WRM.I18n.getText('example.prefix.i18n.key')",
            },
          ],
        },
      ],
    },
  ],
});
