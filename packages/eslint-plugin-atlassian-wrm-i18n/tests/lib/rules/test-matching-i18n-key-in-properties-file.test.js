/**
 * @fileoverview Checks there's a matching i18n key in the specified properties files
 * @author Michael Kemp
 */
'use strict';

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require('../../../lib/rules/matching-i18n-key-in-properties-file');
const RuleTester = require('eslint').RuleTester;

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester();

ruleTester.run('matching-i18n-key-in-properties-file', rule, {
  valid: [
    {
      code: 'WRM.I18n.getText("matching.i18n.key")',
      options: [['matching.i18n.key']],
    },
  ],

  invalid: [
    {
      code: 'WRM.I18n.getText("nonexistent.i18n.key")',
      options: [[]],
      errors: [
        {
          messageId: 'noMatchingKey',
          data: {
            i18nKey: 'nonexistent.i18n.key',
          },
          type: 'Literal',
        },
      ],
    },
  ],
});
