/**
 * @fileoverview Checks the first argument is a string literal, which is all the WRM I18n system supports
 * @author Michael Kemp
 */
'use strict';

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require('../../../lib/rules/first-argument-is-a-string-literal');
const RuleTester = require('eslint').RuleTester;

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parserOptions: {
    ecmaVersion: 6, // Required to be able to parse template strings
  },
});

ruleTester.run('first-argument-is-a-string-literal', rule, {
  valid: [
    `I18n.getText('something.valid')`,
    `WRM.I18n.getText('something.valid')`,
    `AJS.I18n.getText('something.valid')`,
    `WRM.I18n.getText('plugin.i18n.multi.second');`,
    `WRM["I18n"].getText('something.valid')`,
    `WRM["I18n"].getText(  "plugin.i18n.multi.sixth"  , result())`,
    `namespace.I18n.getText(  'plugin.i18n.multi.third'  )`,
    `window.window.WRM.I18n.getText('plugin.i18n.deeply - nested - namespace');`,
    `lol.I18n.getText('plugin.i18n.deeply - nested - namespace');`,
    `WRM('I18n').getText('plugin.i18n.deeply - nested - namespace');`, // not actually valid, just here to ensure there's a type-guard
  ],

  invalid: [
    {
      code: 'WRM.I18n.getText(methodUsageIsInvalid())',
      errors: [{ messageId: 'onlySupportLiterals', type: 'CallExpression' }],
    },
    {
      code: 'AJS.I18n.getText(methodUsageIsInvalid())',
      errors: [{ messageId: 'onlySupportLiterals', type: 'CallExpression' }],
    },
    {
      code: 'I18n.getText(methodUsageIsInvalid())',
      errors: [{ messageId: 'onlySupportLiterals', type: 'CallExpression' }],
    },
    {
      code: 'WRM.I18n.getText(badMethodUsage(), validParameter)',
      errors: [{ messageId: 'onlySupportLiterals', type: 'CallExpression' }],
    },
    {
      code: 'WRM.I18n.getText(BAD_CONSTANT_USAGE)',
      errors: [{ messageId: 'onlySupportLiterals', type: 'Identifier' }],
    },
    {
      code: "WRM.I18n.getText(WRM.I18n.getText('something.valid'))",
      errors: [{ messageId: 'onlySupportLiterals', type: 'CallExpression' }],
    },
    {
      code: "WRM.I18n.getText(WRM.I18n.getText('something.valid'), validParameter)",
      errors: [{ messageId: 'onlySupportLiterals', type: 'CallExpression' }],
    },
    {
      code: 'WRM.I18n.getText(`Since 2003 the globe has seen SARS-CoV-1, MERS-CoV, Ebola, and H1N1 yet many governments were unprepared for SARS-CoV-2`)',
      errors: [{ messageId: 'onlySupportLiterals', type: 'TemplateLiteral' }],
    },
    {
      code: 'WRM.I18n.getText(`Since ${"2003"} the globe has seen SARS-CoV-1, MERS-CoV, Ebola, and H1N1 yet many governments were unprepared for SARS-CoV-2`)',
      errors: [{ messageId: 'onlySupportLiterals', type: 'TemplateLiteral' }],
    },
    {
      code: 'WRM.I18n.getText(`Since 2003 the globe has seen SARS-CoV-1, MERS-CoV, Ebola, and H1N1 yet many governments were unprepared for SARS-CoV-2`, validParameter)',
      errors: [{ messageId: 'onlySupportLiterals', type: 'TemplateLiteral' }],
    },
    {
      code: 'WRM.I18n.getText(tagged`Since ${year} the globe has seen SARS-CoV-1, MERS-CoV, Ebola, and H1N1 yet many governments were unprepared for SARS-CoV-2`, validParameter)',
      errors: [{ messageId: 'onlySupportLiterals', type: 'TaggedTemplateExpression' }],
    },
    {
      code: "WRM.I18n.getText('first.part.of.an.i18n.key.' + 'second.part.of.an.i18n.key')",
      errors: [
        {
          messageId: 'onlySupportLiterals',
          type: 'BinaryExpression',
          suggestions: [
            {
              messageId: 'concatLiterals',
              output: "WRM.I18n.getText('first.part.of.an.i18n.key.second.part.of.an.i18n.key')",
            },
          ],
        },
      ],
    },
    {
      code: "WRM.I18n.getText('first.part.of.an.i18n.key.' + 'second.part.of.an.i18n.key.' + 'third.part.of.an.i18n.key')",
      errors: [
        {
          messageId: 'onlySupportLiterals',
          type: 'BinaryExpression',
          suggestions: [
            {
              messageId: 'concatLiterals',
              output:
                "WRM.I18n.getText('first.part.of.an.i18n.key.second.part.of.an.i18n.key.' + 'third.part.of.an.i18n.key')",
            },
          ],
        },
      ],
    },
    {
      code: "WRM.I18n.getText('first.part.of.an.i18n.key.' + 'second.part.of.an.i18n.key', validParameter)",
      errors: [
        {
          messageId: 'onlySupportLiterals',
          type: 'BinaryExpression',
          suggestions: [
            {
              messageId: 'concatLiterals',
              output: "WRM.I18n.getText('first.part.of.an.i18n.key.second.part.of.an.i18n.key', validParameter)",
            },
          ],
        },
      ],
    },
    {
      code: 'WRM.I18n.getText("first.part.of.an.i18n.key." +\n  "second.part.of.an.i18n.key");',
      errors: [
        {
          messageId: 'onlySupportLiterals',
          type: 'BinaryExpression',
          suggestions: [
            {
              messageId: 'concatLiterals',
              output: 'WRM.I18n.getText("first.part.of.an.i18n.key.second.part.of.an.i18n.key");',
            },
          ],
        },
      ],
    },
    {
      code: '{I18n.getText("first.part.of.an.i18n.key." +\n  "second.part.of.an.i18n.key");}',
      errors: [
        {
          messageId: 'onlySupportLiterals',
          type: 'BinaryExpression',
          suggestions: [
            {
              messageId: 'concatLiterals',
              output: '{I18n.getText("first.part.of.an.i18n.key.second.part.of.an.i18n.key");}',
            },
          ],
        },
      ],
    },
  ],
});
