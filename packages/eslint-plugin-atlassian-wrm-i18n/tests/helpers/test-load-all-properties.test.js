const { resolve } = require('path');

const { getAllPropertiesKeys } = require('../../lib/helpers/load-all-properties');

describe('Should be able to load valid files', () => {
  it('that are empty', () => {
    const testFilename = resolve(__dirname, '..', 'resources', 'properties', 'i18n', 'empty.properties');
    const properties = getAllPropertiesKeys([testFilename]);

    // eslint-disable-next-line no-undef
    expect(properties).toStrictEqual([]);
  });

  describe('with only a', () => {
    it('comment line', () => {
      const testFilename = resolve(__dirname, '..', 'resources', 'properties', 'i18n', 'comment-line.properties');
      const properties = getAllPropertiesKeys([testFilename]);

      // eslint-disable-next-line no-undef
      expect(properties).toStrictEqual([]);
    });

    it('key=property line', () => {
      const testFilename = resolve(
        __dirname,
        '..',
        'resources',
        'properties',
        'i18n',
        'key-equals-property.properties',
      );
      const properties = getAllPropertiesKeys([testFilename]);

      // eslint-disable-next-line no-undef
      expect(properties).toStrictEqual(['key']);
    });

    it('key:property line', () => {
      const testFilename = resolve(__dirname, '..', 'resources', 'properties', 'i18n', 'key-colon-property.properties');
      const properties = getAllPropertiesKeys([testFilename]);

      // eslint-disable-next-line no-undef
      expect(properties).toStrictEqual(['key']);
    });
  });

  it('with key:property broken over multiple lines', () => {
    const testFilename = resolve(
      __dirname,
      '..',
      'resources',
      'properties',
      'i18n',
      'key-equals-property-broken-up.properties',
    );
    const properties = getAllPropertiesKeys([testFilename]);

    // eslint-disable-next-line no-undef
    expect(properties).toStrictEqual(['key']);
  });

  it('that come from integration hell', () => {
    const testFilename = resolve(__dirname, '..', 'resources', 'properties', 'i18n', 'from-hell.properties');
    const properties = getAllPropertiesKeys([testFilename]);

    // eslint-disable-next-line no-undef
    expect(properties).toStrictEqual(['indented.key', 'broken.up.key', 'key.missing.property', 'key2']);
  });
});
