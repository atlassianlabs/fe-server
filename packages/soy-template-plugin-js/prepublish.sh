#!/bin/sh
set -e

./etc/setup_maven.sh

# The version of NPM package should match the version of Maven artifact that can be found here:
# https://packages.atlassian.com/content/repositories/atlassian-public/com/atlassian/soy/atlassian-soy-cli-support/
VERSION=$(grep '"atlassian-soy-cli-support":' package.json |  sed -E 's/.*"[^"]+": "([^"]+)".*/\1/')
echo "Installing \"atlassian-soy-cli-support\" version ${VERSION}..."
rm -rf ./dist

mvn dependency:copy \
-Dartifact=com.atlassian.soy:atlassian-soy-cli-support:$VERSION:jar:jar-with-dependencies \
-Dmdep.stripClassifier=true \
-Dmdep.stripVersion=true \
-DoutputDirectory=./dist/jar

mvn dependency:unpack \
-Dartifact=com.atlassian.soy:soy-template-plugin:$VERSION \
-DoutputDirectory=./dist \
-Dproject.basedir=/tmp \
-Dmdep.unpack.includes=**\/*.js,**\/LICENSE.txt

mv dist/META-INF/LICENSE.txt ./LICENSE
rmdir dist/META-INF
