// Plugin
export type ResourceParamMap = Map<string, ResourceParam[]>;

export type PluginOptions = {
  /** @property pluginKey - The fully qualified plugin key. e.g.: com.atlassian.jira.plugins.my-jira-plugin */
  pluginKey?: string;

  /** @property xmlDescriptors - Path to the directory where this plugin stores the descriptors about this plugin, used by the WRM to load your frontend code. */
  xmlDescriptors?: string;

  /** @property packageName - Name of frontend package to associate all output JS files with. */
  packageName?: string;

  /** @property resourceParamMap - Parameters to be added to specific file types. */
  resourceParamMap?: MapOrObject<ResourceParam[]>;

  /** @property prettyPrint - Makes the resulting resources xml pretty printed for debugging purposes */
  prettyPrint?: boolean;
};

// XML
export type AttributesObject = { [key: string]: unknown };

// WebResources
export type ResourceParam = {
  name: string;
  value: string;
};

// Helpers
export type MapOrObject<T> = Map<string, T> | { [key: string]: T };
