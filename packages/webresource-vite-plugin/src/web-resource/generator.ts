import path from 'node:path';

import { renderElement } from '../helpers/xml';
import type { PluginOptions, ResourceParam, ResourceParamMap } from '../types/types';

export type WebResource = {
  key: string;
  dependencies: Array<string>;
  resources: Array<WebResourceResource>;
};

export type Parameter = {
  name: string;
  value: string;
};

export type WebResourceResource = {
  name: string;
  location: string;
  params?: Array<Parameter>;
};

function generateParameterXml({ name, value }: Parameter) {
  const attrs = { name, value };
  return renderElement('param', attrs);
}

function generateResourceXml(resource: WebResourceResource, parameterMap: ResourceParamMap) {
  const { name, location, params } = resource;

  const children = [];

  // Default attributes from configuration
  const assetContentType = path.extname(location).substring(1);
  const parameters = parameterMap.get(assetContentType) || [];
  const renderParameters = (attributes: ResourceParam) => children.push(renderElement('param', attributes));
  parameters.forEach(renderParameters);

  if (params) {
    children.push(...params.map(generateParameterXml));
  }

  const attributes = {
    type: 'download',
    name,
    location,
  };

  return renderElement('resource', attributes, children);
}

function generateResourcesXml(resources: WebResourceResource[], resourceParamMap: ResourceParamMap) {
  return (
    resources
      // ignore all `.map` files, since the WRM finds them of its own accord.
      .filter((resource) => !resource.location.endsWith('.map'))
      .map((resource) => generateResourceXml(resource, resourceParamMap))
  );
}

function generateDependencyXml(dependencies: string[]) {
  return dependencies.map((dependency) => renderElement('dependency', undefined, dependency));
}

function generateWebResourceXml(pluginOptions: PluginOptions, webResource: WebResource) {
  const { key, dependencies, resources } = webResource;

  const attrs = { key };

  const children = [
    ...generateDependencyXml(dependencies),
    ...generateResourcesXml(resources, pluginOptions.resourceParamMap as ResourceParamMap),
  ];

  return renderElement('web-resource', attrs, children);
}

export function generateWebResourcesXml(pluginOptions: PluginOptions, webResources: Array<WebResource>) {
  const transformed = webResources.map((wr) => generateWebResourceXml(pluginOptions, wr));
  return renderElement('bundle', undefined, transformed);
}
