import type { PluginOptions } from '../types/types';
import type { WebResource } from '../web-resource/generator';

export function generateAssociationsReport(pluginOptions: PluginOptions, webResources: Array<WebResource>) {
  const files = [
    ...new Set(
      webResources.map((descriptor) => descriptor.resources.map((resource) => resource.location)).flat(),
    ).values(),
  ];

  const output = {
    packageName: pluginOptions.packageName,
    outputDirectoryFiles: files,
  };

  return JSON.stringify(output, null, 2);
}
