import { unionBy } from 'lodash';

import type { MapOrObject, ResourceParam, ResourceParamMap } from '../types/types';

export const toMap = <T>(original: MapOrObject<unknown>): Map<string, T> => {
  if (original instanceof Map) {
    return original as Map<string, T>;
  }
  return original && typeof original === 'object' ? new Map(Object.entries(original)) : new Map();
};

export function ensureResourceParamsAreUnique(params: ResourceParamMap) {
  params.forEach((val, key, map) => {
    const values = ([] as ResourceParam[]).concat(val).filter(Boolean);
    map.set(key, unionBy(values.reverse(), 'name').reverse());
  });
  return params;
}
