import type { AttributesObject } from '../types/types';

export const stringifyAttributes = (attributes: AttributesObject) => {
  if (!attributes) {
    return '';
  }

  return (
    ' ' +
    Object.keys(attributes)
      .map((key) => {
        const val = typeof attributes[key] === 'undefined' ? '' : String(attributes[key]);
        return `${key}="${val}"`;
      })
      .join(' ')
  );
};

export const renderElement = (
  name: string,
  attributes: AttributesObject | string | undefined,
  children?: string | string[],
) => {
  if (typeof attributes === 'object') {
    attributes = stringifyAttributes(attributes);
  }
  // avoid outputting 'undefined' when attributes aren't present.
  if (!attributes) {
    attributes = '';
  }

  // convert children array in to a string.
  // assume they are string values already. (todo: refactor for nested rendering?)
  if (Array.isArray(children)) {
    children = children.join('');
  }
  // render self-closing tags based on whether there is no child input.
  if (!children) {
    return `<${name}${attributes}/>`;
  }

  return `<${name}${attributes}>${children}</${name}>`;
};
