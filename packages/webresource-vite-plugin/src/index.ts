import path from 'node:path';
import { pd as PrettyData } from 'pretty-data';
import type { Plugin } from 'vite';

import { generateAssociationsReport } from './association/generator';
import { ensureResourceParamsAreUnique, toMap } from './helpers/options';
import type { PluginOptions } from './types/types';
import type { WebResource, WebResourceResource } from './web-resource/generator';
import { generateWebResourcesXml } from './web-resource/generator';

const defaultResourceParams = new Map().set('svg', [
  {
    name: 'content-type',
    value: 'image/svg+xml',
  },
]);

const defaultOptions = {
  pluginKey: '${atlassian.plugin.key}',
  xmlDescriptors: 'META-INF/plugin-descriptor/wrm-vite-modules.xml',
  resourceParamMap: defaultResourceParams,
  prettyPrint: false,
};

const ASSOCIATIONS_PATH_IN_BUNDLE = 'META-INF/fe-manifest-associations';

function webresourcePlugin(options: PluginOptions = {}): Plugin {
  const pluginOptions = Object.assign({}, defaultOptions, options);
  const { pluginKey, xmlDescriptors } = pluginOptions;

  pluginOptions.resourceParamMap = ensureResourceParamsAreUnique(toMap(pluginOptions.resourceParamMap));

  const xmlDescriptorFilepath = path.relative('.', xmlDescriptors);

  const resolveLocalDep = (dep: string) => `${pluginKey}:${dep}`;

  return {
    name: 'webresource',

    generateBundle(_, bundle) {
      // generate WRM metadata used to link bundles together in XML
      const wrmMetadata: { [k: string]: { key: string } } = {};
      for (const [filename, info] of Object.entries(bundle)) {
        const key = ('isEntry' in info && info.isEntry ? 'entry_' : 'split_') + info.fileName;
        wrmMetadata[filename] = {
          key,
        };
      }

      const webResources: Array<WebResource> = [];
      const webResourcesAssets: Array<WebResourceResource> = [];
      for (const [filename, metadata] of Object.entries(bundle)) {
        if (!('imports' in metadata)) {
          // CSS, Images, txt, etc...
          webResourcesAssets.push({ name: metadata.fileName, location: metadata.fileName });
          continue;
        }

        const dependencies = metadata.imports.map((i) => resolveLocalDep(wrmMetadata[i].key));

        const resources: Array<WebResourceResource> = [{ name: metadata.fileName, location: metadata.fileName }];

        if (metadata.viteMetadata?.importedAssets) {
          metadata.viteMetadata.importedAssets.forEach((asset) => resources.push({ name: asset, location: asset }));
        }

        if (metadata.viteMetadata?.importedCss) {
          metadata.viteMetadata.importedCss.forEach((asset) => resources.push({ name: asset, location: asset }));
        }

        webResources.push({
          key: wrmMetadata[filename].key,
          dependencies,
          resources,
        });
      }

      if (webResourcesAssets.length) {
        webResources.push({
          // TODO hash?
          key: 'assets',
          dependencies: [],
          resources: webResourcesAssets,
        });
      }

      this.emitFile({
        type: 'asset',
        name: 'wrm-module-definitions',
        fileName: xmlDescriptorFilepath,
        source: pluginOptions.prettyPrint
          ? PrettyData.xml(generateWebResourcesXml(pluginOptions, webResources))
          : generateWebResourcesXml(pluginOptions, webResources),
      });

      if (pluginOptions.packageName) {
        const associationsFilepath = path.relative('.', ASSOCIATIONS_PATH_IN_BUNDLE);
        const fileName = path.join(associationsFilepath, `${pluginKey}-vite.intermediary.json`);

        this.emitFile({
          type: 'asset',
          name: 'wrm-associations-definitions',
          fileName,
          source: generateAssociationsReport(pluginOptions, webResources),
        });
      }
    },
  };
}

export = webresourcePlugin;
