# Atlassian Web-Resource Vite Plugin

**Early alpha development**

This plugin is experimental work and not supported by Atlassian.

Auto-generates web-resource definitions from your Vite code, for usage in an Atlassian product or plugin.

Features compatible with the webpack plugin:

- WRM modules generation (modules.xml)
  - Single bundled web resources
  - Dependencies (basic dependency, shared code)
  - Resources (parameters)
- FE Manifest generation (.intermediary.json)

Not supported (yet?):

- WRM modules
  - Transformations (like i18n and less)
  - Provided modules
  - Async chunks (not tested)
  - Data providers
  - Deprecations
- Hot-reloading

[Issue tracker](https://ecosystem.atlassian.net/jira/software/c/projects/SPFE/boards/672)
