import { load } from 'cheerio';
import fs from 'node:fs';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, './relative/dist');

let result;
let output;
let wrDefs;
let $;

let wrStats;

const xmlDescriptors = './all-rolled-up.xml';
const expectedPath = path.resolve(OUTPUT_DIR, xmlDescriptors);

describe('configure-output-dir', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [plugin({ xmlDescriptors })],
      build: {
        outDir: OUTPUT_DIR,
        assetsDir: '.',
        rollupOptions: {
          input: path.resolve(FRONTEND_SRC_DIR, 'index.js'),
          output: {
            format: 'amd',
          },
        },
      },
    });
    output = result.output;
    wrDefs = output.find((file) => file.fileName.endsWith('.xml'));
    wrStats = fs.statSync(expectedPath);
    $ = load(wrDefs.source);
  });

  it('outputs to the expected sub-directory', () => {
    expect(wrStats.isFile()).toEqual(true);
  });

  it('contains web-resource definitions', () => {
    const source = fs.readFileSync(expectedPath, 'utf-8');
    expect(source).toEqual(wrDefs.source);
  });

  it('resource locations exist', () => {
    const resourceLocations = $('web-resource > resource')
      .map((i, el) => {
        return $(el).attr('location');
      })
      .get();
    expect(resourceLocations.length).toEqual(3);

    const resourceStats = resourceLocations
      .map((loc) => path.join(OUTPUT_DIR, loc))
      .map(fs.statSync)
      .map((stats) => stats.isFile());
    expect(resourceStats).toEqual([true, true, true]);
  });
});
