import log from './utils/one';
console.log('a module', log());

import('./utils/two.js').then((log) => console.log(log()));
import('./utils/deep/three.js').then((log) => console.log(log()));
