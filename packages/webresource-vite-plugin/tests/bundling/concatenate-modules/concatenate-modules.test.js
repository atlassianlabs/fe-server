import { load } from 'cheerio';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

let result;
let wrDefs;
let $;

describe('single-module', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [plugin()],
      build: {
        outDir: OUTPUT_DIR,
        assetsDir: '.',
        rollupOptions: {
          input: path.resolve(FRONTEND_SRC_DIR, 'index.js'),
        },
      },
    });
    wrDefs = result.output.find((file) => file.fileName.endsWith('.xml'));
    $ = load(wrDefs.source);
  });

  it('generates a web-resource', () => {
    expect($('web-resource').length).toEqual(1);
  });

  it('contains single resource', () => {
    const resource = $('web-resource > resource');
    expect(resource.attr('type')).toEqual('download');
    expect(resource.attr('name')).toMatch(/.js$/);
    expect(resource.attr('location')).toMatch(/.js$/);
  });
});
