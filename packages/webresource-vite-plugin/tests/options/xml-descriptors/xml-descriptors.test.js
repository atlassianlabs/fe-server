import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

const XML_DESCRIPTOR = 'META-INF/plugin-descriptor/wrm-vite-modules.xml';

const inputOptions = (wrm) => ({
  plugins: [wrm],
  build: {
    outDir: OUTPUT_DIR,
    assetsDir: '.',
    rollupOptions: {
      input: path.resolve(FRONTEND_SRC_DIR, 'index.js'),
    },
  },
});

describe('xml-descriptors', () => {
  it(`descriptor path defaults to '${XML_DESCRIPTOR}'`, async () => {
    const p = plugin();
    const result = await build(inputOptions(p));
    const files = result.output.map((file) => file.fileName);
    expect(files).toEqual(expect.arrayContaining([XML_DESCRIPTOR]));
  });

  it('descriptor path accepts relative paths', async () => {
    const p = plugin({ xmlDescriptors: './././foo-bar.xml' });
    const result = await build(inputOptions(p));
    const files = result.output.map((file) => file.fileName);
    expect(files).toEqual(expect.arrayContaining(['foo-bar.xml']));
  });
});
