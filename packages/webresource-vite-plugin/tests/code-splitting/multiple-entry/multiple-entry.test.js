import { load } from 'cheerio';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');
const get = (filepath) => path.resolve(FRONTEND_SRC_DIR, filepath);

let result;
let output;
let wrDefs;
let $;

describe('multiple-entry', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [plugin()],
      build: {
        outDir: OUTPUT_DIR,
        assetsDir: '.',
        rollupOptions: {
          input: [get('component-1.js'), get('component-2.js')],
        },
      },
    });
    output = result.output;
    wrDefs = output.find((file) => file.fileName.endsWith('.xml'));
    $ = load(wrDefs.source);
  });

  it('generates three web-resources', () => {
    expect($('web-resource').length).toEqual(3);
  });

  it('generates two entrypoints and one split chunk', () => {
    const keys = $('web-resource')
      .map((i, el) => $(el).attr('key'))
      .get();

    expect(keys).toEqual(
      expect.arrayContaining([
        expect.stringMatching(/^entry_component-1/),
        expect.stringMatching(/^entry_component-2/),
        expect.stringMatching(/^split_obj/),
      ]),
    );
  });

  it('each entrypoint has a dependency on the split chunk', () => {
    const deps = $('web-resource[key^="entry"] > dependency');

    expect(deps.length).toEqual(2);
    expect(deps.map((i, el) => $(el).text()).get()).toEqual(
      expect.arrayContaining([expect.stringMatching(/:split_obj/), expect.stringMatching(/:split_obj/)]),
    );
  });
});
