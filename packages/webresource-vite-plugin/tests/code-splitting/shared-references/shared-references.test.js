import { load } from 'cheerio';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

let result;
let output;
let wrDefs;
let $;

describe('shared-references', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [plugin()],
      build: {
        outDir: OUTPUT_DIR,
        assetsDir: '.',
        rollupOptions: {
          input: path.resolve(FRONTEND_SRC_DIR, 'index.js'),
          preserveEntrySignatures: 'strict',
        },
      },
    });
    output = result.output;
    wrDefs = output.find((file) => file.fileName.endsWith('.xml'));
    $ = load(wrDefs.source);
  });

  it('generates three web-resources', () => {
    expect($('web-resource').length).toEqual(3);
  });

  it('generates one entrypoint and two split chunks', () => {
    const keys = $('web-resource')
      .map((i, el) => $(el).attr('key'))
      .get();

    expect(keys).toEqual(
      expect.arrayContaining([
        expect.stringMatching(/^entry_index/),
        expect.stringMatching(/^split_index/),
        expect.stringMatching(/^split_lazy/),
      ]),
    );
  });
});
