import { load } from 'cheerio';
import fs from 'node:fs';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

let result;
let output;
let wrDefs;
let $;

let bundledFile;

describe('css-and-assets-via-style-loader', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [plugin()],
      build: {
        outDir: OUTPUT_DIR,
        assetsDir: '.',
        rollupOptions: {
          input: path.resolve(FRONTEND_SRC_DIR, 'app.js'),
        },
      },
    });
    output = result.output;
    wrDefs = output.find((file) => file.fileName.endsWith('.xml'));
    bundledFile = output.find((f) => f.fileName.startsWith('app'));
    $ = load(wrDefs.source);
  });

  it('should create an "asset"-webresource containing the image referenced in the stylesheet', () => {
    const resource = $('web-resource[key^="assets"] > resource');

    expect(resource.attr('type')).toEqual('download');
    expect(path.extname(resource.attr('name'))).toEqual('.png');
  });

  it('should interpolate the CSS rules in to the JS code', () => {
    const bundleFile = fs.readFileSync(path.join(OUTPUT_DIR, bundledFile.fileName), 'utf-8');
    const expectedLine = /<div class="\$\{(.*?)\}"><div class="\$\{(.*?)\}"><\/div><\/div>`;/;

    const result = expectedLine.exec(bundleFile);

    expect(result).toHaveLength(3);
    expect(result[1]).toContain('.wurst');
    expect(result[2]).toContain('.tricky');
  });
});
