import { load } from 'cheerio';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

let result;
let output;
let wrDefs;
let $;

describe('resource-defaults', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [plugin()],
      build: {
        outDir: OUTPUT_DIR,
        assetsInlineLimit: 0,
        assetsDir: '.',
        rollupOptions: {
          input: path.resolve(FRONTEND_SRC_DIR, 'app.js'),
        },
      },
    });
    output = result.output;
    wrDefs = output.find((file) => file.fileName.endsWith('.xml'));
    $ = load(wrDefs.source, { xml: true });
  });

  it('should add image/svg+xml by default for svg files', () => {
    const svg = $('web-resource[key^="assets"] > resource[name$=".svg"]');
    expect(svg.attr('type')).toEqual('download');
    expect(svg.children()).toHaveLength(1);
    expect(svg.children()[0].attribs['name']).toEqual('content-type');
    expect(svg.children()[0].attribs['value']).toEqual('image/svg+xml');
  });
});
