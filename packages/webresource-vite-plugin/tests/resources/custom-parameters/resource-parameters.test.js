import { load } from 'cheerio';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

let result;
let output;
let wrDefs;
let $;

describe('resource-parameters', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [
        plugin({
          resourceParamMap: {
            svg: [
              {
                name: 'content-type',
                value: 'image/svg+xml',
              },
              {
                name: 'foo',
                value: 'bar',
              },
            ],
            png: [
              {
                name: 'bar',
                value: 'baz',
              },
            ],
            jpg: [
              {
                name: 'duped',
                value: 'first',
              },
              {
                name: 'bar',
                value: 'baz',
              },
              {
                name: 'duped',
                value: 'second',
              },
            ],
          },
        }),
      ],
      build: {
        outDir: OUTPUT_DIR,
        assetsInlineLimit: 0,
        assetsDir: '.',
        rollupOptions: {
          input: path.resolve(FRONTEND_SRC_DIR, 'app.js'),
        },
      },
    });
    output = result.output;
    wrDefs = output.find((file) => file.fileName.endsWith('.xml'));
    $ = load(wrDefs.source, { xml: true });
  });

  it('should add a param to a resource', () => {
    const resources = $('web-resource[key^="assets"] > resource');
    expect(resources).toHaveLength(3);

    const png = $('web-resource[key^="assets"] > resource[name$=".png"]');
    expect(path.extname(png.attr('name'))).toEqual('.png');
    expect(png.children()).toHaveLength(1);
    expect(png.children()[0].attribs['name']).toEqual('bar');
    expect(png.children()[0].attribs['value']).toEqual('baz');

    const svg = $('web-resource[key^="assets"] > resource[name$=".svg"]');
    expect(svg.attr('type')).toEqual('download');
    expect(svg.children()).toHaveLength(2);
    expect(svg.children()[0].attribs['name']).toEqual('content-type');
    expect(svg.children()[0].attribs['value']).toEqual('image/svg+xml');
    expect(svg.children()[1].attribs['name']).toEqual('foo');
    expect(svg.children()[1].attribs['value']).toEqual('bar');
  });

  it('should include the last defined value for a given parameter', () => {
    const jpg = $('web-resource[key^="assets"] > resource[name$=".jpg"]');
    expect(jpg.children()).toHaveLength(2);
    expect(jpg.children()[0].attribs['name']).toEqual('bar');
    expect(jpg.children()[1].attribs['name']).toEqual('duped');
    expect(jpg.children()[1].attribs['value']).toEqual('second');
  });
});
