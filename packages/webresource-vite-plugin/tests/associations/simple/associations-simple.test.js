import fs from 'node:fs';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { build } from 'vite';
import { beforeAll, describe, expect, it } from 'vitest';

import plugin from '../../../src/index';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

let result;
let jsFiles;
let associationJsonContent;

describe('simple-association', () => {
  beforeAll(async () => {
    result = await build({
      plugins: [
        plugin({
          pluginKey: 'com.atlassian.plugin.test',
          packageName: '@atlassian/vite-webresource-plugin.tests.associations-simple',
        }),
      ],
      build: {
        outDir: OUTPUT_DIR,
        assetsDir: '.',
        rollupOptions: {
          input: path.resolve(FRONTEND_SRC_DIR, 'index.js'),
        },
      },
    });
    jsFiles = result.output.filter((file) => file.fileName.endsWith('.js')).map((f) => f.fileName);

    const jsonPath = path.join(
      OUTPUT_DIR,
      'META-INF/fe-manifest-associations/com.atlassian.plugin.test-vite.intermediary.json',
    );
    associationJsonContent = JSON.parse(fs.readFileSync(jsonPath, 'utf-8'));
  });

  it('association file structure is correct', () => {
    expect(associationJsonContent).toHaveProperty('packageName');
    expect(typeof associationJsonContent['packageName']).toBe('string');
    expect(associationJsonContent).toHaveProperty('outputDirectoryFiles');
    expect(Array.isArray(associationJsonContent['outputDirectoryFiles'])).toBeTruthy();
  });

  it('association package name is taken from config', () => {
    expect(associationJsonContent.packageName).toEqual('@atlassian/vite-webresource-plugin.tests.associations-simple');
  });

  it('association file includes relatives paths for files compiled through entries (exclusively)', () => {
    expect(associationJsonContent.outputDirectoryFiles).toEqual(expect.arrayContaining(jsFiles));
    expect(associationJsonContent.outputDirectoryFiles.length).toEqual(1);
  });
});
