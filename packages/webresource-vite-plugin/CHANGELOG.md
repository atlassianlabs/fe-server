# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/webresource-vite-plugin@0.2.0..@atlassian/webresource-vite-plugin@0.1.1) (2024-07-19)


### Features

* using proper manifest name for vite ([2ffea5e](https://bitbucket.org/atlassianlabs/fe-server/commits/2ffea5e34d775426779b401c926fa135e24ce757))



### [0.1.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/webresource-vite-plugin@0.1.1..@atlassian/webresource-vite-plugin@0.1.0) (2024-07-18)

**Note:** Version bump only for package @atlassian/webresource-vite-plugin





## 0.1.0 (2024-07-16)


### Features

* Initial public release for Vite WRM plugin ([226226d](https://bitbucket.org/atlassianlabs/fe-server/commits/226226dca4b3d8d1dc9300c8f92fc9f7621a09dd))
