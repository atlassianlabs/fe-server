import runtimeLoadShim from '../../src/shims/runtime-load-shim';

const linePrefix = '/******/ ';
const templateShim = {
  tabs: 4,
  generate: runtimeLoadShim,
};

export const asyncChunkLoader = (pluginKey: string) => {
  const { generate, tabs } = templateShim;
  const templateCode = generate(pluginKey, false, '').trim();
  return (
    templateCode
      .split('\n')
      // webpack indents empty lines with only have the tabs of non-empty lines
      .map((line) => linePrefix + '\t'.repeat(line !== '' ? tabs : tabs / 2) + line)
      .join('\n')
  );
};
