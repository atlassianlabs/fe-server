import type { Node } from 'xml-parser';

const getByName = (nodes: Node[], key: string) => {
  return nodes.filter((n) => n.name === key) as Node[];
};

export const getDependencies = (node: Node) => {
  return getByName(node.children, 'dependency');
};

export const getTransformations = (node: Node) => {
  return getByName(node.children, 'transformation');
};

export const getDataProviders = (node: Node) => {
  return getByName(node.children, 'data');
};

export const getResources = (node: Node) => {
  return getByName(node.children, 'resource');
};

export const getWebResources = (node: Node) => {
  return getByName(node.children, 'web-resource');
};

export const getContext = (node: Node) => {
  return getByName(node.children, 'context');
};

export const getContent = (nodes: Node[]) => {
  return nodes.map((n) => n.content) as string[];
};

export const findByExtension = (nodes: Node[], extname: string) => {
  return nodes.find((transformation) => transformation.attributes.extension === extname) as Node;
};

export const getByKey = (nodes: Node[], key: string): Node => {
  return nodes.find((node) => node.attributes.key === key) as Node;
};

export const getByKeyStarsWith = (nodes: Node[], key: string) => {
  return nodes.find((node) => node.attributes.key.startsWith(key)) as Node;
};

export const getByKeyEndsWith = (nodes: Node[], key: string) => {
  return nodes.find((node) => node.attributes.key.endsWith(key)) as Node;
};

export const findKeyLike = (nodes: Node[], needle: string) => {
  return nodes.find((n) => n.attributes.key.indexOf(needle) > -1) as Node;
};

export const byExtension = (resources: Node[], ext: string) =>
  resources.filter((r) => r.attributes.name.endsWith(ext))[0];
