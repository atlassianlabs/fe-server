import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import parse from 'xml-parser';

import { getByKeyStarsWith, getResources } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('asset-loading-via-js', () => {
  let stats: Stats;
  let assets: parse.Node;
  let resources: parse.Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      assets = getByKeyStarsWith(results.root.children, 'assets');
      resources = getResources(assets!);
      done();
    });
  });

  it('should create an "asset"-webresource containing the asset', () => {
    expect(assets).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('should add all assets to the "asset"-webresource', () => {
    const resourceNames = resources.map((r) => r.attributes.name);
    expect(resourceNames).toEqual(expect.arrayContaining(['compiled-ice.png', 'compiled-rect.svg']));
  });

  it('should overwrite webpack output path to point to a wrm-resource', () => {
    // setup
    const bundleFile = fs.readFileSync(path.join(targetDir, 'app.js'), 'utf-8');
    const publicPathLines = bundleFile.match(/__webpack_require__\.p\s*?=.+?;/g)!;
    const injectedLine = publicPathLines.find((line) => line.includes('download'))!;

    expect(injectedLine).toBeTruthy();
    expect(injectedLine.startsWith('__webpack_require__.p = AJS.contextPath()')).toBeTruthy();
    expect(
      injectedLine.endsWith(`/download/resources/com.atlassian.plugin.test:${assets.attributes.key}/";`),
    ).toBeTruthy();
  });
});
