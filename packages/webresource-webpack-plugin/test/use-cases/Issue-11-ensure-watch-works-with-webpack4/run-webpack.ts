import webpack from 'webpack';

import config from './webpack.config';

process.on('uncaughtRejection', (err) => {
  console.log('WEBPACK FAILED UNEXPECTEDLY - FAILING YOUR TESTS WITH 19', err);
  // eslint-disable-next-line no-process-exit
  process.exit(19);
});

process.on('uncaughtException', (err) => {
  console.log('WEBPACK FAILED UNEXPECTEDLY - FAILING YOUR TESTS WITH 19', err);
  // eslint-disable-next-line no-process-exit
  process.exit(19);
});

webpack(config, (err) => {
  if (err) {
    console.log('WEBPACK FAILED UNEXPECTEDLY - FAILING YOUR TESTS WITH 19', err);
    // eslint-disable-next-line no-process-exit
    process.exit(19);
  }
});
