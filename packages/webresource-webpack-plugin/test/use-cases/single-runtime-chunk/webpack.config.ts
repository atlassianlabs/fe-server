import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'target');

const config = (
  // eslint-disable-next-line @typescript-eslint/ban-types
  runtimeChunk: string | boolean | object,
  webresourceOutput: string,
  singleRuntimeWebResourceKey?: string,
): Configuration => ({
  mode: 'development',
  context: FRONTEND_SRC_DIR,
  entry: {
    first: path.join(FRONTEND_SRC_DIR, 'first.js'),
    second: path.join(FRONTEND_SRC_DIR, 'second.js'),
    third: path.join(FRONTEND_SRC_DIR, 'third.js'),
  },
  optimization: {
    // @ts-expect-error We know we dont perfectly match the type. Its fine.
    runtimeChunk,
    splitChunks: {
      minSize: 0,
      chunks: 'all',
    },
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      xmlDescriptors: webresourceOutput,
      verbose: false,
      singleRuntimeWebResourceKey,
    }),
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(OUTPUT_DIR),
  },
});

export default config;
