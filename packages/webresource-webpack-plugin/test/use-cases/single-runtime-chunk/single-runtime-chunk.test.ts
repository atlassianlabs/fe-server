import fs from 'fs';
import path from 'path';
import type { Configuration } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { setBaseDependencies } from '../../../src/deps/base-dependencies';
import { getByKey, getDependencies, getResources, getWebResources } from '../../util/xml-parser';
import baseConfig from './webpack.config';

const RUNTIME_WR_KEY = 'common-runtime';
const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-single.xml');

describe('single runtime chunk', () => {
  const PLUGIN_KEY = 'com.atlassian.plugin.test';
  const BASE_DEPS = ['base.context:dep1', 'base.context:dep2'];

  function runTheTestsFor(config: Configuration, runtimeName: string, runtimeWrKeyOpt?: string) {
    let wrNodes: Node[];
    const runtimeWrKey = runtimeWrKeyOpt || RUNTIME_WR_KEY;

    beforeEach(function (done) {
      setBaseDependencies(BASE_DEPS);

      webpack(config, () => {
        const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
        const results = parse(xmlFile);
        wrNodes = results.root.children.filter((n) => n.name === 'web-resource');
        done();
      });
    });

    it('creates a web-resource for the runtime', () => {
      const wrKeys = wrNodes.map((n) => n.attributes.key);
      expect(wrKeys).toContain(runtimeWrKey);
    });

    it('adds the runtime to the dedicated web-resource for it', () => {
      const runtimeWR = getByKey(wrNodes, runtimeWrKey);
      const runtimeResources = getResources(runtimeWR);
      const runtimeResourceLocations = runtimeResources.map((n) => n.attributes.location);
      expect(runtimeResources).toHaveLength(1);
      expect(runtimeResourceLocations[0]).toEqual(runtimeName);
    });

    it('adds base dependencies to the runtime web-resource', () => {
      const runtimeWR = getByKey(wrNodes, runtimeWrKey);
      const dependencies = getDependencies(runtimeWR);
      const dependencyNames = dependencies.map((d) => d.content);
      expect(dependencyNames).toEqual(expect.arrayContaining(BASE_DEPS));
    });

    it('does not add the runtime to more than one web-resource', () => {
      const allResourceNodes = wrNodes.flatMap(getResources);
      const allResourceLocations = allResourceNodes.map((n) => n.attributes.location);
      const runtimeCount = allResourceLocations.filter((loc) => loc === runtimeName);
      expect(runtimeCount).toHaveLength(1);
    });

    it('adds a dependency on the runtime to each entrypoint web-resource', () => {
      const entrypoints = wrNodes.filter((n) => n.attributes.key.startsWith('entry'));
      entrypoints.forEach((entry) => {
        const dependencies = getDependencies(entry);
        const dependencyNames = dependencies.map((d) => d.content);
        expect(dependencyNames).toContain(`${PLUGIN_KEY}:${runtimeWrKey}`);
      });
    });
  }

  describe('when configured as "single"', () => {
    const config = baseConfig('single', webresourceOutput);

    runTheTestsFor(config, 'runtime.js');
  });

  describe('when configured with a static name', () => {
    const name = 'custom';
    const config = baseConfig({ name }, webresourceOutput);

    runTheTestsFor(config, `${name}.js`);
  });

  describe('when configured with a name and custom output format', () => {
    const name = 'fun';
    const config = baseConfig({ name }, webresourceOutput);
    config.output!.filename = 'prefixed.[name].suffixed.js';

    runTheTestsFor(config, `prefixed.${name}.suffixed.js`);
  });

  describe('when configured with a web-resource key', () => {
    const name = 'custom';
    const webresourceKey = 'foobar';
    const config = baseConfig({ name }, webresourceOutput, webresourceKey);

    runTheTestsFor(config, `${name}.js`, webresourceKey);
  });

  describe('when not configured', () => {
    const config = baseConfig(false, webresourceOutput);
    let wrNodes: Node[];

    beforeEach(function (done) {
      webpack(config, () => {
        const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
        const results = parse(xmlFile);
        wrNodes = getWebResources(results.root);
        done();
      });
    });

    it('does not create a web-resource for the runtime', () => {
      const wrKeys = wrNodes.map((n) => n.attributes.key);
      expect(wrKeys).not.toContain(RUNTIME_WR_KEY);
    });
  });
});
