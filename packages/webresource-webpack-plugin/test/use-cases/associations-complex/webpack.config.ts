import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import fs from 'fs';
import path from 'path';
import type { Configuration } from 'webpack';

const FRONTEND_SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'target/classes');

const DIRECT_COPY_FILE = 'copied-file-should-be-ignored.js';

const config: Configuration = {
  mode: 'development',
  context: FRONTEND_SRC_DIR,
  entry: {
    'simple-entry': path.join(FRONTEND_SRC_DIR, 'entry.js'),
    'simple-entry-2': path.join(FRONTEND_SRC_DIR, 'entry2.js'),
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 0,
    },
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      contextMap: { 'simple-entry': [''] },
      addEntrypointNameAsContext: true,
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      verbose: false,
    }),
    {
      apply: (compiler) => {
        compiler.hooks.compilation.tap('Directly Copy JS File Plugin', (compilation) => {
          compilation.hooks.beforeCodeGeneration.tap('Copy', () => {
            const sourceFile = path.join(compiler.context, DIRECT_COPY_FILE);
            const destinationFile = path.join(compiler.outputPath, DIRECT_COPY_FILE);

            // Otherwise hangs
            fs.mkdirSync(path.dirname(destinationFile), { recursive: true });

            fs.copyFileSync(sourceFile, destinationFile);
          });
        });
      },
    },
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(OUTPUT_DIR),
  },
};

export default config;
