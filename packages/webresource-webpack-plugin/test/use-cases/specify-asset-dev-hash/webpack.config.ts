import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'target');

const webResourcePlugin = (prefix: string) => {
  return new WrmPlugin({
    pluginKey: 'com.atlassian.plugin.test',
    xmlDescriptors: path.join(__dirname, 'target', 'META-INF', 'plugin-descriptor', `${prefix}.xml`),
    webresourceKeyMap: {
      feature: `${prefix}Resource`,
    },
    devAssetsHash: `${prefix}DevAssets`,
  });
};

const webpackConfigForLibrary: Configuration = {
  name: 'Library',
  context: __dirname,
  entry: {
    library: path.join(FRONTEND_SRC_DIR, 'library.js'),
  },
  output: {
    library: '[name]',
    libraryTarget: 'amd',
    filename: '[name].js',
    path: path.resolve(OUTPUT_DIR),
  },
  plugins: [webResourcePlugin('lib')],
};

const webpackConfigForFeature: Configuration = {
  name: 'Feature',
  context: __dirname,
  mode: 'development',
  entry: {
    feature: path.join(FRONTEND_SRC_DIR, 'feature.js'),
  },
  output: {
    filename: '[name].js',
    path: path.resolve(OUTPUT_DIR),
  },
  plugins: [webResourcePlugin('feat')],
};

export default [webpackConfigForLibrary, webpackConfigForFeature] as [Configuration, Configuration];
