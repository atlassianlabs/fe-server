import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { findByExtension, findKeyLike, getTransformations } from '../../util/xml-parser';
import config from './webpack.extend-tranformations.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('specify-transformation', () => {
  let wrNodes: Node[];

  beforeEach((done) => {
    webpack(config, () => {
      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrNodes = results.root.children;
      done();
    });
  });

  describe('extending transformations', () => {
    let entryJsTrans: Node;
    let lessTrans: Node;
    let svgTrans: Node;

    beforeEach(() => {
      const entrypointTransformations = getTransformations(findKeyLike(wrNodes, 'app-one'));
      const assetTransformations = getTransformations(findKeyLike(wrNodes, 'assets'));

      if (!entrypointTransformations) {
        throw new Error("Webpack compilation failed. Can't find entrypoints transformations.");
      }

      if (!assetTransformations) {
        throw new Error("Webpack compilation failed. Can't find assets transformations.");
      }

      entryJsTrans = findByExtension(entrypointTransformations, 'js');

      lessTrans = findByExtension(assetTransformations, 'less');
      svgTrans = findByExtension(assetTransformations, 'svg');
    });

    it('should include default transformations', () => {
      expect(entryJsTrans.children.map((c) => c.attributes.key)).toContain('jsI18n');
    });

    it('should contain additional transformers', () => {
      expect(entryJsTrans.children.map((c) => c.attributes.key)).toContain('custom-transformer');
      expect(entryJsTrans.children.map((c) => c.attributes.key)).toContain('foo-transformer');

      expect(lessTrans.children.map((c) => c.attributes.key)).toContain('less-transformer');

      expect(svgTrans.children.map((c) => c.attributes.key)).toContain('bar');
    });
  });
});
