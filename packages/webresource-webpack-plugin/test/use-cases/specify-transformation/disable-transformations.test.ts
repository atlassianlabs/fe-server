import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { findByExtension, findKeyLike, getTransformations } from '../../util/xml-parser';
import config from './webpack.disable-tranformations.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('specify-transformation', () => {
  let wrNodes: Node[];

  beforeEach((done) => {
    webpack(config, () => {
      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrNodes = results.root.children;
      done();
    });
  });

  describe('disabling transformations', () => {
    let entryJsTrans: Node;
    let entrySoyTrans: Node;

    let jsTrans: Node;
    let htmlTrans: Node;
    let soyTrans: Node;
    let svgTrans: Node;
    let txtTrans: Node;

    beforeEach(() => {
      const entrypointTransformations = getTransformations(findKeyLike(wrNodes, 'app-one'));
      const assetTransformations = getTransformations(findKeyLike(wrNodes, 'assets'));

      if (!entrypointTransformations) {
        throw new Error("Webpack compilation failed. Can't find entrypoints transformations.");
      }

      if (!assetTransformations) {
        throw new Error("Webpack compilation failed. Can't find assets transformations.");
      }

      entryJsTrans = findByExtension(entrypointTransformations, 'js');
      entrySoyTrans = findByExtension(entrypointTransformations, 'soy');

      jsTrans = findByExtension(assetTransformations, 'js');
      htmlTrans = findByExtension(assetTransformations, 'html');
      soyTrans = findByExtension(assetTransformations, 'soy');
      svgTrans = findByExtension(assetTransformations, 'svg');
      txtTrans = findByExtension(assetTransformations, 'txt');
    });

    it('should disable all transformations', () => {
      expect(entryJsTrans).toBeUndefined();
      expect(entrySoyTrans).toBeUndefined();

      expect(jsTrans).toBeUndefined();
      expect(htmlTrans).toBeUndefined();
      expect(soyTrans).toBeUndefined();
      expect(svgTrans).toBeUndefined();
      expect(txtTrans).toBeUndefined();
    });
  });
});
