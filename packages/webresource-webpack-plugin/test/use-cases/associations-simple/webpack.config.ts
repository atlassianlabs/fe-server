import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import path from 'path';
import type { Configuration } from 'webpack';

const FRONTEND_SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'target/classes');

const config: Configuration = {
  mode: 'development',
  context: FRONTEND_SRC_DIR,
  entry: {
    'simple-entry': path.join(FRONTEND_SRC_DIR, 'simple.js'),
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      packageName: '@atlassian/webpack-webresource-plugin.tests.associations-simple',
      contextMap: { 'simple-entry': [''] },
      addEntrypointNameAsContext: true,
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      verbose: false,
    }),
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(OUTPUT_DIR),
  },
};

export default config;
