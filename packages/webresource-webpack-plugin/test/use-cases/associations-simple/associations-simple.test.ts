import fs from 'fs';
import path from 'path';
import webpack from 'webpack';

import config from './webpack.config';

const targetDir = path.join(__dirname, 'target/classes');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('simple case with association', () => {
  beforeAll((done) => {
    webpack(config, (err, stats) => {
      if (err) {
        throw err;
      }

      if (stats!.hasErrors() || stats!.hasWarnings()) {
        throw Error('Webpack build failed.');
      }

      done();
    });
  });

  it('compiles an xml file', () => {
    expect(fs.existsSync(webresourceOutput)).toEqual(true);
  });

  describe('association file', () => {
    let associationJsonContent: Record<string, string>;

    beforeAll(() => {
      const jsonPath = path.join(
        targetDir,
        'META-INF/fe-manifest-associations/com.atlassian.plugin.test-webpack.intermediary.json',
      );

      const isJsonExisting = fs.existsSync(jsonPath);
      if (!isJsonExisting) {
        throw new Error('Association JSON was not created.');
      }

      associationJsonContent = JSON.parse(fs.readFileSync(jsonPath, 'utf-8'));
    });

    it('association file structure is correct', () => {
      expect(associationJsonContent).toHaveProperty('packageName');
      expect(typeof associationJsonContent['packageName']).toBe('string');
      expect(associationJsonContent).toHaveProperty('outputDirectoryFiles');
      expect(Array.isArray(associationJsonContent['outputDirectoryFiles'])).toBeTruthy();
    });

    it('association package name is taken from config', () => {
      expect(associationJsonContent.packageName).toEqual(
        '@atlassian/webpack-webresource-plugin.tests.associations-simple',
      );
    });

    it('association file includes relatives paths for files compiled through entries (exclusively)', () => {
      expect(associationJsonContent.outputDirectoryFiles).toContain('simple-entry.js');
      expect(associationJsonContent.outputDirectoryFiles.length).toEqual(1);
    });
  });
});
