import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'target');

const getConfigWithMiniCssPlugin = (miniCssPlugin?: MiniCssExtractPlugin): Configuration => ({
  mode: 'production',
  entry: {
    main: path.join(FRONTEND_SRC_DIR, 'index.js'),
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [...(miniCssPlugin ? [MiniCssExtractPlugin.loader] : []), 'css-loader'],
      },
    ],
  },
  output: {
    path: OUTPUT_DIR,
    filename: '[name]-bundle.js',
  },
  plugins: [
    ...(miniCssPlugin ? [miniCssPlugin] : []),
    new WrmPlugin({
      verbose: true,
      pluginKey: 'com.refined.labs.webpack-5-test',
      addEntrypointNameAsContext: false,
      contextMap: {
        main: ['atl.general', 'atl.admin'],
      },
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
    }),
  ],
});

export { getConfigWithMiniCssPlugin };
