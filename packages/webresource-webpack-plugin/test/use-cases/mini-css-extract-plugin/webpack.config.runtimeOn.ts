import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import { getConfigWithMiniCssPlugin } from './webpack.config';

export const configRuntimeOn = getConfigWithMiniCssPlugin(new MiniCssExtractPlugin({ runtime: true }));
