import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import { getConfigWithMiniCssPlugin } from './webpack.config';

export const configRuntimeOff = getConfigWithMiniCssPlugin(new MiniCssExtractPlugin({ runtime: false }));
