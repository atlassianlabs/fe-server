AJS.toInit(() => {
  // Dynamic import results in looking for the wrong generated web-resource key.
  import(/* webpackChunkName: "dynamic-styles" */ './styles.css');
});
