import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import parse from 'xml-parser';

import { getResources, getWebResources } from '../../util/xml-parser';
import { configRuntimeNoMiniCssPlugin } from './webpack.config.noMiniCssExtractPlugin';
import { configRuntimeOff } from './webpack.config.runtimeOff';
import { configRuntimeOn } from './webpack.config.runtimeOn';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

const DYNAMIC_STYLES_CHUNK = 'dynamic-styles.css';

describe('mini-css-extract-plugin', () => {
  it('should compile with runtime off', (done) => {
    webpack(configRuntimeOff, (err, stats) => {
      if (err) {
        throw err;
      }
      expect(stats!.hasErrors()).toBe(false);
      expect(stats!.hasWarnings()).toBe(false);
      expect(fs.existsSync(webresourceOutput)).toBe(true);
      expect(fs.existsSync(path.join(targetDir, 'dynamic-styles.css'))).toBe(true);

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      const webResources = getWebResources(results.root);

      const webResourceWithExtractedCss = webResources
        .flatMap(getResources)
        .find((child) => child.attributes.name === DYNAMIC_STYLES_CHUNK);
      expect(webResourceWithExtractedCss).toBeTruthy();

      done();
    });
  });

  it('should warn about runtime enabled', (done) => {
    webpack(configRuntimeOn, (err, stats) => {
      if (err) {
        throw err;
      }
      expect(stats!.hasErrors()).toBe(false);
      expect(stats!.hasWarnings()).toBe(true);
      expect(stats!.toJson().warnings![0].message).toBe(
        "MiniCssExtractPlugin runtime option needs to be disabled so that it doesn't interfere with Web Resource Manager runtime.",
      );

      done();
    });
  });

  it('should not warn if no MiniCssExtractPlugin', (done) => {
    webpack(configRuntimeNoMiniCssPlugin, (err, stats) => {
      if (err) {
        throw err;
      }
      expect(stats!.hasErrors()).toBe(false);
      expect(stats!.hasWarnings()).toBe(false);

      done();
    });
  });
});
