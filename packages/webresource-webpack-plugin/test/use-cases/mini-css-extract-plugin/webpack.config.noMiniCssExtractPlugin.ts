import { getConfigWithMiniCssPlugin } from './webpack.config';

export const configRuntimeNoMiniCssPlugin = getConfigWithMiniCssPlugin();
