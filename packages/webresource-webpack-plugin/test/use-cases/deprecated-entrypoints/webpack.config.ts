import path from 'path';
import type { Configuration } from 'webpack';

import type { DeprecationInfo } from '../../../src/types/types';
import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

const config: (deprecatedValue: DeprecationInfo) => Configuration = (deprecatedValue: DeprecationInfo) => ({
  mode: 'development',
  entry: {
    deprecated: path.join(FRONTEND_SRC_DIR, 'deprecated-entrypoint.js'),
    replacement: path.join(FRONTEND_SRC_DIR, 'replacement-entrypoint.js'),
    'mapped-replacement': path.join(FRONTEND_SRC_DIR, 'replacement-entrypoint.js'),
    'fancy-mapped-replacement': path.join(FRONTEND_SRC_DIR, 'replacement-entrypoint.js'),
    'named-replacement': path.join(FRONTEND_SRC_DIR, 'replacement-entrypoint.js'),
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'test.atlassian.foobar',
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      verbose: false,
      deprecatedEntrypoints: {
        deprecated: deprecatedValue,
      },
      webresourceKeyMap: {
        'mapped-replacement': 'mapped-resource',
        'fancy-mapped-replacement': {
          key: 'fancy-mapped-resource',
          name: 'Fancy Mapped Resource',
        },
        'named-replacement': {
          name: 'Named Replacement Resource',
        },
      },
    }),
  ],

  output: {
    filename: '[name].js',
    path: OUTPUT_DIR,
  },
});

export default config;
