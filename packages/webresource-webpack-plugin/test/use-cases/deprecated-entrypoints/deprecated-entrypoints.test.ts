import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import type { DeprecationInfo } from '../../../src/types/types';
import { getByKey } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

/* eslint-disable jest/no-identical-title --
 * Because this test suite generates new describe declarations, the jest/no-identical-title rule will detect false positives.
 **/
describe('deprecated-entrypoints', () => {
  type FoundEntrypointsCallback = (getEntrypointNamed: (entrypointName: string) => Node | undefined) => void;

  function setupWebpackBuild(deprecatedValue: DeprecationInfo): Node[] {
    const entrypoints: Node[] = [];
    beforeAll((done) => {
      webpack(config(deprecatedValue), () => {
        const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
        const results = parse(xmlFile);
        entrypoints.push(...results.root.children.filter((n) => n.attributes.key.startsWith('entry'))!);
        done();
      });
    });
    return entrypoints;
  }

  function describeDeprecatedValue(deprecatedValue: DeprecationInfo, callback: FoundEntrypointsCallback) {
    describe('the entrypoint with "deprecated" set to: `' + JSON.stringify(deprecatedValue) + '`', () => {
      const entrypoints: Node[] = setupWebpackBuild(deprecatedValue);
      callback((entrypointName: string) => getByKey(entrypoints, `entrypoint-${entrypointName}`));
    });
  }

  [true, {}].forEach((deprecatedValue) => {
    describeDeprecatedValue(deprecatedValue, (getEntrypointNamed) => {
      it('should contain a single <deprecated> element', () => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
      });

      describe('the <deprecated> element', () => {
        let deprecatedNode: Node | undefined;
        beforeAll(() => {
          const deprecatedEntrypoint = getEntrypointNamed('deprecated');
          deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
        });
        it('should be present', () => {
          expect(deprecatedNode).not.toBeUndefined();
        });
        it('should have no attributes', () => {
          expect(Object.keys(deprecatedNode!.attributes)).toHaveLength(0);
        });
        it('should have no children', () => {
          expect(deprecatedNode!.children).toHaveLength(0);
        });
      });
    });
  });

  describeDeprecatedValue({ sinceVersion: '9.0' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should be present', () => {
        expect(deprecatedNode).not.toBeUndefined();
      });
      it('should have a single attribute', () => {
        expect(Object.keys(deprecatedNode!.attributes)).toHaveLength(1);
      });
      it('should have a "since" attribute with the value "9.0"', () => {
        expect(deprecatedNode!.attributes.since).toBe('9.0');
      });
    });
  });

  describeDeprecatedValue({ removeInVersion: '10.0' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should be present', () => {
        expect(deprecatedNode).not.toBeUndefined();
      });
      it('should have a single attribute', () => {
        expect(Object.keys(deprecatedNode!.attributes)).toHaveLength(1);
      });
      it('should have a "remove" attribute with the value "10.0"', () => {
        expect(deprecatedNode!.attributes.remove).toBe('10.0');
      });
    });
  });

  describeDeprecatedValue({ alternative: 'test.atlassian:something-else' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should be present', () => {
        expect(deprecatedNode).not.toBeUndefined();
      });
      it('should have a single attribute', () => {
        expect(Object.keys(deprecatedNode!.attributes)).toHaveLength(1);
      });
      it('should have an "alternative" specifying an alternative web resource', () => {
        expect(deprecatedNode!.attributes.alternative).toBe('test.atlassian:something-else');
      });
    });
  });

  describeDeprecatedValue({ alternative: 'replacement' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should have an "alternative" using the configured plugin key', () => {
        expect(deprecatedNode!.attributes.alternative).toBe('test.atlassian.foobar:entrypoint-replacement');
      });
    });
  });

  describeDeprecatedValue({ alternative: 'mapped-replacement' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should have an "alternative" using the configured plugin key', () => {
        expect(deprecatedNode!.attributes.alternative).toBe('test.atlassian.foobar:mapped-resource');
      });
    });
  });

  describeDeprecatedValue({ alternative: 'fancy-mapped-replacement' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should have an "alternative" using the configured plugin key', () => {
        expect(deprecatedNode!.attributes.alternative).toBe('test.atlassian.foobar:fancy-mapped-resource');
      });
    });
  });

  describeDeprecatedValue({ alternative: 'named-replacement' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should have an "alternative" using the configured plugin key', () => {
        expect(deprecatedNode!.attributes.alternative).toBe('test.atlassian.foobar:entrypoint-named-replacement');
      });
    });
  });

  describeDeprecatedValue({ alternative: 'some-other-resource' }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should have an "alternative" using the configured plugin key', () => {
        expect(deprecatedNode!.attributes.alternative).toBe('test.atlassian.foobar:some-other-resource');
      });
    });
  });

  describeDeprecatedValue({ extraInfo: "This one isn't written in Rust." }, (getEntrypointNamed) => {
    it('should contain a single <deprecated> element', () => {
      const deprecatedEntrypoint = getEntrypointNamed('deprecated');
      expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
    });

    describe('the <deprecated> element', () => {
      let deprecatedNode: Node | undefined;
      beforeAll(() => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
      });
      it('should be present', () => {
        expect(deprecatedNode).not.toBeUndefined();
      });
      it('should have no attributes', () => {
        expect(Object.keys(deprecatedNode!.attributes)).toHaveLength(0);
      });
      it('should have a child text node giving more information about the deprecation', () => {
        expect(deprecatedNode!.content).toBe("This one isn't written in Rust.");
      });
    });
  });

  describeDeprecatedValue(
    { sinceVersion: '5', removeInVersion: '6', alternative: 'replacement', extraInfo: 'foobar' },
    (getEntrypointNamed) => {
      it('should contain a single <deprecated> element', () => {
        const deprecatedEntrypoint = getEntrypointNamed('deprecated');
        expect(deprecatedEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(1);
      });

      describe('the <deprecated> element', () => {
        let deprecatedNode: Node | undefined;
        beforeAll(() => {
          const deprecatedEntrypoint = getEntrypointNamed('deprecated');
          deprecatedNode = deprecatedEntrypoint!.children.find((n) => n.name === 'deprecated');
        });
        it('should be present', () => {
          expect(deprecatedNode).not.toBeUndefined();
        });
        it('should have three attributes', () => {
          expect(Object.keys(deprecatedNode!.attributes)).toHaveLength(3);
        });
        it('should have a "since" attribute with the value "5"', () => {
          expect(deprecatedNode!.attributes.since).toBe('5');
        });
        it('should have a "remove" attribute with the value "6"', () => {
          expect(deprecatedNode!.attributes.remove).toBe('6');
        });
        it('should have an "alternative" using the configured plugin key', () => {
          expect(deprecatedNode!.attributes.alternative).toBe('test.atlassian.foobar:entrypoint-replacement');
        });
        it('should have a child text node giving more information about the deprecation', () => {
          expect(deprecatedNode!.content).toBe('foobar');
        });
      });
    },
  );

  describe('a non-deprecated entrypoint', () => {
    const entrypoints = setupWebpackBuild(true);

    it('should not have a deprecated entrypoint', () => {
      const replacementEntrypoint = getByKey(entrypoints, 'entrypoint-replacement');
      expect(replacementEntrypoint!.children.filter((n) => n.name === 'deprecated')).toHaveLength(0);
    });
  });
});
/* eslint-enable jest/no-identical-title */
