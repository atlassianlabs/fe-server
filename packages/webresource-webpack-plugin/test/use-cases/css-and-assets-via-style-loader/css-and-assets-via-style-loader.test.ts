import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKeyStarsWith } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('css-and-assets-via-style-loader', () => {
  let stats: Stats;
  let assets: Node;
  let resources: Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;
      if (stats.hasErrors()) {
        throw new Error('Webpack compilation failed');
      }

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      assets = getByKeyStarsWith(results.root.children, 'assets');
      resources = assets.children.filter((n) => n.name === 'resource')!;
      done();
    });
  });

  it('should create an "asset"-webresource containing the image referenced in the stylesheet', () => {
    expect(resources[0].attributes.type).toEqual('download');
    expect(path.extname(resources[0].attributes.name)).toEqual('.png');
  });

  it('should interpolate the CSS rules in to the JS code', () => {
    // setup
    const bundleFile = fs.readFileSync(path.join(targetDir, 'app.js'), 'utf-8');
    const expectedLine = /const html = `<div class="\$\{(.*?)\}"><div class="\$\{(.*?)\}"><\/div><\/div>`;/;

    const result = expectedLine.exec(bundleFile)!;

    expect(result).toHaveLength(3);
    expect(result[1]).toContain('.wurst');
    expect(result[2]).toContain('.tricky');
  });
});
