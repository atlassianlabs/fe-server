import path from 'path';
import type { Chunk, Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.join(__dirname, 'src');
const OUTPUT_DIR = path.join(__dirname, 'target');

const providedDependencies = new Map();
providedDependencies.set('jquery', {
  dependency: 'jira.webresources:jquery',
  import: "require('jquery')",
});
providedDependencies.set('underscore', {
  dependency: 'com.atlassian.plugin.jslibs:underscore-1.4.4',
  import: "require('underscore')",
});

const config: Configuration = {
  mode: 'development',
  devtool: false,
  entry: {
    app: path.join(FRONTEND_SRC_DIR, 'app.js'),
  },
  optimization: {
    runtimeChunk: true,
    chunkIds: 'named',
    moduleIds: 'named',
    splitChunks: {
      name(module: unknown, chunks: Chunk[]) {
        return chunks.map((item) => item.name).join('~');
      },
      minSize: 0,
      chunks: 'all',
    },
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      providedDependencies,
      verbose: false,
    }),
  ],
  output: {
    filename: '[name].js',
    path: OUTPUT_DIR,
  },
};
export default config;
