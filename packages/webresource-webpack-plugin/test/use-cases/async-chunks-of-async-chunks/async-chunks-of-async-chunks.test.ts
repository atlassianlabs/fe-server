import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { asyncChunkLoader } from '../../fixtures/webpack-runtime-chunks';
import { getByKey, getByKeyStarsWith, getContent, getDependencies } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('async-chunks-of-async-chunks', () => {
  let stats: Stats;
  let runtime: Node;
  let app: Node;
  let asyncBar: Node;
  let asyncFoo: Node;
  let asyncAsyncBar: Node;
  let asyncAsyncBarTwo: Node;
  let asyncAsyncAsyncBar: Node;

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      runtime = getByKeyStarsWith(results.root.children, 'entry');
      app = getByKey(results.root.children, 'split_app');
      asyncBar = getByKey(results.root.children, 'async-bar');
      asyncFoo = getByKey(results.root.children, 'async-foo');
      asyncAsyncBar = getByKey(results.root.children, 'async-async-bar');
      asyncAsyncBarTwo = getByKey(results.root.children, 'async-async-bar-two');
      asyncAsyncAsyncBar = getByKey(results.root.children, 'async-async-async-bar');
      done();
    });
  });

  it('should create a webresource for each async chunk', () => {
    expect(runtime).toBeTruthy();
    expect(app).toBeTruthy();
    expect(asyncBar).toBeTruthy();
    expect(asyncFoo).toBeTruthy();
    expect(asyncAsyncBar).toBeTruthy();
    expect(asyncAsyncBarTwo).toBeTruthy();
    expect(asyncAsyncAsyncBar).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('should inject a WRM pre-condition checker into the webpack runtime', () => {
    // setup
    const bundleFile = fs.readFileSync(path.join(targetDir, 'runtime~app.js'), 'utf-8');
    const expectedRuntimeAdjustment = asyncChunkLoader('com.atlassian.plugin.test');

    expect(bundleFile).toContain(expectedRuntimeAdjustment);
  });

  it('adds shared provided dependencies only to the entry point', () => {
    const appDeps = getContent(getDependencies(app));
    const asyncBarDeps = getContent(getDependencies(asyncBar));
    const asyncFooDeps = getContent(getDependencies(asyncFoo));
    const asyncAsyncBarDeps = getContent(getDependencies(asyncAsyncBar));
    const asyncAsyncBarTwoDeps = getContent(getDependencies(asyncAsyncBarTwo));
    const asyncAsyncAsyncBarDeps = getContent(getDependencies(asyncAsyncAsyncBar));

    expect(appDeps.includes('com.atlassian.plugin.jslibs:underscore-1.4.4')).toBeTruthy();
    expect(asyncBarDeps.includes('com.atlassian.plugin.jslibs:underscore-1.4.4')).not.toEqual(true);
    expect(asyncFooDeps.includes('com.atlassian.plugin.jslibs:underscore-1.4.4')).not.toEqual(true);
    expect(asyncAsyncBarDeps.includes('com.atlassian.plugin.jslibs:underscore-1.4.4')).not.toEqual(true);
    expect(asyncAsyncBarTwoDeps.includes('com.atlassian.plugin.jslibs:underscore-1.4.4')).not.toEqual(true);
    expect(asyncAsyncAsyncBarDeps.includes('com.atlassian.plugin.jslibs:underscore-1.4.4')).not.toEqual(true);
  });

  it('adds async-chunk-only deps only to the async-chunk-webresource', () => {
    const entryDeps = getContent(getDependencies(app));
    const asyncBarDeps = getContent(getDependencies(asyncBar));
    const asyncFooDeps = getContent(getDependencies(asyncFoo));
    const asyncAsyncBarDeps = getContent(getDependencies(asyncAsyncBar));
    const asyncAsyncBarTwoDeps = getContent(getDependencies(asyncAsyncBarTwo));
    const asyncAsyncAsyncBarDeps = getContent(getDependencies(asyncAsyncAsyncBar));

    expect(entryDeps.includes('jira.webresources:jquery')).not.toEqual(true);
    expect(asyncBarDeps.includes('jira.webresources:jquery')).toBeTruthy();
    expect(asyncFooDeps.includes('jira.webresources:jquery')).not.toEqual(true);
    expect(asyncAsyncBarDeps.includes('jira.webresources:jquery')).not.toEqual(true);
    expect(asyncAsyncBarTwoDeps.includes('jira.webresources:jquery')).not.toEqual(true);
    expect(asyncAsyncAsyncBarDeps.includes('jira.webresources:jquery')).not.toEqual(true);
  });
});
