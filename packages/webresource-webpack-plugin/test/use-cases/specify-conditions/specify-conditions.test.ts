import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { findKeyLike } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('specify-conditions', () => {
  let stats: Stats;
  let wrNodes: Node[];

  function getCondition(node: Node) {
    return node.children.filter((childNode) => childNode.name === 'condition');
  }

  function getConditions(node: Node) {
    return node.children.filter((childNode) => childNode.name === 'conditions');
  }

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrNodes = results.root.children.filter((n) => n.attributes.key.startsWith('entry'));
      done();
    });
  });

  it('should run without error', () => {
    expect(wrNodes).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  describe('add conditions to entry-points', () => {
    it('should add nextes conditions as specified in the conditionMap to app-one', () => {
      const wrWithGoodConfig = findKeyLike(wrNodes, 'app-one');
      const conditions = getConditions(wrWithGoodConfig);

      expect(conditions).toBeTruthy();
      expect(conditions).toHaveLength(1);

      const condition = conditions[0];
      expect('AND').toEqual(condition.attributes.type);
      expect(condition.children).toHaveLength(2);

      expect(condition).toMatchInlineSnapshot(
        {
          name: 'conditions',
          attributes: { type: 'AND' },
          children: [
            {
              name: 'conditions',
              attributes: { type: 'OR' },
              children: [
                {
                  name: 'condition',
                  attributes: { class: 'foo.bar.baz', invert: 'true' },
                  children: [{ name: 'param', attributes: { name: 'permission' }, children: [], content: 'admin' }],
                  content: '',
                },
                {
                  name: 'condition',
                  attributes: { class: 'foo.bar.baz2' },
                  children: [{ name: 'param', attributes: { name: 'permission' }, children: [], content: 'project' }],
                  content: '',
                },
              ],
              content: '',
            },
            {
              name: 'condition',
              attributes: { class: 'foo.bar.baz3' },
              children: [{ name: 'param', attributes: { name: 'permission' }, children: [], content: 'admin' }],
              content: '',
            },
          ],
          content: '',
        },
        `
        Object {
          "attributes": Object {
            "type": "AND",
          },
          "children": Array [
            Object {
              "attributes": Object {
                "type": "OR",
              },
              "children": Array [
                Object {
                  "attributes": Object {
                    "class": "foo.bar.baz",
                    "invert": "true",
                  },
                  "children": Array [
                    Object {
                      "attributes": Object {
                        "name": "permission",
                      },
                      "children": Array [],
                      "content": "admin",
                      "name": "param",
                    },
                  ],
                  "content": "",
                  "name": "condition",
                },
                Object {
                  "attributes": Object {
                    "class": "foo.bar.baz2",
                  },
                  "children": Array [
                    Object {
                      "attributes": Object {
                        "name": "permission",
                      },
                      "children": Array [],
                      "content": "project",
                      "name": "param",
                    },
                  ],
                  "content": "",
                  "name": "condition",
                },
              ],
              "content": "",
              "name": "conditions",
            },
            Object {
              "attributes": Object {
                "class": "foo.bar.baz3",
              },
              "children": Array [
                Object {
                  "attributes": Object {
                    "name": "permission",
                  },
                  "children": Array [],
                  "content": "admin",
                  "name": "param",
                },
              ],
              "content": "",
              "name": "condition",
            },
          ],
          "content": "",
          "name": "conditions",
        }
      `,
      );
    });

    it('should add simple condition as specified in condition map to app-two', () => {
      const wrWithGoodConfig = findKeyLike(wrNodes, 'app-two');
      const conditions = getCondition(wrWithGoodConfig);

      expect(conditions).toBeTruthy();
      expect(conditions).toHaveLength(1);

      const condition = conditions[0];
      expect('foo.bar.baz').toEqual(condition.attributes.class);
      expect('true').toEqual(condition.attributes.invert);

      expect(condition).toMatchInlineSnapshot(
        {
          name: 'condition',
          attributes: { class: 'foo.bar.baz', invert: 'true' },
          children: [{ name: 'param', attributes: { name: 'permission' }, children: [], content: 'admin' }],
          content: '',
        },
        `
        Object {
          "attributes": Object {
            "class": "foo.bar.baz",
            "invert": "true",
          },
          "children": Array [
            Object {
              "attributes": Object {
                "name": "permission",
              },
              "children": Array [],
              "content": "admin",
              "name": "param",
            },
          ],
          "content": "",
          "name": "condition",
        }
      `,
      );
    });

    it('should add multiple conditions as specified in condition map to app-three', () => {
      const wrWithGoodConfig = findKeyLike(wrNodes, 'app-three');
      const conditions = getCondition(wrWithGoodConfig);

      expect(conditions).toBeTruthy();
      expect(conditions).toHaveLength(2);

      const condition1 = conditions[0];
      expect('foo.bar.baz').toEqual(condition1.attributes.class);
      expect('true').toEqual(condition1.attributes.invert);
      const condition2 = conditions[1];
      expect('foo.bar.baz2').toEqual(condition2.attributes.class);
      expect(undefined).toEqual(condition2.attributes.invert);

      expect(JSON.stringify(conditions)).toEqual(
        `[{"name":"condition","attributes":{"class":"foo.bar.baz","invert":"true"},"children":[{"name":"param","attributes":{"name":"permission"},"children":[],"content":"admin"}],"content":""},{"name":"condition","attributes":{"class":"foo.bar.baz2"},"children":[{"name":"param","attributes":{"name":"permission"},"children":[],"content":"project"}],"content":""}]`,
      );
    });
  });
});
