import fs from 'fs';
import path from 'path';
import webpack from 'webpack';

import { asyncChunkLoader } from '../../fixtures/webpack-runtime-chunks';
import configAfter from './webpack.after.config';
import configBefore from './webpack.before.config';

const targetDir = path.join(__dirname, 'target');

describe('ensure-runtime-overwrite-of-mini-css-extract-plugin', () => {
  describe('mini-css-extract-plugin is defined before wrm-plugin', () => {
    beforeEach((done) => {
      webpack(configBefore, () => {
        done();
      });
    });

    it('should inject a WRM pre-condition checker into the webpack runtime', () => {
      // setup
      const bundleFile = fs.readFileSync(path.join(targetDir, 'app-before.js'), 'utf-8');
      const expectedRuntimeAdjustment = asyncChunkLoader('com.atlassian.plugin.test');

      expect(bundleFile).toContain(expectedRuntimeAdjustment);
    });
  });

  describe('mini-css-extract-plugin is defined after wrm-plugin', () => {
    beforeEach((done) => {
      webpack(configAfter, () => {
        done();
      });
    });

    it('should inject a WRM pre-condition checker into the webpack runtime', () => {
      // setup
      const bundleFile = fs.readFileSync(path.join(targetDir, 'app-after.js'), 'utf-8');
      const expectedRuntimeAdjustment = asyncChunkLoader('com.atlassian.plugin.test');

      expect(bundleFile).toContain(expectedRuntimeAdjustment);
    });
  });
});
