import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getByKey } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('async-chunks-named-context', () => {
  let stats: Stats;
  let runtime: Node;
  let runtime2: Node;
  let app: Node;
  let app2: Node;
  let splitAppAndApp2: Node;
  let asyncBar: Node;
  let asyncFoo: Node;
  let asyncAsyncBar: Node;
  let asyncAsyncBarTwo: Node;
  let asyncAsyncAsyncBar: Node;

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      runtime = getByKey(results.root.children, 'entrypoint-app');
      runtime2 = getByKey(results.root.children, 'entrypoint-app2');
      app = getByKey(results.root.children, 'split_app');
      app2 = getByKey(results.root.children, 'split_app2');
      splitAppAndApp2 = getByKey(results.root.children, 'split_app~app2');
      asyncBar = getByKey(results.root.children, 'async-bar');
      asyncFoo = getByKey(results.root.children, 'async-foo');
      asyncAsyncBar = getByKey(results.root.children, 'async-async-bar');
      asyncAsyncBarTwo = getByKey(results.root.children, 'async-async-bar-two');
      asyncAsyncAsyncBar = getByKey(results.root.children, 'async-async-async-bar');
      done();
    });
  });

  it('should compile correctly', () => {
    expect(runtime).toBeTruthy();
    expect(runtime2).toBeTruthy();
    expect(app).toBeTruthy();
    expect(app2).toBeTruthy();
    expect(splitAppAndApp2).toBeTruthy();
    expect(asyncBar).toBeTruthy();
    expect(asyncFoo).toBeTruthy();
    expect(asyncAsyncBar).toBeTruthy();
    expect(asyncAsyncBarTwo).toBeTruthy();
    expect(asyncAsyncAsyncBar).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('has a context named after the webpack chunk name', () => {
    const getContext = (node: Node) => node.children.filter((n) => n.name === 'context')[0];
    expect(getContext(asyncBar)).toHaveProperty('content', 'async-chunk-async-bar');
    expect(getContext(asyncFoo)).toHaveProperty('content', 'async-chunk-async-foo');
    expect(getContext(asyncAsyncBar)).toHaveProperty('content', 'async-chunk-async-async-bar');
    expect(getContext(asyncAsyncBarTwo)).toHaveProperty('content', 'async-chunk-async-async-bar-two');
    expect(getContext(asyncAsyncAsyncBar)).toHaveProperty('content', 'async-chunk-async-async-async-bar');
  });
});
