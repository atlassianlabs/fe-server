import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getResources, getWebResources } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'plugin-descriptor', 'wr-defs.xml');

describe('source maps', () => {
  let wrs;
  let resources: Node[];

  beforeEach((done) => {
    webpack(config, (err, st) => {
      if (err || st!.hasErrors()) {
        throw err;
      }

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      wrs = getWebResources(results.root);
      resources = wrs.flatMap(getResources);
      done();
    });
  });

  // the WRM checks if a `resource[location]`.map file exists and, if so, will serve it.
  it('does not add resource blocks for map files', () => {
    const mapResources = resources.filter((r) => r.attributes.location.endsWith('map'));
    expect(mapResources).toHaveLength(0);
  });
});
