import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const FRONTEND_SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'target');

const config: Configuration = {
  mode: 'production',
  devtool: 'source-map',
  context: FRONTEND_SRC_DIR,
  entry: {
    first: path.join(FRONTEND_SRC_DIR, 'first.js'),
    second: path.join(FRONTEND_SRC_DIR, 'second.js'),
    third: path.join(FRONTEND_SRC_DIR, 'third.js'),
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      minSize: 0,
      chunks: 'all',
    },
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'com.atlassian.plugin.test',
      xmlDescriptors: path.join(OUTPUT_DIR, 'plugin-descriptor', 'wr-defs.xml'),
    }),
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(OUTPUT_DIR),
  },
};

export default config;
