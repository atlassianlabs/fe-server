import fs from 'fs';
import path from 'path';
import webpack from 'webpack';

import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const wrmManifestOutput = path.join(targetDir, 'manifest.json');

describe('wrm-manifest-path', () => {
  it('generates a manifest JSON file', (done) => {
    webpack(config, (err, stats) => {
      if (err) {
        throw err;
      }
      expect(stats!.hasErrors()).toEqual(false);
      expect(stats!.hasWarnings()).toEqual(false);
      expect(fs.existsSync(wrmManifestOutput)).toEqual(true);
      done();
    });
  });

  it('contains all entrypoints', (done) => {
    webpack(config, (err) => {
      if (err) {
        throw err;
      }

      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const wrmManifest = require(wrmManifestOutput).providedDependencies;
      const entries = Object.getOwnPropertyNames(wrmManifest);
      expect(entries).toHaveLength(2);
      expect(wrmManifest.app.dependency).toEqual('com.atlassian.plugin.test:entrypoint-app');
      expect(wrmManifest.app2.dependency).toEqual('com.atlassian.plugin.test:app2-custom-entrypoint-name');

      done();
    });
  });

  it('handles custom library [name]s', (done) => {
    webpack(config, (err) => {
      if (err) {
        throw err;
      }

      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const wrmManifest = require(wrmManifestOutput).providedDependencies;
      expect(wrmManifest.app.import.var).toEqual(`require('app-with-a-custom-library-name')`);
      expect(wrmManifest.app.import.amd).toEqual('app-with-a-custom-library-name');

      expect(wrmManifest.app2.import.var).toEqual(`require('app2-with-a-custom-library-name')`);
      expect(wrmManifest.app2.import.amd).toEqual('app2-with-a-custom-library-name');

      done();
    });
  });
});
