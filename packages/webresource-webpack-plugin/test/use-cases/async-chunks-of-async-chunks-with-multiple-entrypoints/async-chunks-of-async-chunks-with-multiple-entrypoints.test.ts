import fs from 'fs';
import path from 'path';
import type { Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { asyncChunkLoader } from '../../fixtures/webpack-runtime-chunks';
import { getByKey, getContent, getDependencies } from '../../util/xml-parser';
import config from './webpack.config';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('async-chunks-of-async-chunks-with-multiple-entrypoints', () => {
  let stats: Stats;
  let runtime: Node;
  let runtime2: Node;
  let app: Node;
  let app2: Node;
  let splitAppAndApp2: Node;
  let asyncBar: Node;
  let asyncFoo: Node;
  let asyncAsyncBar: Node;
  let asyncAsyncBarTwo: Node;
  let asyncAsyncAsyncBar: Node;

  beforeEach((done) => {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile);
      runtime = getByKey(results.root.children, 'entrypoint-app');
      runtime2 = getByKey(results.root.children, 'entrypoint-app2');
      app = getByKey(results.root.children, 'split_app');
      app2 = getByKey(results.root.children, 'split_app2');
      splitAppAndApp2 = getByKey(results.root.children, 'split_app~app2');
      asyncBar = getByKey(results.root.children, 'async-bar');
      asyncFoo = getByKey(results.root.children, 'async-foo');
      asyncAsyncBar = getByKey(results.root.children, 'async-async-bar');
      asyncAsyncBarTwo = getByKey(results.root.children, 'async-async-bar-two');
      asyncAsyncAsyncBar = getByKey(results.root.children, 'async-async-async-bar');
      done();
    });
  });

  it('should create a webresource for each async chunk', () => {
    expect(runtime).toBeTruthy();
    expect(runtime2).toBeTruthy();
    expect(app).toBeTruthy();
    expect(app2).toBeTruthy();
    expect(splitAppAndApp2).toBeTruthy();
    expect(asyncBar).toBeTruthy();
    expect(asyncFoo).toBeTruthy();
    expect(asyncAsyncBar).toBeTruthy();
    expect(asyncAsyncBarTwo).toBeTruthy();
    expect(asyncAsyncAsyncBar).toBeTruthy();
    expect(stats.hasErrors()).toEqual(false);
    expect(stats.hasWarnings()).toEqual(false);
  });

  it('should inject a WRM pre-condition checker into the webpack runtime', () => {
    // setup
    const bundleFile = fs.readFileSync(path.join(targetDir, 'runtime~app.js'), 'utf-8');
    const expectedRuntimeAdjustment = asyncChunkLoader('com.atlassian.plugin.test');

    expect(bundleFile).toContain(expectedRuntimeAdjustment);
  });

  describe('dependencies per web-resource', () => {
    let runtimeDeps: string[];
    let runtimeDeps2: string[];
    let appDeps: string[];
    let appDeps2: string[];
    let splitAppAndApp2Deps: string[];
    let asyncBarDeps: string[];
    let asyncFooDeps: string[];
    let asyncAsyncBarDeps: string[];
    let asyncAsyncBarTwoDeps: string[];
    let asyncAsyncAsyncBarDeps: string[];

    beforeEach(() => {
      runtimeDeps = getContent(getDependencies(runtime));
      runtimeDeps2 = getContent(getDependencies(runtime2));
      appDeps = getContent(getDependencies(app));
      appDeps2 = getContent(getDependencies(app2));
      splitAppAndApp2Deps = getContent(getDependencies(splitAppAndApp2));
      asyncBarDeps = getContent(getDependencies(asyncBar));
      asyncFooDeps = getContent(getDependencies(asyncFoo));
      asyncAsyncBarDeps = getContent(getDependencies(asyncAsyncBar));
      asyncAsyncBarTwoDeps = getContent(getDependencies(asyncAsyncBarTwo));
      asyncAsyncAsyncBarDeps = getContent(getDependencies(asyncAsyncAsyncBar));
    });

    it('adds the correct split chunk dependencies to the entrypoints', () => {
      expect(runtimeDeps).toContain('com.atlassian.plugin.test:split_app~app2');
      expect(runtimeDeps).toContain('com.atlassian.plugin.test:split_app');

      expect(runtimeDeps2).toContain('com.atlassian.plugin.test:split_app~app2');
      expect(runtimeDeps2).toContain('com.atlassian.plugin.test:split_app2');
    });

    it('adds shared provided dependencies only to the entry point', () => {
      expect(splitAppAndApp2Deps).toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');

      expect(runtimeDeps).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(runtimeDeps2).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(appDeps).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(appDeps2).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(asyncBarDeps).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(asyncFooDeps).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(asyncAsyncBarDeps).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(asyncAsyncBarTwoDeps).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(asyncAsyncAsyncBarDeps).not.toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
    });

    it('adds async-chunk-only deps only to the async-chunk-webresource', () => {
      expect(asyncBarDeps).toContain('jira.webresources:jquery');

      expect(splitAppAndApp2Deps).not.toContain('jira.webresources:jquery');
      expect(runtimeDeps).not.toContain('jira.webresources:jquery');
      expect(runtimeDeps2).not.toContain('jira.webresources:jquery');
      expect(appDeps).not.toContain('jira.webresources:jquery');
      expect(appDeps2).not.toContain('jira.webresources:jquery');
      expect(asyncFooDeps).not.toContain('jira.webresources:jquery');
      expect(asyncAsyncBarDeps).not.toContain('jira.webresources:jquery');
      expect(asyncAsyncBarTwoDeps).not.toContain('jira.webresources:jquery');
      expect(asyncAsyncAsyncBarDeps).not.toContain('jira.webresources:jquery');
    });
  });
});
