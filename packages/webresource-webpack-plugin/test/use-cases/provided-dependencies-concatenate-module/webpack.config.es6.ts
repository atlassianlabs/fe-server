import path from 'path';

import { config } from './webpack.config';

const FRONTEND_SRC_DIR = path.join(__dirname, 'src-es6');

export const configEs6 = {
  ...config,
  entry: {
    index1: path.join(FRONTEND_SRC_DIR, './index1.ts'),
    index2: path.join(FRONTEND_SRC_DIR, './index2.ts'),
  },
};
