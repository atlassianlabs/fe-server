import path from 'path';
import type { Configuration } from 'webpack';

import WrmPlugin from '../../../src/WrmPlugin';

const OUTPUT_DIR = path.join(__dirname, 'target');

/*
 * Reproduction based on https://ecosystem.atlassian.net/browse/SPFE-912
 */
export const config: Configuration = {
  mode: 'production',
  stats: {
    optimizationBailout: true,
    nestedModules: true,
  },
  output: {
    uniqueName: 'example',
    path: OUTPUT_DIR,
    filename: '[name].js',
    chunkFilename: '[name].js',
  },
  resolve: {
    extensions: ['*', '.ts', '.tsx', '.js', '.jsx'],
  },
  context: path.join(__dirname),
  optimization: {
    minimize: false,
    runtimeChunk: 'single',
    splitChunks: false,
    concatenateModules: true,
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'example',
      xmlDescriptors: path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml'),
      addEntrypointNameAsContext: true,
      providedDependencies: {
        '@tngtech/my-library': {
          dependency: 'example' + ':my-library',
          import: {
            var: "require('my-library')",
            amd: 'my-library',
          },
        },
      },
      watch: false,
      watchPrepare: false,
    }),
  ],
};
