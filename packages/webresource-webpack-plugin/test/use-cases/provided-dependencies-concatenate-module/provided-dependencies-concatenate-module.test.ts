import fs from 'fs';
import path from 'path';
import type { Configuration, Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { configAmd } from './webpack.config.amd';
import { configEs6 } from './webpack.config.es6';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

/**
 * Important: Those tests check if provided dependency is not concatenated with normal modules
 * GIVEN it is imported only in one module (only imported in index1, not imported in index2)
 *
 * https://ecosystem.atlassian.net/browse/SPFE-912
 */
describe('provided-dependencies-concatenate-module', () => {
  let stats: Stats;
  let index1Dependencies: Node[];

  describe('ES6', () => {
    beforeEach((done) => runWebpack(configEs6, done));

    it('generates dependency on provided dependency', () => {
      expectProvidedDependenciesAreDefined();
    });
  });

  describe('AMD', () => {
    beforeEach((done) => runWebpack(configAmd, done));

    it('generates dependency on provided dependency', () => {
      expectProvidedDependenciesAreDefined();
    });
  });

  const runWebpack = (config: Configuration, done: () => void) => {
    webpack(config, (err, _stats) => {
      if (err) {
        throw err;
      }
      stats = _stats!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const xmlDocument = parse(xmlFile);

      const index1SplitWebResource = xmlDocument.root.children.find((n) => n.attributes.key === 'split_index1')!;
      index1Dependencies = index1SplitWebResource.children.filter((n) => n.name === 'dependency');
      done();
    });
  };

  const expectProvidedDependenciesAreDefined = () => {
    expect(index1Dependencies).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          content: 'example:my-library',
        }),
      ]),
    );

    expect(stats!.hasErrors()).toEqual(false);
    expect(stats!.hasWarnings()).toEqual(false);
  };
});
