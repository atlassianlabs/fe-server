import path from 'path';

import { config } from './webpack.config';

const FRONTEND_SRC_DIR = path.join(__dirname, 'src-amd');

export const configAmd = {
  ...config,
  entry: {
    index1: path.join(FRONTEND_SRC_DIR, './index1.js'),
    index2: path.join(FRONTEND_SRC_DIR, './index2.js'),
  },
};
