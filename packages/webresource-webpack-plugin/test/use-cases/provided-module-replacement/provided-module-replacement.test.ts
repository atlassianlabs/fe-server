import fs from 'fs';
import path from 'path';
import type { Configuration, Stats } from 'webpack';
import webpack from 'webpack';
import type { Node } from 'xml-parser';
import parse from 'xml-parser';

import { getContent, getDependencies } from '../../util/xml-parser';
import configAsMap from './webpack.config.with-map';
import configAsObject from './webpack.config.with-object';

const targetDir = path.join(__dirname, 'target');
const webresourceOutput = path.join(targetDir, 'META-INF', 'plugin-descriptor', 'wr-webpack-bundles.xml');

describe('provided-modules-replacement', () => {
  let stats: Stats;
  let entry: Node;
  let dependencies: string[];

  function runWebpack(config: Configuration, done: () => void) {
    webpack(config, (err, st) => {
      stats = st!;

      const xmlFile = fs.readFileSync(webresourceOutput, 'utf-8');
      const results = parse(xmlFile)!;
      entry = results.root.children.find((n) => n.attributes.key.startsWith('entry'))!;
      dependencies = getContent(getDependencies(entry))!;
      done();
    });
  }

  function runTheTestsFor(config: Configuration) {
    beforeEach((done) => runWebpack(config, done));

    it('should create a webresource with dependencies for each async chunk', () => {
      expect(entry).toBeTruthy();
      expect(dependencies).toBeTruthy();
      expect(stats.hasErrors()).toEqual(false);
      expect(stats.hasWarnings()).toEqual(false);
    });

    it('add a dependency for the provided module to the webresource', () => {
      expect(dependencies).toContain('jira.webresources:jquery');
      expect(dependencies).toContain('com.atlassian.plugin.jslibs:underscore-1.4.4');
      expect(dependencies).toContain('a.plugin.key:webresource-key');
    });
  }

  describe('using a map', () => {
    runTheTestsFor(configAsMap);
  });

  describe('using a plain object', () => {
    runTheTestsFor(configAsObject);
  });
});
