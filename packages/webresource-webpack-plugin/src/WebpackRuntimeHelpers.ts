import type { Compilation, Compiler } from 'webpack';

import type { FactoryHandler, ResolveDataHandler } from './types/extracted-webpack-types';

export const hookIntoNormalModuleFactory = (stageName: string, compiler: Compiler, cb: FactoryHandler) => {
  compiler.hooks.compile.tap(stageName, (params) => {
    const hooks = params.normalModuleFactory.hooks;
    const passThruFactory: ResolveDataHandler = (data, callback) => callback();
    hooks.factorize.tapAsync(
      {
        name: stageName,
        stage: 99,
      },
      cb(passThruFactory),
    );
  });
};

export const hookIntoCompileDoneToGenerateReports = (
  stageName: string,
  compiler: Compiler,
  cb: (compilation: Compilation, callback: () => void) => void,
) => {
  compiler.hooks.done.tapAsync(stageName, (stats, callback) => {
    cb(stats.compilation, callback);
  });
};
