import flatMap from 'lodash/flatMap';
import uniq from 'lodash/uniq';
import type { Chunk, Compilation, Compiler, Module } from 'webpack';

import type { ChunkGroup, Entrypoint } from './types/extracted-webpack-types';
import type { WrmResource } from './types/types';
import ProvidedExternalDependencyModule from './webpack-modules/ProvidedExternalDependencyModule';
import WrmDependencyModule from './webpack-modules/WrmDependencyModule';
import WrmResourceModule from './webpack-modules/WrmResourceModule';

function isWrmResourceModule(m: Module): m is WrmResourceModule {
  return m instanceof WrmResourceModule;
}

function isProvidedDependencyOrWrmDependency(m: Module): m is WrmDependencyModule | ProvidedExternalDependencyModule {
  return m instanceof ProvidedExternalDependencyModule || m instanceof WrmDependencyModule;
}

const _getExternalResourceModules = (compilation: Compilation, chunk: Chunk): WrmResource[] => {
  return getChunkModules(compilation, chunk)
    .filter(isWrmResourceModule)
    .map((m) => (m as WrmResourceModule).getResource());
};

const _getExternalDependencyModules = (compilation: Compilation, chunk: Chunk): string[] => {
  return getChunkModules(compilation, chunk)
    .filter(isProvidedDependencyOrWrmDependency)
    .sort((a, b) => {
      return (
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        require('webpack/lib/util/comparators.js').compareModulesByPreOrderIndexOrIdentifier(compilation.moduleGraph)(
          a,
          b,
        )
      );
    })
    .map((m) => (m as WrmDependencyModule | ProvidedExternalDependencyModule).getDependency());
};

export const getAllAsyncChunks = (entryPoints: Entrypoint[]) => {
  const seenChunkGroups = new Set();
  const recursivelyGetAllAsyncChunks = (chunkGroups: ChunkGroup[]): Chunk[] => {
    const unseen = chunkGroups.filter((cg) => {
      // circuit breaker
      // dont use a chunk group more than once
      const alreadySeen = seenChunkGroups.has(cg);
      seenChunkGroups.add(cg);
      return !alreadySeen;
    });
    return flatMap(unseen, (cg) => [...cg.chunks, ...recursivelyGetAllAsyncChunks(cg.getChildren())]);
  };

  // get all async chunks "deep"
  const allAsyncChunks = flatMap(entryPoints, (e) => recursivelyGetAllAsyncChunks(e.getChildren()));

  // dedupe
  return uniq(allAsyncChunks);
};

export const getChunkModules = (compilation: Compilation, chunk: Chunk) => {
  return compilation.chunkGraph.getChunkModules(chunk);
};

export const getExternalResourcesForChunk = (compilation: Compilation, chunk: Chunk) => {
  const externalResources = new Set<WrmResource>();

  for (const dep of _getExternalResourceModules(compilation, chunk)) {
    externalResources.add(dep);
  }
  return Array.from(externalResources);
};

export const getDependenciesForChunks = (compilation: Compilation, val: Chunk | Chunk[]) => {
  const chunks = Array.isArray(val) ? val : [val];
  const externalDeps = new Set<string>();
  for (const chunk of chunks) {
    for (const dep of _getExternalDependencyModules(compilation, chunk)) {
      externalDeps.add(dep);
    }
    // TODO: figure out how to pass this "properly" as a module
    // @ts-expect-error See WrmPlugin.ts where this is set.
    if (chunk.needsWrmRequire) {
      externalDeps.add('com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager');
    }
  }
  return Array.from(externalDeps);
};

export const isRunningInProductionMode = (compiler: Compiler) => {
  const { mode } = compiler.options;

  return mode === 'production' || (mode === 'none' && process.env.NODE_ENV === 'production');
};

export const isSingleRuntime = (compiler: Compiler) => {
  const { options } = compiler;
  const runtimeChunkCfg = options.optimization && options.optimization.runtimeChunk;
  if (runtimeChunkCfg) {
    if (runtimeChunkCfg === 'single') {
      return true;
    }

    const { name } = runtimeChunkCfg as { name?: string | ((arg: Record<string, unknown>) => string) };
    if (typeof name === 'string') {
      return true;
    }
    if (typeof name === 'function') {
      const resultA = name({ name: 'foo' });
      const resultB = name({ name: 'bar' });
      return resultA === resultB;
    }
  }
  return false;
};

export const extractLibraryDetailsFromWebpackConfig = (compiler: Compiler) => {
  const { library } = compiler.options.output;
  if (!library) {
    return { target: compiler.options.externalsType, name: undefined };
  }

  return {
    target: library.type,
    name: library.name as string,
  };
};
