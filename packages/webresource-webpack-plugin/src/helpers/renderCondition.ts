import type { Condition, ConditionParam, Conditions } from '../types/types';
import { renderElement } from './xml';

const renderParams = (params: ConditionParam[] | undefined) => {
  if (!params) {
    return '';
  }
  return params.map((param) => renderElement('param', param.attributes, param.value)).join('');
};

function isConditions(arg: Condition): arg is Conditions {
  if (Array.isArray(arg)) {
    return false;
  }

  return 'type' in arg;
}

const renderCondition = (condition: Condition | undefined): string => {
  if (!condition) {
    return '';
  }

  // we have an array of conditions or condition
  if (Array.isArray(condition)) {
    return condition.map(renderCondition).join('');
  }

  // we have a "conditions"-joiner for multiple sub conditions
  if (isConditions(condition)) {
    return renderElement('conditions', { type: condition.type }, renderCondition(condition.conditions));
  }

  // we have a "condition"
  return renderElement(
    'condition',
    ` class="${condition.class}" ${condition.invert ? 'invert="true"' : ''}`,
    renderParams(condition.params),
  );
};

export default renderCondition;
