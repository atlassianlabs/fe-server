import path from 'path';

import type { TransformationMap } from '../types/types';
import { renderElement } from './xml';

const renderTransformer = (transformers: string[]) => {
  if (transformers && transformers.length) {
    return transformers.map((transformer) => renderElement('transformer', { key: transformer })).join('');
  }
  return '';
};

/**
 * Generates the appropriate function to be used when filtering a transform map down to only those required.
 */
const transformFilterFactory = (resources: Resource[]) => {
  if (resources && resources.length) {
    const resourceFiletypes = resources.map((resource) => path.extname(resource.location).substr(1));
    return (ext: string) => resourceFiletypes.includes(ext);
  }
  return () => true;
};

type Resource = {
  location: string;
};

/**
 * Converts a map of filetype-to-transformer entries in to the set of XML transform elements
 * required for a given set of resources. Renders every transform if no resources are provided.
 */
const renderTransformations = (transformations: TransformationMap, resources: Resource[] = []): string[] => {
  return Array.from(transformations.keys())
    .filter(transformFilterFactory(resources))
    .map((extension) =>
      renderElement('transformation', { extension }, renderTransformer(transformations.get(extension)!)),
    );
};

export default renderTransformations;
