import path from 'path';

import type {
  ChunkResourceDescriptor,
  DataProvider,
  DeprecationInfo,
  Options,
  ResourceParam,
  ResourceParamMap,
  TransformationMap,
  WrmResource,
} from '../types/types';
import renderCondition from './renderCondition';
import renderTransforms from './renderTransformations';
import { parseWebResourceAttributes } from './web-resource-parser';
import { renderElement } from './xml';

/**
 * Renders list of data providers {@see DataProvider} as <data key="provider-key" class="data.provider.Class" /> elements
 */
const renderDataProviders = (dataProviders: DataProvider[] | undefined) => {
  if (!Array.isArray(dataProviders) || dataProviders.length === 0) {
    return [];
  }

  return dataProviders.map((dataProvider) =>
    renderElement('data', {
      key: dataProvider.key,
      class: dataProvider.class,
    }),
  );
};

function renderContexts(contexts: string[] | undefined) {
  return contexts ? contexts.map((context) => `<context>${context}</context>`) : [];
}

function renderDependencies(dependencies: string[] | undefined) {
  return dependencies ? dependencies.map((dependency) => `<dependency>${dependency}</dependency>`) : [];
}

function renderDeprecationInfo(
  deprecationInfo: DeprecationInfo | undefined,
  pluginKey: string,
  webResourceDescriptors: ChunkResourceDescriptor[],
) {
  if (deprecationInfo) {
    if (deprecationInfo === true) {
      return ['<deprecated />'];
    }
    const attributes: { since?: string; remove?: string; alternative?: string } = {};
    if (deprecationInfo.sinceVersion) {
      attributes.since = deprecationInfo.sinceVersion;
    }
    if (deprecationInfo.removeInVersion) {
      attributes.remove = deprecationInfo.removeInVersion;
    }
    if (deprecationInfo.alternative) {
      attributes.alternative = calculateAlternativeResourceKey(
        deprecationInfo.alternative,
        pluginKey,
        webResourceDescriptors,
      );
    }

    return [renderElement('deprecated', attributes, deprecationInfo.extraInfo)];
  }
  return [];
}

/**
 * Expand the alternative attribute value to a full resource key if it is a reference to another entrypoint.
 * @param alternativeAttributeValue the value of the "alternative" attribute in the deprecatedEntrypoints map. Can be a simple string
 *                                  referencing another entrypoint or a full resource key.
 * @param pluginKey the plugin key
 * @param webResourceDescriptors list all entry-points specified in the current build
 */
function calculateAlternativeResourceKey(
  alternativeAttributeValue: string,
  pluginKey: string,
  webResourceDescriptors: ChunkResourceDescriptor[],
) {
  // If a colon is present, the users have specified a full resource key, and we don't need to do any mapping.
  if (alternativeAttributeValue.includes(':')) {
    return alternativeAttributeValue;
  }

  // We need to check if the alternative entry-point's web resource key has been mapped to something else.
  const entryPointDescriptor = webResourceDescriptors.find(
    (descriptor) => descriptor.attributes.moduleId === alternativeAttributeValue,
  );
  if (entryPointDescriptor !== undefined) {
    return `${pluginKey}:${entryPointDescriptor.attributes.key}`;
  }

  // If it's not an entry point just assume it's some other web resource in the same plugin defined outside of webpack.
  return `${pluginKey}:${alternativeAttributeValue}`;
}

const generateResourceElement = (resource: WrmResource, parameterMap: ResourceParamMap) => {
  const { name, location } = resource;
  const assetContentType = path.extname(location).substr(1);
  const parameters = parameterMap.get(assetContentType) || [];
  const children: string[] = [];
  const renderParameters = (attributes: ResourceParam) => children.push(renderElement('param', attributes));
  parameters.forEach(renderParameters);

  return renderElement(
    'resource',
    {
      type: 'download',
      name,
      location,
    },
    children,
  );
};

const renderResources = (parameterMap: ResourceParamMap, resources: WrmResource[]) => {
  return resources
    ? resources
        .filter(Boolean)
        // ignore all `.map` files, since the WRM finds them of its own accord.
        .filter((resource) => !resource.location.endsWith('.map'))
        .map((resource) => generateResourceElement(resource, parameterMap))
    : [];
};

export const renderWebResource = (
  webresource: ChunkResourceDescriptor,
  descriptors: ChunkResourceDescriptor[],
  options: Options,
) => {
  const {
    resources = [],
    externalResources = [],
    contexts,
    dependencies,
    deprecationInfo,
    conditions,
    dataProviders,
  } = webresource;
  const attributes = parseWebResourceAttributes(webresource.attributes);
  const allResources: WrmResource[] = [];
  const children = [];

  const prependPathPrefix = (location: string) => options.locationPrefix + location;

  // add resources for direct dependencies (e.g., JS and CSS files)
  allResources.push(...resources.map((res) => ({ name: res, location: prependPathPrefix(res) })));

  // add resources for indirect dependencies (e.g., images extracted from CSS)
  allResources.push(...externalResources.map((wr) => ({ name: wr.name, location: prependPathPrefix(wr.location) })));

  children.push(
    ...renderTransforms(options.transformationMap as TransformationMap, allResources),
    ...renderContexts(contexts),
    ...renderDependencies(dependencies),
    ...renderDeprecationInfo(deprecationInfo, options.pluginKey, descriptors),
    ...renderResources(options.resourceParamMap as ResourceParamMap, allResources),
    ...renderDataProviders(dataProviders),
    renderCondition(conditions),
  );

  return renderElement('web-resource', attributes, children);
};
