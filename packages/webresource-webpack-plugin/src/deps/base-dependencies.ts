/**
 * @fileOverview
 * Collects a set of web-resource dependencies that should be added
 * to all the web-resources generated during compilation.
 */

import uniq from 'lodash/uniq';

const CROSS_PLATFORM_BASE_DEPS: string[] = [];

const processStrings = (arr: string[]) => {
  return uniq([...CROSS_PLATFORM_BASE_DEPS, ...arr].filter(Boolean));
};

let configuredContexts: string[] = [];

export const getBaseDependencies = () => {
  // defensively cloning so consumers can't accidentally add anything
  return [...configuredContexts];
};

export const setBaseDependencies = (dependencies: string[] | string | undefined) => {
  const contexts: string[] = [];
  if (Array.isArray(dependencies)) {
    contexts.push(...dependencies);
  } else if (typeof dependencies === 'string') {
    contexts.push(dependencies);
  }

  configuredContexts = processStrings(contexts);
};

export const addBaseDependency = (dependency: string) => {
  configuredContexts = processStrings([...configuredContexts, dependency]);
};
