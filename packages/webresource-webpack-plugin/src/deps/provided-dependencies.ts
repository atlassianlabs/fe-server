import type { ProvidedDependenciesMap, ProvidedDependency } from '../types/types';

export const buildProvidedDependency = (
  pluginKey: string,
  resourceKey: string,
  importVar: string,
  importAmd: string,
): ProvidedDependency => {
  return {
    dependency: `${pluginKey}:${resourceKey}`,
    import: {
      var: importVar,
      amd: importAmd,
    },
  };
};

const webresourcePluginName = 'com.atlassian.plugins.atlassian-plugins-webresource-plugin';
const webresourceDep = buildProvidedDependency.bind(undefined, webresourcePluginName);

export const builtInProvidedDependencies: ProvidedDependenciesMap = new Map()
  .set(
    'wrm/require',
    buildProvidedDependency(
      'com.atlassian.plugins.atlassian-plugins-webresource-rest',
      'web-resource-manager',
      'WRM.require',
      'wrm/require',
    ),
  )
  .set('wrm/context-path', webresourceDep('context-path', 'WRM.contextPath', 'wrm/context-path'))
  .set('wrm/data', webresourceDep('data', 'WRM.data', 'wrm/data'))
  .set('wrm/format', webresourceDep('format', 'WRM.format', 'wrm/format'))
  .set('wrm/i18n', webresourceDep('i18n', 'WRM.I18n', 'wrm/i18n'));
