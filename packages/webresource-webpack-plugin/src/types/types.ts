// GENERAL
export type ArrayBaseType<T> = T extends Array<infer U> ? U : never;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Callable = (...args: any[]) => any;
export type ObjectOrMap = Record<string, unknown> | Map<string, unknown>;

// PLUGIN SPECIFIC
export type ConditionParam = {
  attributes?: {
    [key: string]: string;
  };
  value?: string;
};

export type ConditionClass = {
  class: string;
  invert?: boolean;
  params?: ConditionParam[];
};

export type Conditions = {
  type: string;
  conditions: Array<Conditions | ConditionClass>;
};

export type Condition = Conditions | ConditionClass | Array<Conditions | Condition>;

export type DeprecationInfo =
  | {
      sinceVersion?: string;
      removeInVersion?: string;
      alternative?: string;
      extraInfo?: string;
    }
  | true;

export type ProvidedDependency = {
  dependency: string;
  import:
    | {
        var: string;
        amd?: string;
      }
    | string;
};

export type DataProvider = {
  key: string;
  class: string;
};

export type ResourceParam = {
  name: string;
  value: string;
};

export type WebresourceObject = {
  key?: string;
  name?: string;
  state?: string | boolean;
};
export type WebresourceType = string | WebresourceObject;

type MapOrObject<T> = Map<string, T> | { [key: string]: T };

export type ContextMap = Map<string, string[]>;
export type TransformationMap = Map<string, string[]>;
export type ProvidedDependenciesMap = Map<string, ProvidedDependency>;
export type ConditionsMap = Map<string, Condition>;
export type DataProvidersMap = Map<string, DataProvider[]>;
export type DeprecationMap = Map<string, DeprecationInfo>;
export type ResourceParamMap = Map<string, ResourceParam[]>;
export type WebresourceMap = Map<string, WebresourceType>;

export type Options = {
  /** @property pluginKey - The fully qualified plugin key. e.g.: com.atlassian.jira.plugins.my-jira-plugin */
  pluginKey: string;
  /** @property xmlDescriptors - Path to the directory where this plugin stores the descriptors about this plugin, used by the WRM to load your frontend code. */
  xmlDescriptors: string;

  /** @property packageName - Name of frontend package to associate all output JS files with. */
  packageName?: string;

  /** @property devAssetsHash - Custom hash used in development environment resources name. */
  devAssetsHash?: string;
  /** @property locationPrefix - Specify the sub-directory for all web-resource location values. */
  locationPrefix?: string;
  /** @property wrmManifestPath - Path to the WRM manifest file where this plugin stores the mapping of modules to generated web-resources. e.g.: `{"my-entry": "com.example.app:entrypoint-my-entry"}`. Useful if you set { output: { library, libraryTarget } } in your webpack config, to use your build result as provided dependencies for other builds. */
  wrmManifestPath?: string;
  /** @property singleRuntimeWebResourceKey - Ensure all web-resources depend on a single runtime shared amongst them */
  singleRuntimeWebResourceKey?: string;

  /** @property conditionMap - Conditions to be applied to the specified entry-point */
  conditionMap?: MapOrObject<Condition>;
  /** @property contextMap - One or more "context"s to which an entrypoint will be added. e.g.: `{"my-entry": ["my-plugin-context", "another-context"]}` */
  contextMap?: MapOrObject<string[]>;
  /** @property dataProvidersMap - A list of data providers that will be added to the entry point e.g.: `{"my-entry": [{ key: "data-provider-key", class: "my.data.provider.JavaClass" }]}` */
  dataProvidersMap?: MapOrObject<DataProvider[]>;
  /** @property deprecatedEntrypoints - Map of entrypoints that are deprecated to more detailed information about the deprecation including the affected versions, alternatives and reason. e.g.: `{"outdated-entrypoint": { sinceVersion: "2.0.0", removeInVersion: "3.0.0", alternative: "rewritten-endpoint", extraInfo: "outdated-endpoint does not support dark mode" }}` */
  deprecatedEntrypoints?: MapOrObject<DeprecationInfo>;
  /** @property providedDependencies - Map of ES6/AMD module identifiers to their related WRM module keys and JavaScript import values (read: webpack external dependency definition). If this module identifier is imported or required somewhere in the compiled code, it will not be bundled, but instead replaced with the specified external dependency placeholder. */
  providedDependencies?: MapOrObject<ProvidedDependency>;
  /** @property resourceParamMap - Parameters to be added to specific file types. */
  resourceParamMap?: MapOrObject<ResourceParam[]>;
  /** @property transformationMap - Transformations to be applied to the specified file-types. */
  transformationMap?: MapOrObject<string[]> | false;
  /** @property webresourceKeyMap - An explicit name for the web-resource generated per entry point. e.g.: `{"my-entry": "legacy-webresource-name"}`. */
  webresourceKeyMap?: MapOrObject<WebresourceType>;

  /** @property addAsyncNameAsContext - Adds the name of the async chunk as a context prefixed by `async-chunk-`. Will only do so if a webpackChunkName is set. */
  addAsyncNameAsContext?: boolean;
  /** @property addEntrypointNameAsContext - Guarantees each entrypoint will be given a context matching its name. Use with caution; this can adversely affect page weight and may conflict with other plugins and feature code. */
  addEntrypointNameAsContext?: boolean;
  /** @property noWRM - Do not add any WRM specifics to the webpack runtime to allow development on a greenfield. */
  noWRM?: boolean;
  /** @property useDocumentWriteInWatchMode - Use document.write when possible in "watch-mode" to ensure scripts are loaded "blocking" as they would be in a production environment */
  useDocumentWriteInWatchMode?: boolean;
  /** @property verbose - Indicate if log output should be verbose. */
  verbose?: boolean;
  /** @property watch - Trigger watch mode - this requires webpack-dev-server and will redirect requests to the entrypoints to the dev-server that must be running under webpack's `options.output.publicPath`. */
  watch?: boolean;
  /** @property watchPrepare - In conjunction with watch mode - indicates that only "redirects" to a webserver should be build in this run. */
  watchPrepare?: boolean;
};

export type ConsolidatedOptions = Options &
  Required<Omit<Options, 'wrmManifestPath' | 'singleRuntimeWebResourceKey' | 'packageName'>> & {
    conditionMap: ConditionsMap;
    contextMap: ContextMap;
    dataProvidersMap: DataProvidersMap;
    deprecatedEntrypoints: DeprecationMap;
    providedDependencies: ProvidedDependenciesMap;
    resourceParamMap: ResourceParamMap;
    transformationMap: TransformationMap;
    webresourceKeyMap: WebresourceMap;
  };

export type WrmResource = {
  name: string;
  location: string;
};

export type ChunkResourceDescriptor = {
  attributes: { key: string; moduleId?: string };
  resources: string[];
  dependencies?: string[];
  deprecationInfo?: DeprecationInfo;
  contexts?: string[];
  conditions?: Condition;
  dataProviders?: DataProvider[];
  externalResources?: WrmResource[];
};

export type AssetNames = Map<string, string>;
