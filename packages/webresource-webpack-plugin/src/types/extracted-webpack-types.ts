import type { Compilation, Compiler } from 'webpack';

import type { ArrayBaseType } from './types';

export type Entrypoint = Compilation['entrypoints'] extends Map<string, infer U> ? U : never;
export type PathData = Parameters<Compilation['getAssetPath']>[1];

type NormalModuleFactory = Parameters<
  Parameters<InstanceType<typeof Compiler>['hooks']['normalModuleFactory']['tap']>[1]
>[0];
type FactorizeHandlerParameters = Parameters<Parameters<NormalModuleFactory['hooks']['factorize']['tapAsync']>[1]>;
type FactorizeHandlerCallback = FactorizeHandlerParameters[1];
type ResolveData = FactorizeHandlerParameters[0];

export type ResolveDataHandler = (data: ResolveData, cb: FactorizeHandlerCallback) => void;
export type FactoryHandler = (fn: ResolveDataHandler) => ResolveDataHandler;

export type ChunkGroup = ArrayBaseType<Compilation['chunkGroups']>;
