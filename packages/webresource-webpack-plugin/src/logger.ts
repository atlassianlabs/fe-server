/* eslint-disable no-console */
let verbose = false;
export const setVerbose = (arg: boolean) => (verbose = arg);

export const log = (...args: unknown[]) => {
  if (verbose) {
    console.log(...args);
  }
};

export const warn = (...args: unknown[]) => {
  if (verbose) {
    console.warn(...args);
  }
};

export const error = (...args: unknown[]) => {
  if (verbose) {
    console.error(...args);
  }
};
