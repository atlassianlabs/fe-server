import type { Compilation, Compiler } from 'webpack';

import type { AppResourceParams } from './AppResources';
import AppResources from './AppResources';

type AppResourcesFactoryParams = Omit<AppResourceParams, 'compiler' | 'compilation'>;

export default class AppResourceFactory {
  constructor(private appResourcesFactoryParams: AppResourcesFactoryParams) {}

  build(compiler: Compiler, compilation: Compilation) {
    return new AppResources({ ...this.appResourcesFactoryParams, compiler, compilation });
  }
}
