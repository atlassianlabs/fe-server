import ExternalModule from 'webpack/lib/ExternalModule';

import type { ProvidedDependency } from '../types/types';

/**
 * ProvidedExternalModule ensures that the `chunkCondition` is true to allow proper asyncChunk context handling
 * ExternalModules can be defined in different ways. Some of them require the external module to be loaded in the entry point.
 * This is generally not bad as it doesnt cost anything. However the WRM Plugin relies on ExternalModules to specify provided dependencies.
 * These provided dependencies are then added to the context of the chunk in which they occur. Async chunks should therefore
 * have their own dependencies to make the entrypoint as small as possible.
 * ProvidedExternalModule ensures that.
 */
export default class ProvidedExternalDependencyModule extends ExternalModule {
  private _wrmDependency: string;
  private _request: ProvidedDependency['import'];
  private _target: 'var' | 'amd';

  constructor(request: ProvidedDependency['import'], dependency: string, target: 'var' | 'amd') {
    super(request, target);
    this._request = request;
    this._wrmDependency = dependency;
    this._target = target;
  }

  libIdent() {
    return `${this._wrmDependency}/${typeof this._request === 'string' ? this._request : this._request[this._target]}`;
  }

  chunkCondition() {
    return true;
  }

  getDependency() {
    return this._wrmDependency;
  }

  getConcatenationBailoutReason() {
    return `Provided external dependencies can't be concatenated`;
  }
}
