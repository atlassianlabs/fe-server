import type { sources } from 'webpack';
import DllModule from 'webpack/lib/DllModule';
import { RawSource } from 'webpack-sources';

/**
 * This module type allows inclusion of special dependencies in the webpack compilation process
 * that ultimately output nothing in the compiled bundles.
 *
 * This is of use when including certain file types with no associated loader in the compilation process,
 * as is the case with things like Atlassian Soy templates.
 */
export default class EmptyExportsModule extends DllModule {
  constructor(dependency: string, type: string) {
    super(null, [], dependency, type);
  }

  chunkCondition() {
    return true;
  }

  source() {
    // eslint-disable-next-line prettier/prettier
    return new RawSource('module.exports = undefined;') as unknown as sources.RawSource;
  }
}
