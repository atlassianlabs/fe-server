/**
 Method signature is:
 ```
 var inProgress[url] = [done];
 var dataWebpackPrefix = "...";
 // loadScript function to load a script via script tag
 __webpack_require__.l = (url, done, key) => {
    ```
 The variables are:
 'done' - callback function
 'key' - should be in the form "chunk-" + chunkId
 */

const runtimeLoadShim = (pluginKey: string, watchMode: boolean, publicPath: string) => {
  const watchModeShim = watchMode
    ? `
function waitForScriptsLoaded(done) {
   // scripts that are showing up in the performance stats are already loaded, not including scripts in the dom that still load
   const loadedScriptsSrc = performance.getEntriesByType('resource')
      .filter(r => r.initiatorType === 'script' && r.name.startsWith('${publicPath}'))
      .map(r => r.name);
   const domScripts = Array.from(document.querySelectorAll('script')).filter(s => s.src.startsWith('${publicPath}'));
   const deltaScripts = domScripts.filter(s => !loadedScriptsSrc.includes(s.src));
   const promises = deltaScripts.map(deltaScript => {
      return new Promise(res => {
         deltaScript.addEventListener('load', res);
         deltaScript.addEventListener('error', res);
         deltaScript.addEventListener('abort', res);
      });
   });
   Promise.all(promises).then(done);
}
`
    : '';

  return `
// the raw chunkId is used as a web-resource key.
// webpack5's jsonp loader will call the load function with a prefixed chunkId.
// see https://github.com/webpack/webpack/blob/master/lib/web/JsonpChunkLoadingRuntimeModule.js#L181
const chunkId = (key || '').replace(/^chunk-/,'');
// use WRM.require instead of webpack's url-based lookup.
// since WRM.require can handle in-progress loads itself, we don't need to worry about
// registering the 'done' callback in the inProgress array; we just call it after WRM does its thing.
// ------
// WATCHMODE - In watch mode a batch file is loaded that contains a script that loads the actual contents from a webpack dev server.
// This means we cant execute the "done" callback for webpack before the second script is loaded.
// The solution is to check for all scripts to the dev-server in "performance" as this will only contain already fully loaded scripts and
// calculate the diff to all scripts in the body. Any script that is not showing up in "performance" will be listened to for a load event.
// Only once all those load events are successfully returned do we call the "done" callback to webpack and prevent a range of errors that
// would be triggered otherwise.
${watchModeShim}
WRM.require('wrc!${pluginKey}:' + chunkId).then(() => ${
    watchMode ? 'waitForScriptsLoaded(done)' : 'done()'
  }, (data) => {
    var e = new Event('missing');
    e.data = data;
    done(e);
});
// we don't want the rest of the normal load function to run.
return;
`;
};

export default runtimeLoadShim;
