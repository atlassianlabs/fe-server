const path = require('path');
const supportedExtensions = ['.ts', '.js', '.d.ts'];

module.exports = {
  extends: [
    'eslint:recommended',
    'prettier',
    'plugin:import/warnings',
    'plugin:import/errors',
    'plugin:import/typescript',
    'plugin:node/recommended',
  ],
  env: {
    node: true,
  },
  parserOptions: {
    ecmaVersion: 10, // make sure to match with the supported minimum node version
    sourceType: 'module',
    tsconfigRootDir: path.resolve(__dirname),
    project: './tsconfig.eslint.json',
  },
  settings: {
    node: {
      tryExtensions: supportedExtensions,
    },
  },
  plugins: ['@typescript-eslint', 'babel', 'filenames', 'import', 'node', 'prettier'],
  overrides: [
    // TypeScript
    {
      files: ['*.ts', '*.d.ts'],

      parser: '@typescript-eslint/parser',
      extends: ['plugin:@typescript-eslint/eslint-recommended', 'plugin:@typescript-eslint/recommended'],
      rules: {
        '@typescript-eslint/no-useless-constructor': 'error',
        '@typescript-eslint/no-explicit-any': 'error',
        '@typescript-eslint/consistent-type-imports': [
          'error',
          {
            prefer: 'type-imports',
            disallowTypeAnnotations: true,
          },
        ],
        // note you must disable the base rule as it can report incorrect errors
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': 'error',

        // TODO: Fix this another time
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
      },
    },
    {
      files: ['test/**/*.ts'],
      env: {
        jest: true,
      },
      rules: {
        // some packages used in test dont properly specify their exports
        'node/no-unpublished-import': 'off',
        'node/no-extraneous-import': 'off',

        'import/default': 'off',

        // its just tests :shrug:
        'sonarjs/no-duplicate-string': 'off',
        'sonarjs/no-identical-functions': 'off',

        // Jest
        'jest/no-done-callback': 'off',
      },
    },
  ],
  rules: {
    // Node ESLint
    'node/no-unsupported-features/es-syntax': [
      'error',
      {
        ignores: [
          // Modules are supported by TypeScript
          'modules',

          // Dynamic imports are supported by TypeScript
          'dynamicImport',
        ],
      },
    ],

    // There are to many issues with devDependencies
    'node/no-unpublished-require': 'off',

    'import/order': ['error', { groups: [['builtin', 'external', 'internal']] }],
    'import/no-unresolved': 'error',

    // Sonar
    'sonarjs/no-nested-template-literals': 'warn', // We don't mind nested template literals
  },
};
