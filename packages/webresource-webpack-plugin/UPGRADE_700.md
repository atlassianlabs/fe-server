# For everyone

## Removed API

Default transformation map no longer provides `less-transformer`. Ideally, when using Webpack, `.less` files are compiled to CSS using https://www.npmjs.com/package/less-loader.

To preserve existing behaviour, one can extend default providers with `less-transformer` explicitly.

```js
new WrmPlugin({
  transformationMap: WrmPlugin.extendTransformations({
    less: ['less-transformer'],
  }),
});
```
