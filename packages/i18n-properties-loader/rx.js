/**
 * Creates new regex.
 * Source: https://gist.github.com/shannonmoeller/b4f6fbab2ffec56213e7
 *
 * @param flags
 * @return {RegExp}
 */
module.exports = function rx(flags) {
  const trailingComments = /\s+#.*$/gm;
  const surroundingWhitespace = /^\s+|\s+$/gm;
  const literalNewlines = /[\r\n]/g;

  return (strings, ...values) => {
    function toPattern(pattern, rawString, i) {
      let value = values[i];

      if (value == null) {
        return pattern + rawString;
      }

      if (value instanceof RegExp) {
        value = value.source;
      }

      return pattern + rawString + value;
    }

    const compiledPattern = strings.raw
      .reduce(toPattern, '')
      .replace(trailingComments, '')
      .replace(surroundingWhitespace, '')
      .replace(literalNewlines, '');

    return new RegExp(compiledPattern, flags);
  };
};
