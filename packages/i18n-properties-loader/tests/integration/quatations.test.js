const { bundleEntryPoint } = require('./helpers');

// https://docs.oracle.com/javase/8/docs/api/java/text/MessageFormat.html

describe('quotations', () => {
  it('should remove the quotes from quoted values', async () => {
    const { modulePath } = await bundleEntryPoint('./src/quotation-quoted-values.js');
    const { getString } = require(modulePath);

    expect(getString()).toEqual('foo bar biz baz');
  });

  it('should output the pattern {0} instead of formatting the value', async () => {
    const { modulePath } = await bundleEntryPoint('./src/quotation-format-pattern.js');
    const { getString, getStringWithParam } = require(modulePath);

    expect(getString()).toEqual('Quote the format pattern {0}');

    expect(getStringWithParam('error')).not.toContain('error');
    expect(getStringWithParam('error')).toEqual('Quote the format pattern {0}');
  });

  it('should output curly brackets', async () => {
    const { modulePath } = await bundleEntryPoint('./src/quotation-curly-brackets.js');
    const { getString } = require(modulePath);

    expect(getString()).toEqual('Two curly brackets {}');
  });

  it('should remove the only single quote from the message', async () => {
    const { modulePath } = await bundleEntryPoint('./src/quotation-single-quote.js');
    const { getStringOne, getStringTwo } = require(modulePath);

    expect(getStringOne()).toEqual('thats not a mistake'); // It's not! There shouldn't be any single quote char in this message
    expect(getStringTwo()).toEqual('thats not a mistake either'); // It's not! There shouldn't be any single quote char in this message
  });
});
