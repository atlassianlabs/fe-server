import { I18n } from 'wrm/i18n';

export const getString = () => I18n.getText('unescape.single.quotes');

export const getStringWithParams = (input) => I18n.getText('unescape.single.quotes.with.param', input);
