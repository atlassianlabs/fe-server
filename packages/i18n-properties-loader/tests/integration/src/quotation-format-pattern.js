import { I18n } from 'wrm/i18n';

export const getString = () => I18n.getText('quotation.format.pattern');

export const getStringWithParam = (param) => I18n.getText('quotation.format.pattern', param);
