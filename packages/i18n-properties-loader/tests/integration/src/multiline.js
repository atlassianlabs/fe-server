import { I18n } from '@atlassian/wrm-react-i18n';

export const getMultilineString = () => I18n.getText('multiline.string');

export const getSingleLineString = () => I18n.getText('single.line.string');
