const path = require('path');
const SRC_DIR = path.join(__dirname);
const OUTPUT_DIR = path.join(__dirname, '../dist');

const i18nPropertiesLoaderPath = path.resolve(__dirname, '../../../index.js');
const i18nFiles = [path.resolve(SRC_DIR, 'i18n.properties')];

module.exports = {
  devtool: false,
  mode: 'development',
  entry: {},

  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: i18nPropertiesLoaderPath,
            options: {
              i18nFiles,
            },
          },
        ],
      },
    ],
  },

  resolve: {
    alias: {
      'wrm/i18n': '../mocks/wrm-i18n-mock.js',
      '@atlassian/wrm-react-i18n': '../mocks/atlassian-wrm-react-i18n-mock.js',
    },
  },

  output: {
    filename: '[name].js',
    path: OUTPUT_DIR,
    libraryTarget: 'commonjs2',
  },
};
