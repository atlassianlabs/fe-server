import { default as format } from './format';

const WrmI18n = {
  format,
  getText: (i18nKey, ...params) => {
    console.warn(
      `No translation found for '${i18nKey}'. Have you included the "jsI18n" transformation in your web-resource configuration?`,
    );
    return WrmI18n.format.apply(null, [i18nKey, ...params]);
  },
  km: new Map(),
};

global.WRM = {
  I18n: WrmI18n,
};

export default WrmI18n;
