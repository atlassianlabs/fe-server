/**
 * @jest-environment jsdom
 *
 * This test suite assumes WRM >= 4.2.0.
 * See `integration-wrm.test.js` for a test suite for older WRM versions.
 */
const React = require('react');
const { render, screen } = require('@testing-library/react');
const { compileProject } = require('./helpers/project');

// Mock the runtime module
jest.mock('wrm/i18n', () => require('../i18n'), {
  virtual: true,
});

jest.mock('wrm/format', () => require('wrm/i18n').format, {
  virtual: true,
});

describe('wrm-react-i18n integration', () => {
  let consoleWarnSpy;
  let wrmFormatOriginalSpy;
  let wrmGetTextOriginalSpy;

  beforeAll(async () => {
    console.debug('Compiling project...');
    await compileProject();
    console.debug('Project compiled');

    consoleWarnSpy = jest.spyOn(console, 'warn');
    wrmFormatOriginalSpy = jest.spyOn(require('wrm/i18n'), 'format');
    wrmGetTextOriginalSpy = jest.spyOn(require('wrm/i18n'), 'getText');
  });

  afterEach(() => {
    consoleWarnSpy.mockReset();
  });

  describe('api', () => {
    it('should expose the "I18n.getText" function', () => {
      const module = require('../wrm-react-i18n');

      expect(module.I18n).toBeDefined();
      expect(module.I18n.getText).toEqual(expect.any(Function));
    });

    it('should expose the "format" function', () => {
      const module = require('../wrm-react-i18n');

      expect(module.format).toEqual(expect.any(Function));
    });
  });

  describe('behaviour', () => {
    // a note: a warning comes from the original WRM function, here from its mock
    it('should show a warning when the "I18n.getText" function wasn\'t replaced in the runtime', () => {
      // given

      const module = require('../wrm-react-i18n');

      // when
      module.I18n.getText('foo');

      // then
      expect(console.warn).toHaveBeenCalledWith(
        'No translation found for \'foo\'. Have you included the "jsI18n" transformation in your web-resource configuration?',
      );
    });

    it('should replace the translation with React components', () => {
      // given
      const greetings = <span>Hi</span>;
      const name = <strong>Maciej</strong>;
      const phrase = '{0} there {1}!';

      // when
      const module = require('../wrm-react-i18n');
      const result = module.I18n.getText(phrase, greetings, name);
      render(result);

      // then
      // Find the exact HTML output
      const element = screen.getByText(
        (content, element) => element.innerHTML === '<span>Hi</span> there <strong>Maciej</strong>!',
      );

      expect(element).toBeDefined();
      expect(element.textContent).toEqual('Hi there Maciej!');
    });

    it('should not change the original WRM format function', () => {
      // given
      const module = require('../wrm-react-i18n');

      // when
      module.I18n.getText('foo');

      // then
      expect(require('wrm/i18n').format).toStrictEqual(wrmFormatOriginalSpy);
    });

    it('should invoke the original WRM format function', () => {
      // given
      const module = require('../wrm-react-i18n');

      // when
      module.I18n.getText('foo');

      // then
      expect(wrmFormatOriginalSpy).toBeCalledTimes(1);

      // when
      module.I18n.getText('foo2');

      // then
      expect(wrmFormatOriginalSpy).toBeCalledTimes(2);
    });

    it('should not change the original WRM getText function', () => {
      // given
      const module = require('../wrm-react-i18n');

      // when
      module.I18n.getText('foo');

      // then
      expect(require('wrm/i18n').getText).toStrictEqual(wrmGetTextOriginalSpy);
    });

    it('should invoke the original WRM getText function', () => {
      // given
      const module = require('../wrm-react-i18n');

      // when
      module.I18n.getText('foo');

      // then
      expect(wrmGetTextOriginalSpy).toBeCalledTimes(1);

      // when
      module.I18n.getText('foo2');

      // then
      expect(wrmGetTextOriginalSpy).toBeCalledTimes(2);
    });
  });
});
