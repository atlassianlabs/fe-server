#!/usr/bin/env node
// This is a workaround for issue with NPM 7 https://github.com/npm/cli/issues/2632
// eslint-disable-next-line node/no-unpublished-bin
// eslint-disable-next-line node/no-missing-require
require('../dist/cli.js');
