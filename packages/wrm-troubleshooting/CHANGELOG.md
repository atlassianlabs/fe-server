# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.6.4](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.6.4..@atlassian/wrm-troubleshooting@0.6.3) (2024-11-19)


### Reverts

* Revert "chore: changelog linting and postrelease step" ([9eb1420](https://bitbucket.org/atlassianlabs/fe-server/commits/9eb14202fdeb66555fbbe75acdd6eec0faac0afe))



### [0.6.3](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.6.3..@atlassian/wrm-troubleshooting@0.6.2) (2024-06-25)


### Bug Fixes

* **soy-loader:** [SPFE-1067](https://ecosystem.atlassian.net/browse/SPFE-1067) - ensure soy-loader accepts webpack 4 as a peer dependency and add system-level tests for working with all supported webpack versions ([3d8fe4e](https://bitbucket.org/atlassianlabs/fe-server/commits/3d8fe4e97c7af231c32dacf860b4fa54e08053fa))



### [0.6.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.6.2..@atlassian/wrm-troubleshooting@0.6.1) (2024-05-28)

### Bug Fixes

### [0.6.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.6.1..@atlassian/wrm-troubleshooting@0.6.0) (2024-05-13)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

## 0.6.0 (2024-04-05)

### Features

- **webresource-webpack-plugin:** introduce association generation for resource files. Automatically pick up the associated package name from Webpack context path ([a129ce4](https://bitbucket.org/atlassianlabs/fe-server/commits/a129ce413b5d7876017028ec09a7cd70dd9386da))

### Bug Fixes

- [SPFE-894](https://ecosystem.atlassian.net/browse/SPFE-894) refactor code and fix some ESLint Sonar rules ([4b995c2](https://bitbucket.org/atlassianlabs/fe-server/commits/4b995c2f531c12b7d7c9010fc19b0a1efa842358))

### [0.5.11](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.11..@atlassian/wrm-troubleshooting@0.5.10) (2023-02-07)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### [0.5.10](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.10..@atlassian/wrm-troubleshooting@0.5.9) (2022-08-09)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### [0.5.9](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.9..@atlassian/wrm-troubleshooting@0.5.8) (2022-06-06)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### 0.5.8 (2022-02-10)

### Bug Fixes

- [SPFE-894](https://ecosystem.atlassian.net/browse/SPFE-894) refactor code and fix some ESLint Sonar rules ([4b995c2](https://bitbucket.org/atlassianlabs/fe-server/commits/4b995c2f531c12b7d7c9010fc19b0a1efa842358))

### [0.5.7](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.7..@atlassian/wrm-troubleshooting@0.5.6) (2021-11-17)

### Bug Fixes

- [SPFE-759](https://ecosystem.atlassian.net/browse/SPFE-759) Check if one of the "mvn" or "atlas-mvn" commands is available ([2b09a6a](https://bitbucket.org/atlassianlabs/fe-server/commits/2b09a6a99480178c1e224a9ae68b313aae6d59b3))

### 0.5.6 (2021-11-17)

### Bug Fixes

- [SPFE-759](https://ecosystem.atlassian.net/browse/SPFE-759) Fix compatibility issues with Windows OS ([2a09d0d](https://bitbucket.org/atlassianlabs/fe-server/commits/2a09d0d1c8bc353c7717cbcf9190828773a8350b))
- [SPFE-760](https://ecosystem.atlassian.net/browse/SPFE-760) Fix checking webpack-cli version in CLI >= 4.9 ([5fac5a0](https://bitbucket.org/atlassianlabs/fe-server/commits/5fac5a0e9e1b45a30a17c0dac52952a7e1df7b1c))
- **ci:** [SPFE-760](https://ecosystem.atlassian.net/browse/SPFE-760) Run integration tests with webpack 4 and 5 ([65a8b17](https://bitbucket.org/atlassianlabs/fe-server/commits/65a8b179561218d0fcdc37d0cc286e1af08f0e0e))
- **deps:** update dependency @amplitude/node to v1.9.1 ([57dae59](https://bitbucket.org/atlassianlabs/fe-server/commits/57dae59bc2f5f119cc790f017205b4530ae593af))
- Update webpack depndency ([00fc73e](https://bitbucket.org/atlassianlabs/fe-server/commits/00fc73e95ad4a2f89ee4fd3561ab3fb60d1b9516))

### [0.5.5](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.5..@atlassian/wrm-troubleshooting@0.5.4) (2021-10-14)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### [0.5.4](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.4..@atlassian/wrm-troubleshooting@0.5.3) (2021-10-08)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### [0.5.3](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.3..@atlassian/wrm-troubleshooting@0.5.2) (2021-09-30)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### [0.5.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.2..@atlassian/wrm-troubleshooting@0.5.1) (2021-09-27)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### [0.5.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.1..@atlassian/wrm-troubleshooting@0.5.0) (2021-09-24)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

## [0.5.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.5.0..@atlassian/wrm-troubleshooting@0.4.3) (2021-09-22)

- Update internal dependencies

### Features

- **ts:** Update codebase to TS 4.4 ([36da0d8](https://bitbucket.org/atlassianlabs/fe-server/commits/36da0d885c1e3c49a27f6a86bae1c3b3844a5d90))
- **wrm-troubleshooting:** [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) add version checker when package is installed globally ([df21c42](https://bitbucket.org/atlassianlabs/fe-server/commits/df21c42b17f006996b9855f1ce442f718ef91c90))

### [0.4.3](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.4.3..@atlassian/wrm-troubleshooting@0.4.2) (2021-09-02)

### Bug Fixes

- **wrm-troubleshooting:** [SPFE-738](https://ecosystem.atlassian.net/browse/SPFE-738) Update readme and use proper link to an image ([c848737](https://bitbucket.org/atlassianlabs/fe-server/commits/c84873747810e43b160f0cee2e015d17f3233868))

### [0.4.2](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.4.2..@atlassian/wrm-troubleshooting@0.4.1) (2021-09-02)

**Note:** Version bump only for package @atlassian/wrm-troubleshooting

### [0.4.1](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.4.1..@atlassian/wrm-troubleshooting@0.4.0) (2021-09-02)

### Bug Fixes

- **wrm-troubleshooting:** add the "prepublishOnly" script to build the package before publishing ([329c66b](https://bitbucket.org/atlassianlabs/fe-server/commits/329c66b55714575c11f5e03f6da668309163a98c))

## [0.4.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/wrm-troubleshooting@0.4.0..v0.3.3) (2021-09-02)

### Features

- **wrm-troubleshooting:** [SPFE-738](https://ecosystem.atlassian.net/browse/SPFE-738) Update readme for troubleshooting tool ([361b262](https://bitbucket.org/atlassianlabs/fe-server/commits/361b262ac77932a0e2c8e18779ee0c4a72dcf7b1))
- **wrm-troubleshooting:** [SPFE-738](https://ecosystem.atlassian.net/browse/SPFE-738) Migrate the repository into monorepo ([3b4d4a3](https://bitbucket.org/atlassianlabs/fe-server/commits/3b4d4a3ac3a38aeabe02c83fe5c1fdf039fb23fe))

### [0.3.3](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/branches/compare/v0.3.3..v0.3.2) (2021-08-16)

### Bug Fixes

- fix settings package version ([cd9c010](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/cd9c010f594b47b7387fca59ca53f429397051be))
- remove extra console.log ([89b61c0](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/89b61c011cb251a8c20da8c4786de3e7f5d1c3e2))

### [0.3.2](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/branches/compare/v0.3.2..v0.3.1) (2021-08-16)

### Bug Fixes

- Improve reading the package version ([a53d0ae](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/a53d0ae785b4b03aea297b890fb93b3d70b4024f))

### [0.3.1](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/branches/compare/v0.3.1..v0.3.0) (2021-08-16)

### Bug Fixes

- **deps:** update dependencies ([39d1e5f](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/39d1e5fbed55958f6030205b208cb3d47c60cf88))

## [0.3.0](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/branches/compare/v0.3.0..v0.2.0) (2021-08-16)

### Features

- [SPFE-629](https://ecosystem.atlassian.net/browse/SPFE-629) Collect analytics ([7ffdaac](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/7ffdaac26bfd7b16c15c79c2cd80000ee690ba27))

### Bug Fixes

- fix the location in `dist` directory ([8f7df7e](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/8f7df7ea54d95813d8b267105bfc274b03483c38))

## [0.2.0](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/branches/compare/v0.2.0..v0.1.1) (2021-08-13)

### Features

- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) Add intro text ([52c6918](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/52c6918be9716ee593afaed8a9da818b39bcc462))
- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) add missing links to guides ([11e227c](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/11e227cf6077b6d1f93d2cba49a60f497c51d36d))
- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) Improve checking config files ([1fcea37](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/1fcea371fc90997a51415aa7f36b4170bf0e259a))
- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) Prettify and improve the output ([d5818e4](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/d5818e4dc8c60bd97a196b1eacaf7f66c1fa13f4))
- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) Prettify the output ([f6a73da](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/f6a73da748cb6f8ea5a61e1a6268b6bf75b5a59b))

### Bug Fixes

- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) Fix closing the process after run ([d14e2f7](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/d14e2f7d38ef5d5796ab3ca5b1439cd28bff43b0))
- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) Fix resolving relative paths ([536b50e](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/536b50e45253bc23911f5ffa408b18ff74e6891f))
- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) fix typo in question ([eba46b1](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/eba46b13a2982d6e89b392c2692957dcef04e099))
- [SPFE-516](https://ecosystem.atlassian.net/browse/SPFE-516) update message and fix runing tests ([b0c6684](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/b0c66845882126b41b48105f37cd2171603ed660))

### 0.1.1 (2021-07-29)

### Features

- **release:** [SPFE-614](https://ecosystem.atlassian.net/browse/SPFE-614) Run build before publishing ([51136f0](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/51136f08207835bed7f0b163042539b7a217731d))

## 0.1.0 (2021-07-29)

### Features

- [SPFE-611](https://ecosystem.atlassian.net/browse/SPFE-611) Use mvn command directly without calling Atlassian SDK ([3b33d24](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/3b33d240e16d64bf9c90ef941b069964608edfb7))
- [SPFE-615](https://ecosystem.atlassian.net/browse/SPFE-615) add timeout to external commands ([fb0c1f5](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/fb0c1f5db5edae71671f17b13b41a722c534979c))
- **tests:** [SPFE-531](https://ecosystem.atlassian.net/browse/SPFE-531) Add happy-path integration test ([148c5c5](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/148c5c5dd627ff8fcc3f4775ceeae8d2cc41f9af))
- **tests:** [SPFE-531](https://ecosystem.atlassian.net/browse/SPFE-531) Add Jest ([05e1224](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/05e12247e816f1833af63387bec7896ebd529835))
- **tests:** [SPFE-531](https://ecosystem.atlassian.net/browse/SPFE-531) Add smoke tests for displaying version ([6686101](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/6686101170f6ade7ca161769eb6d5b3ba84e1ff6))

### Bug Fixes

- **ts:** [SPFE-616](https://ecosystem.atlassian.net/browse/SPFE-616) fix types for empty step result ([9b49dfa](https://bitbucket.org/atlassian/wrm-troubleshooting-tool/commits/9b49dfab0a9bf71f5309b3be37602c089c462e3b))
