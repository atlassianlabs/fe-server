# WRM Troubleshooting tool

![node version](https://img.shields.io/node/v/@atlassian/wrm-troubleshooting.svg)
![npm downloads](https://img.shields.io/npm/dt/@atlassian/wrm-troubleshooting.svg)

A tool that can help you with troubleshooting the configuration of `webpack` and Atlassian P2 project.

## Usage

The troubleshooting tool was designed to check and verify the configuration of the [Atlassian P2 plugin][p2-plugin-docs], a modern front-end setup configured with [webpack][webpack] and [`atlassian-webresource-webpack-plugin`][webpack-wrm-plugin] plugin.

Additionally, the troubleshooting tool can verify if the webpack bundled JS code can be accessed with Atlassian Web-Resource Manager (WRM) inside the Atlassian product runtime.

To check if your project was configured correctly open a terminal and navigate to the root directory of your project:

```shell
cd /my-project/location
```

Next, run the `npx` command:

```shell
npx @atlassian/wrm-troubleshooting

# or run a standalone command if the package is installed globally
wrm-troubleshooting
```

The tool will ask you a few questions and will guide you in case your project configuration is invalid or broken.

You can also read the next paragraph to learn how to install the package globally if you prefer that.

### Example and demo

![WRM Troubleshooting Demo](https://bitbucket.org/atlassianlabs/fe-server/raw/master/packages/wrm-troubleshooting/docs/wrm-troubleshooting-demo.gif)

Watch the demo with [asciinema](https://asciinema.org/a/433644)

## Custom Babel environments

Some projects are using Babel with `@babel/preset-env` preset and can define a custom environment overrides for webpack:

### Example of `.babelrc` or `babel.config.js` files

```json5
{
  env: {
    development: {
      presets: [
        // A list of Babel preset
      ],
      plugins: [
        // A list of Babel plugins
      ],
    },

    production: {
      plugins: [
        // A list of Babel plugins
      ],
    },
  },
}
```

In this case you need to guide the WRM troubleshooting tool about what environment should be selected to bundle the webpack code.
To do that, you can execute the `npx @atlassian/wrm-troubleshooting` command with the `BABEL_ENV` variables like so:

```shell
BABEL_ENV=production npx @atlassian/wrm-troubleshooting
```

## Installation

You don't need to install this package. You can use it with the `npx` command.

In case you still would like to install the package, you can install it globally with NPM:

```shell
npm install --global @atlassian/wrm-troubleshooting
```

or by Yarn

```bash
yarn global add @atlassian/wrm-troubleshooting
```

## Minimum requirements

This plugin is compatible with:

- Node 14+
- `webpack` v4 and `webpack` v5
- `atlassian-webresource-webpack-plugin` v4 and `atlassian-webresource-webpack-plugin` v5

[webpack-wrm-plugin]: https://www.npmjs.com/package/atlassian-webresource-webpack-plugin 'webpack WRM plugin'
[webpack]: https://webpack.js.org 'webpack'
[p2-plugin-docs]: https://developer.atlassian.com/server/framework/atlassian-sdk/plugin-framework 'Plugin Framewor'
