import { AbortController } from 'abort-controller';
import type { RequestInfo, RequestInit, Response } from 'node-fetch';
import fetch from 'node-fetch';

export class FetchTimeoutError extends Error {}

export async function fetchWithTimeout(timeout: number, url: RequestInfo, options?: RequestInit): Promise<Response> {
  const abortCtrl = new AbortController();

  setTimeout(() => {
    abortCtrl.abort();
  }, timeout);

  return fetch(url, {
    ...options,
    signal: abortCtrl.signal,
  });
}
