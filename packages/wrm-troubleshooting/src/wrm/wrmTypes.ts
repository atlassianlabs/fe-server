export interface IWrmResource {
  url: string;
  resourceType: 'CSS' | 'JAVASCRIPT';
  ieOnly?: boolean;
  key: string;
  batchType: 'context' | 'resource';
}

export interface IResourcesResponse {
  resources: IWrmResource[];
  unparsedData: {
    [key: string]: string;
  };
  unparsedErrors: {
    [key: string]: string;
  };
}
