//  https://developer.atlassian.com/server/framework/atlassian-sdk/working-with-the-sdk/#supported-atlassian-applications-and-default-ports
export const applicationUrls: readonly { product: string; defaultUrl: string }[] = [
  {
    product: 'Bamboo',
    defaultUrl: 'http://localhost:6990/bamboo',
  },
  {
    product: 'Bitbucket',
    defaultUrl: 'http://localhost:7990/bitbucket',
  },
  {
    product: 'Confluence',
    defaultUrl: 'http://localhost:1990/confluence',
  },
  {
    product: 'Crowd',
    defaultUrl: 'http://localhost:4990/crowd',
  },
  {
    product: 'Crucible',
    defaultUrl: 'http://localhost:3990/fecru',
  },
  {
    product: 'FishEye',
    defaultUrl: 'http://localhost:3990/fecru',
  },
  {
    product: 'Jira',
    defaultUrl: 'http://localhost:2990/jira',
  },
  {
    product: 'RefApp',
    defaultUrl: 'http://localhost:5990/refapp',
  },
] as const;
