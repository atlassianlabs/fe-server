import chalk from 'chalk';
import type { Arguments } from 'yargs';

import { trackEvent } from './analytics/analytics';
import {
  trackVerificationStepEvent,
  TROUBLESHOOTING_COMMAND_PASSED,
  TROUBLESHOOTING_COMMAND_RUN,
} from './analytics/events';
import { cliLink } from './cliUtils';
import { MavenError } from './maven/MavenError';
import { getRelativePath } from './paths';
import { SCAN_FOLDERS_KEY } from './pom';
import {
  didFail,
  didPass,
  getEffectivePomConfig,
  getEffectiveWebpackConfig,
  getWrmWebpackPlugin,
  locatePomFile,
  locateWebpackConfigFile,
  validateOptions,
  verifyPomConfigIsValid,
  verifyWebpackBundle,
  verifyWrmPluginHasValidConfig,
  verifyWrmPluginScanFolders,
} from './steps';
import { getResultPayload } from './steps/types';
import type { VerifyWrmRuntimeResultPayload } from './steps/verifyWrmRuntime';
import { verifyWrmRuntime } from './steps/verifyWrmRuntime';
import type { TroubleshootingOptions, TroubleshootingOptionsWithDefaultValues } from './troubleshootingOptions';
import { defaultTroubleshootingOptions } from './troubleshootingOptions';
import type { PropertyOf } from './types';
import { WebpackError } from './webpack/webpackError';
import type { AtlassianWebpackWrmPluginOptions } from './webpackWrmPlugin';
import { NPM_PACKAGE_NAME, WRM_PLUGIN_NAME } from './webpackWrmPlugin';

export { TroubleshootingOptions };

// eslint-disable-next-line sonarjs/cognitive-complexity
export async function troubleshootingCommand(optionsWithoutDefaults: Arguments<TroubleshootingOptions>): Promise<void> {
  const options: TroubleshootingOptionsWithDefaultValues = Object.assign(
    {},
    defaultTroubleshootingOptions,
    optionsWithoutDefaults,
  );

  await trackEvent(TROUBLESHOOTING_COMMAND_RUN);

  // Check if the passed options are valid
  const validationResult = await validateOptions(options);
  await trackVerificationStepEvent(validateOptions.name, validationResult);

  if (didFail(validationResult)) {
    const { error } = validationResult;

    console.log();
    console.log(`❌ One of the provided options is invalid:`);
    console.log();
    console.log(error.message);
    process.exit(1);
  }

  // Intro
  const color = '#037cbd';
  console.log(
    `👋 Welcome to the ${chalk.hex(color)(
      'WRM webpack troubleshooting',
    )} tool. This tool can help you with self-troubleshooting the configuration of the ${chalk.bold(
      NPM_PACKAGE_NAME,
    )} webpack plugin.`,
  );
  console.log(
    `We will ask you a few questions about the project setup, and we will run a few automated checks to verify the project configuration.`,
  );
  console.log();

  // 1. Find webpack.config.* files
  // TODO: When we have options.webpack we should verify if the file exist
  const webpackConfigFileResult = await locateWebpackConfigFile(options);
  await trackVerificationStepEvent(locateWebpackConfigFile.name, webpackConfigFileResult);

  if (didFail(webpackConfigFileResult)) {
    console.log();
    console.log(
      `❌ We couldn't find a webpack config file. Please check if you are using a correct project directory and/or you have created a webpack config file.`,
    );
    console.log();
    console.log(
      `📖 Check the ${chalk.bold('Getting Started guide')} for webpack: https://webpack.js.org/guides/getting-started/`,
    );
    process.exit(1);
  }

  const { webpackConfigFile } = getResultPayload(webpackConfigFileResult);
  console.log(`✅ We have found a webpack configuration file at "${chalk.green(getRelativePath(webpackConfigFile))}"`);

  // 2. Get pom.xml file
  // TODO: When we have options.pom we should verify if the file exist
  const pomFileResult = await locatePomFile(options);
  await trackVerificationStepEvent(locatePomFile.name, pomFileResult);

  if (didFail(pomFileResult)) {
    console.log();
    console.log(
      `❌ We couldn't find a valid pom.xml file. Please check if you are using a correct project directory and/or you have created a pom.xml file.`,
    );
    console.log();
    console.log(
      `📖 Check the ${chalk.bold('Tutorial and Guide')} for ${chalk.bold(
        'Atlassian SDK',
      )} to learn how to create and build a Java plugin:`,
    );
    console.log('https://developer.atlassian.com/server/framework/atlassian-sdk/create-a-helloworld-plugin-project');

    process.exit(1);
  }

  const { pomFile } = getResultPayload(pomFileResult);
  console.log(`✅ We have found a pom file at "${chalk.green(getRelativePath(pomFile))}"`);

  // 3. Get effective webpack config
  const webpackConfigResult = await getEffectiveWebpackConfig(options, {
    webpackConfigFile,
  });
  await trackVerificationStepEvent(getEffectiveWebpackConfig.name, webpackConfigResult);

  if (didFail(webpackConfigResult)) {
    const { error } = webpackConfigResult;
    console.log();
    console.log("❌ We can't retrieve the webpack configuration.");
    console.log();
    console.log(error.message);

    if (error instanceof WebpackError && options.verbose) {
      console.log('\nWebpack output:\n\n');
      console.log(error.getWebpackOutput());
    }

    process.exit(1);
  }

  const { webpackCliInfo, webpackEffectiveConfig } = getResultPayload(webpackConfigResult);
  console.log(`✅ We have successfully retrieved the webpack configuration.`);

  // 4. Get effective pom config
  const effectivePomConfig = await getEffectivePomConfig(options, { pomFile });
  await trackVerificationStepEvent(getEffectivePomConfig.name, effectivePomConfig);

  if (didFail(effectivePomConfig)) {
    const { error } = effectivePomConfig;
    console.log();
    console.log("❌ We can't retrieve the pom configuration. Bummer");
    console.log();
    console.log(error.message);

    if (error instanceof MavenError && options.verbose) {
      console.log('\nMaven output:\n\n');
      console.log(error.getMavenOutput());
    }

    process.exit(1);
  }

  const { pomConfig } = getResultPayload(effectivePomConfig);
  console.log(
    `✅ We have successfully retrieved configuration from the "${chalk.green(getRelativePath(pomFile))}" file.`,
  );

  // 5. Check if webpack is using WRM plugin
  const wrmWebpackPluginResult = await getWrmWebpackPlugin(options, { webpackEffectiveConfig });
  await trackVerificationStepEvent(getWrmWebpackPlugin.name, wrmWebpackPluginResult);

  if (didFail(wrmWebpackPluginResult)) {
    console.log();
    console.log(
      `❌ We couldn't find a usage of "${chalk.bold(NPM_PACKAGE_NAME)}" inside the "${chalk.green(
        getRelativePath(webpackConfigFile),
      )}" webpack configuration file.`,
    );
    console.log(`Please ensure that the "${chalk.bold(NPM_PACKAGE_NAME)}" plugin is added to the "plugins" list.`);
    console.log();
    console.log(
      `📖 Check the ${chalk.bold('Guide')} to learn how to install and configure the ${chalk.bold(
        NPM_PACKAGE_NAME,
      )} plugin:`,
    );
    console.log('https://www.npmjs.com/package/atlassian-webresource-webpack-plugin#how-to-use-the-plugin');
    process.exit(1);
  }

  const { wrmWebpackPlugin } = getResultPayload(wrmWebpackPluginResult);
  console.log(`✅ We have found that you are using "${chalk.bold(NPM_PACKAGE_NAME)}" in your webpack configuration.`);

  // 6. Check if the "pluginKey" from webpack WRM plugin is matching plugin id from the pom.xml file
  const wrmPluginHasValidConfig = await verifyWrmPluginHasValidConfig(options, {
    pomFile,
    pomConfig,
    webpackEffectiveConfig,
    wrmWebpackPlugin,
  });
  await trackVerificationStepEvent(verifyWrmPluginHasValidConfig.name, wrmPluginHasValidConfig);

  if (didFail(wrmPluginHasValidConfig)) {
    const { error } = wrmPluginHasValidConfig;

    console.log();
    console.log(
      `❌ We have found invalid configuration in "${chalk.green(
        getRelativePath(webpackConfigFile),
      )}" or in "${chalk.green(getRelativePath(pomFile))}" configuration files:`,
    );
    console.log();
    console.log(error.message);
    process.exit(1);
  }

  const { pomXml } = getResultPayload(wrmPluginHasValidConfig);
  console.log(
    `✅ We have found that "${chalk.bold('pluginKey')}" option from ${chalk.bold(
      WRM_PLUGIN_NAME,
    )} webpack plugin is matching plugin key from the pom file.`,
  );

  // 7. Check if the pom.xml file contains "Atlassian-Scan-Folders" entry
  const pomHasValidConfig = await verifyPomConfigIsValid(options, { pomFile, pomXml });
  await trackVerificationStepEvent(verifyPomConfigIsValid.name, pomHasValidConfig);

  if (didFail(pomHasValidConfig)) {
    const { error } = pomHasValidConfig;

    console.log();
    console.log(`❌ We have found invalid configuration in "${chalk.green(getRelativePath(pomFile))}" file`);
    console.log();
    console.log(error.message);
    process.exit(1);
  }

  console.log(
    `✅ We have found that "${chalk.bold(SCAN_FOLDERS_KEY)}" configuration is configured in the "${chalk.green(
      getRelativePath(pomFile),
    )}" file.`,
  );

  // 8. Check if the "xmlDescriptors" from webpack WRM plugin is matching the "Atlassian-Scan-Folders" directory from the pom.xml file
  const wrmScanFolderResult = await verifyWrmPluginScanFolders(options, {
    pomXml,
    pomFile,
    webpackEffectiveConfig,
    wrmWebpackPlugin,
  });
  await trackVerificationStepEvent(verifyWrmPluginScanFolders.name, wrmScanFolderResult);

  if (didFail(wrmScanFolderResult)) {
    const { error } = wrmScanFolderResult;

    console.log();
    console.log(
      `❌ We have found invalid configuration in "${chalk.green(
        getRelativePath(webpackConfigFile),
      )}" or in "${chalk.green(getRelativePath(pomFile))}" configuration files:`,
    );
    console.log();
    console.log(error.message);
    process.exit(1);
  }

  const wrmXmlDescriptorOption: PropertyOf<AtlassianWebpackWrmPluginOptions, 'xmlDescriptors'> = 'xmlDescriptors';

  console.log(
    `✅ We have found that "${chalk.bold(wrmXmlDescriptorOption)}" option from ${chalk.bold(
      WRM_PLUGIN_NAME,
    )} webpack plugin is matching the "${chalk.bold(SCAN_FOLDERS_KEY)}" configuration from the "${chalk.green(
      getRelativePath(pomFile),
    )}" file.`,
  );

  // 9. Run webpack compilation
  const webpackBundlingResult = await verifyWebpackBundle(options, {
    pomXml,
    pomFile,
    webpackConfigFile,
    webpackCliInfo,
  });
  await trackVerificationStepEvent(verifyWebpackBundle.name, webpackBundlingResult);

  if (didFail(webpackBundlingResult)) {
    const { error } = webpackBundlingResult;

    console.log();
    console.log(`❌ We have run webpack bundle but we have found an issue.`);
    console.log();
    console.log(error.message);

    if (error instanceof WebpackError && options.verbose) {
      console.log('\nWebpack output:\n\n');
      console.log(error.getWebpackOutput());
    }

    process.exit(1);
  }

  console.log(`✅ We have run webpack bundle and we haven't found any issues.`);

  // 10. Ask to check bundles in a running application
  const wrmRuntimeVerificationResult = await verifyWrmRuntime(options, { pomFile, pomXml });
  await trackVerificationStepEvent(verifyWrmRuntime.name, wrmRuntimeVerificationResult);

  if (didFail(wrmRuntimeVerificationResult)) {
    const { error } = wrmRuntimeVerificationResult;

    console.log();
    console.log(`❌ We have found some issues while verifying WRM runtime:`);
    console.log();
    console.log(error.message);
    process.exit(1);
  }

  if (didPass(wrmRuntimeVerificationResult)) {
    const resultPayload = getResultPayload(wrmRuntimeVerificationResult);

    // We did run the WRM check
    // The payload of step result will be undefined when we haven't run WRM checks
    if (resultPayload !== undefined) {
      const { webResourceKey, webResources } = resultPayload as VerifyWrmRuntimeResultPayload;

      console.log(
        `✅ We have managed to load the "${chalk.bold(
          webResourceKey,
        )}" web-resource with WRM runtime and the response resulted in ${chalk.bold.green(
          // eslint-disable-next-line sonarjs/no-nested-template-literals
          `${webResources.length} resource(s)`,
        )}`,
      );

      console.log('   You can click on the resource link to verify if the content includes the code from your bundle:');

      for (const resource of webResources) {
        console.log(`     - ${cliLink(resource.url)}`);
      }
    }
  }

  // We are done!
  await trackEvent(TROUBLESHOOTING_COMMAND_PASSED);

  console.log();
  console.log(
    `✅ We have finished all the checks and we haven't found any issues with your webpack or webpack WRM plugin configuration.`,
  );
  console.log();
}
