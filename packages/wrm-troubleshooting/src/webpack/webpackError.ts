export class WebpackError extends Error {
  private readonly webpackOutput: string;

  constructor(message: string, webpackOutput: string) {
    super(message);

    this.webpackOutput = webpackOutput;
  }

  getWebpackOutput(): string {
    return this.webpackOutput;
  }
}
