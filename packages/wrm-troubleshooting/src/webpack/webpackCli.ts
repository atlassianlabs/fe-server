import chalk from 'chalk';
import type { ExecaChildProcess, ExecaError } from 'execa';
import execa from 'execa';
import { promises as fs } from 'fs';
import path from 'path';
import type { SemVer } from 'semver';
import * as semver from 'semver';

import { isSubDirectory } from '../paths';
import { getTimeoutError } from '../steps/types';
import { runWebpackBundleCliV3, runWebpackBundleWithInjectedPluginCliV3 } from './webpackCliV3';
import { runWebpackBundleCliV4, runWebpackBundleWithInjectedPluginCliV4 } from './webpackCliV4';
import { WebpackError } from './webpackError';

enum WebpackCliVersion {
  V3 = 3,
  V4 = 4,
}

export interface WebpackCliInfo {
  /**
   * A webpack-cli major version identifier
   */
  version: WebpackCliVersion;

  /**
   * A working directory location where we can safely call the "npx webpack-cli" command.
   */
  workingDirectory: string;
}

export async function getWebpackCliInfo(
  webpackConfigFile: string,
  commandTimeout: number,
): Promise<WebpackCliInfo | Error | WebpackError> {
  const absoluteDirname = path.resolve(path.dirname(webpackConfigFile));
  const workingDirectory = await resolveCliWorkingDirectory(absoluteDirname);

  if (!workingDirectory) {
    return new Error(
      `We can't detect the location of the ${chalk.bold(
        'webpack CLI',
        // eslint-disable-next-line sonarjs/no-duplicate-string
      )}. Please ensure that the project has installed the "${chalk.bold('webpack-cli')}" NPM dependency.`,
    );
  }

  const cliVersion = await getWebpackCliVersion(workingDirectory, commandTimeout);

  if (cliVersion instanceof Error) {
    return cliVersion;
  }

  return {
    version: cliVersion,
    workingDirectory,
  };
}

const versionsMap: Map<number, WebpackCliVersion> = new Map([
  [3, WebpackCliVersion.V3],
  [4, WebpackCliVersion.V4],
]);

/**
 * Returns the version of the webpack-cli command
 */
async function getWebpackCliVersion(
  workingDirectory: string,
  commandTimeout: number,
): Promise<WebpackCliVersion | Error | WebpackError> {
  let cliVersion: SemVer | null;

  try {
    const { stdout: output } = await execa('npx', ['webpack-cli', '--version'], {
      cwd: workingDirectory,
      timeout: commandTimeout,
    });

    // webpack-cli v3 outputs only a version but webpack-cli v4 outputs multiple versions for each of the components
    cliVersion = semver.parse(output);

    if (cliVersion === null && output.split('\n').length > 1) {
      cliVersion = semver.parse(findCliVersionString(output));
    }
  } catch (err) {
    const error = err as ExecaError;

    if (error.timedOut) {
      return getTimeoutError('webpack', commandTimeout);
    }

    const errorMessage = error.stdout || error.message || error.shortMessage;

    return new WebpackError(
      `We can't detect the location of the ${chalk.bold(
        'webpack CLI',
      )}. Please ensure that the project has installed the "${chalk.bold('webpack-cli')}" NPM dependency.`,
      errorMessage,
    );
  }

  if (cliVersion === null) {
    return new Error(
      `The "webpack-cli" has invalid version "${cliVersion}". Please try reinstalling the "webpack-cli" NPM dependency.`,
    );
  }

  const webpackCliMajorVersion = cliVersion.major;

  if (!versionsMap.has(webpackCliMajorVersion)) {
    const supportedVersions = [...versionsMap.keys()].join(', ');

    return new Error(
      `The current version ${webpackCliMajorVersion} of  "webpack-cli" package is not yet supported. Please try using the "webpack-cli" in one the supported versions instead: ${supportedVersions}.`,
    );
  }

  // We checked if the map has the value
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return versionsMap.get(webpackCliMajorVersion)!;
}

function findCliVersionString(input: string): string | null {
  const match = input.match(/^(?:webpack-cli):?\s+([0-9.a-z\-_]+)$/m);

  return match && match[1];
}
/**
 * Locates the directory where we can safely call the "npx webpack-cli" command withing the project dir
 */
async function resolveCliWorkingDirectory(dir: string): Promise<string | null> {
  // Get a name of the root directory so we can break from the loop while we are traversing path tree
  const rootDirectory = path.parse(dir).root;

  let currentDir = dir;
  let foundPath: string | null = null;

  // Chek if we have a valid dir and we haven't reached the root directory yet
  while (currentDir && currentDir !== rootDirectory) {
    let webpackCliPath: string;

    /**
     * Check if we can resolve webpack-cli binary in the current directory.
     *
     * We need a custom resolution function since we can't simply call {@link require.resolve} with "webpack-cli" as param:
     * ```js
     * require.resolve('webpack-cli', { paths: [currentDir] });
     * ```
     * This wouldn't work with the project that is using Yarn and Workspaces that are hoisting the "webpack-cli" NPM module
     * to the root directory of the monorepo project. If the webpack-cli bin file is located under node_modules in
     * one of the non-project-root directories, most of the time it's the directory we are looking for.
     */
    try {
      webpackCliPath = path.join(currentDir, 'node_modules', '.bin', 'webpack-cli');

      await fs.access(webpackCliPath);
    } catch (e) {
      currentDir = path.resolve(currentDir, '..');

      continue;
    }

    // Check if the resolved webpack path is included in current dir
    if (!isSubDirectory(currentDir, path.dirname(webpackCliPath))) {
      currentDir = path.resolve(currentDir, '..');
      continue;
    }

    foundPath = currentDir;
    break;
  }

  return foundPath || null;
}

export function runWebpackBundleWithInjectedPlugin(
  webpackCliInfo: WebpackCliInfo,
  webpackConfigFile: string,
  commandTimeout: number,
): Promise<Error | ExecaChildProcess> {
  let command: Promise<Error | ExecaChildProcess>;

  switch (webpackCliInfo.version) {
    case WebpackCliVersion.V3:
      command = runWebpackBundleWithInjectedPluginCliV3(webpackCliInfo, webpackConfigFile, commandTimeout);
      break;

    case WebpackCliVersion.V4:
      command = runWebpackBundleWithInjectedPluginCliV4(webpackCliInfo, webpackConfigFile, commandTimeout);
      break;
  }

  return command;
}

export function runWebpackBundle(
  webpackCliInfo: WebpackCliInfo,
  webpackConfigFile: string,
  commandTimeout: number,
): ExecaChildProcess {
  let command: ExecaChildProcess;

  switch (webpackCliInfo.version) {
    case WebpackCliVersion.V3:
      command = runWebpackBundleCliV3(webpackCliInfo, webpackConfigFile, commandTimeout);
      break;

    case WebpackCliVersion.V4:
      command = runWebpackBundleCliV4(webpackCliInfo, webpackConfigFile, commandTimeout);
      break;
  }

  return command;
}
