import chalk from 'chalk';
import inquirer from 'inquirer';
import inquirerAutocompletePrompt from 'inquirer-autocomplete-prompt';
import validUrl from 'valid-url';

import { applicationUrls } from '../applicationUrls';
import { getFullWebResourceKey, getPluginWebResourceDefinitions } from '../atlassianPluginXml';
import { getAtlassianPluginKeyFromPom } from '../pom';
import type { InquirerAutocompleteItem, PomXml } from '../types';
import { WRM_PLUGIN_NAME } from '../webpackWrmPlugin';
import { FetchTimeoutError } from '../wrm/fetchWithTimeout';
import type { SimplifiedWebResource } from '../wrm/wrmRestApi';
import { fetchWebResources } from '../wrm/wrmRestApi';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getFailedResult, getPassedResult, getRestTimeoutResult } from './types';

inquirer.registerPrompt('autocomplete', inquirerAutocompletePrompt);

interface VerifyWrmRuntimeInputPayload extends StepInputPayload {
  pomFile: string;
  pomXml: PomXml;
}

export interface VerifyWrmRuntimeResultPayload extends StepResultPayload {
  webResourceKey: string;
  webResources: SimplifiedWebResource[];
}

type VerifyWrmRuntimeEmptyResultPayload = StepResultPayload;

export const verifyWrmRuntime: VerificationStepWithInputPayload<
  VerifyWrmRuntimeInputPayload,
  VerifyWrmRuntimeEmptyResultPayload | VerifyWrmRuntimeResultPayload
  // eslint-disable-next-line sonarjs/cognitive-complexity
> = async (options, payload) => {
  const { pomFile, pomXml } = payload;

  // Skip running in-app WRM check
  if (options.noWrm) {
    return getPassedResult();
  }

  if (!options.yes) {
    console.log();
    console.log(
      `So far, we haven't found any issues with the ${chalk.bold(
        WRM_PLUGIN_NAME,
      )} webpack plugin or in project configuration.`,
    );
    console.log(
      `Additionally, we can check if the generated bundles from webpack can be loaded by the ${chalk.bold(
        'Web-Resource Manager',
      )} (WRM) in the application runtime.`,
    );
    console.log(
      `For that, you will haven to run your project first. You can run the project using "${chalk.bold(
        'atlas-run',
      )}" command from ${chalk.bold('Atlassian SDK')}`,
    );
    console.log(
      'You can read more about this command here: https://developer.atlassian.com/server/framework/atlassian-sdk/atlas-run/',
    );
    console.log();
  }

  interface ConfirmAnswer {
    confirmCheckingWrmRuntime: boolean;
  }

  // Confirm running WRM runtime check
  if (!options.yes) {
    const { confirmCheckingWrmRuntime }: ConfirmAnswer = await inquirer.prompt({
      type: 'confirm',
      name: 'confirmCheckingWrmRuntime',
      message: `Should we verify the WRM runtime now?`,
    });

    if (!confirmCheckingWrmRuntime) {
      // TODO: Add some message?
      return getPassedResult();
    }
  }

  console.log('👀 We will run WRM runtime check now. This might take some time...');

  let applicationUrl: string | symbol | undefined = options.applicationUrl;

  if (!applicationUrl) {
    console.log();
    console.log(
      'To run WRM runime check, you need to provide a full URL of the running Atlassian Server or DC application like e.g. Jira.',
    );
    console.log('If you are using Atlassian SDK you can check the application default ports and paths here:');
    console.log(
      'https://developer.atlassian.com/server/framework/atlassian-sdk/working-with-the-sdk/#supported-atlassian-applications-and-default-ports',
    );
    console.log();

    interface ApplicationUrlAnswer {
      applicationUrl: string | symbol;
    }

    const OTHER_URL = Symbol('otherUrl');

    ({ applicationUrl } = (await inquirer.prompt({
      type: 'list',
      name: 'applicationUrl',
      message: `Select the full URL to application you are running or select "other" to type a custom URL:`,
      choices: [
        {
          name: 'other',
          value: OTHER_URL,
        },
        new inquirer.Separator(),
        ...applicationUrls.map(({ product, defaultUrl }) => ({
          name: `${defaultUrl} - ${product}`,
          value: defaultUrl,
        })),
        new inquirer.Separator(),
      ],
    })) as ApplicationUrlAnswer);

    // Let the user type the custom URL
    if (applicationUrl === OTHER_URL) {
      const defaultUrl = 'http://localhost:2990/jira';

      ({ applicationUrl } = (await inquirer.prompt({
        type: 'input',
        name: 'applicationUrl',
        message: `What is the full URL to your running application? E.g ${defaultUrl}`,
        validate(input: string) {
          const isValid = Boolean(input && validUrl.isWebUri(input));

          return isValid ? isValid : 'Provided URL is not valid. Try correcting your answer.';
        },
        transformer(input: string) {
          // Remove "/" from the end
          return input && input.trim().replace(/\/+$/, '');
        },
        default: defaultUrl,
      })) as ApplicationUrlAnswer);
    }
  }

  // Get list of generated web-resources
  const webResourceDefinitions = await getPluginWebResourceDefinitions(pomFile, pomXml);

  if (webResourceDefinitions instanceof Error) {
    return getFailedResult(new Error("We couldn't parse the XML of some of web-resources generated by webpack."));
  }

  // TODO: This should be checked after we run webpack bundle
  if (!webResourceDefinitions.length) {
    return getFailedResult(
      new Error("We couldn't find any definitions of the web-resources in the XML files generated by webpack."),
    );
  }

  interface WebResourceKeyAnswer {
    webResourceKey: string;
  }

  // Call WRM API to get some resources
  let webResourceKey = options.resourceKey;

  // TODO: Check if provided web-resource belongs to the resource from generated resources

  if (!webResourceKey) {
    ({ webResourceKey } = (await inquirer.prompt({
      // @ts-expect-error We are using inquirer plugin that is causing TS error
      type: 'autocomplete',
      name: 'webResourceKey',
      message: 'Select a web-resource key you want to verify:',
      source: (answersSoFar: string[], input?: string): InquirerAutocompleteItem[] => {
        const userInput = input || '';

        return webResourceDefinitions
          .filter((webResource) => webResource.key.match(new RegExp(userInput, 'i')))
          .map<InquirerAutocompleteItem>((webResource) => ({
            name: webResource.key,
            value: webResource.key,
          }));
      },
    })) as WebResourceKeyAnswer);
  }

  // Get full web-resource key
  const pluginKey = getAtlassianPluginKeyFromPom(pomXml);
  const fullWebResourceKey = getFullWebResourceKey(pluginKey, webResourceKey);

  // TODO: Check if the application is running

  const webResources = await fetchWebResources(applicationUrl as string, fullWebResourceKey, options.timeout);

  if (webResources instanceof FetchTimeoutError) {
    return getRestTimeoutResult(fetchWebResources.name, options.timeout);
  }

  if (webResources instanceof Error) {
    return getFailedResult(webResources);
  }

  if (webResources.length < 1) {
    return getFailedResult(
      new Error(
        `We didn't manage to retrieve any resources for the "${webResourceKey}" web-resource with WRM runtime.`,
      ),
    );
  }

  return getPassedResult<VerifyWrmRuntimeResultPayload>({
    webResourceKey,
    webResources,
  });
};
