import type { PluginInstanceWithName, WebpackEffectiveConfig } from '../types';
import type { AtlassianWebpackWrmPluginInstanceWithOptions } from '../webpackWrmPlugin';
import { NPM_PACKAGE_NAME, WRM_PLUGIN_NAME } from '../webpackWrmPlugin';
import type { StepInputPayload, StepResultPayload, VerificationStepWithInputPayload } from './types';
import { getFailedResult, getPassedResult } from './types';

interface WrmWebpackPluginInputPayload extends StepInputPayload {
  webpackEffectiveConfig: WebpackEffectiveConfig;
}

interface WrmWebpackPluginResultPayload extends StepResultPayload {
  wrmWebpackPlugin: AtlassianWebpackWrmPluginInstanceWithOptions;
}

const missingPluginError = new Error(
  `Provided webpack configuration is not using "${NPM_PACKAGE_NAME}" webpack plugin.`,
);

export const getWrmWebpackPlugin: VerificationStepWithInputPayload<
  WrmWebpackPluginInputPayload,
  WrmWebpackPluginResultPayload
> = async (options, payload) => {
  const { webpackEffectiveConfig } = payload;

  const { plugins } = webpackEffectiveConfig;

  if (!Array.isArray(plugins)) {
    return getFailedResult(missingPluginError);
  }

  const wrmWebpackPlugin = plugins.find((plugin: PluginInstanceWithName) => plugin.pluginName === WRM_PLUGIN_NAME);

  if (!wrmWebpackPlugin) {
    return getFailedResult(missingPluginError);
  }

  return getPassedResult<WrmWebpackPluginResultPayload>({
    wrmWebpackPlugin: wrmWebpackPlugin as AtlassianWebpackWrmPluginInstanceWithOptions,
  });
};
