export type TroubleshootingOptions = {
  webpack?: string;
  pom?: string;
  yes?: boolean;
  noWrm?: boolean;
  applicationUrl?: string;
  resourceKey?: string;
  verbose?: boolean;

  /**
   * A timeout for external command (in milliseconds)
   */
  timeout?: number;
};

type DefaultValuesTypes = Pick<TroubleshootingOptions, 'yes' | 'noWrm' | 'verbose' | 'timeout'>;

// Default timeout (in seconds)
export const DEFAULT_TIMEOUT = 3 * 60; // 3 minutes

export const defaultTroubleshootingOptions: Required<DefaultValuesTypes> = {
  yes: false,
  noWrm: false,
  verbose: false,
  timeout: DEFAULT_TIMEOUT,
};

export type TroubleshootingOptionsWithDefaultValues = TroubleshootingOptions & Required<DefaultValuesTypes>;
