#!/usr/bin/env node
/* eslint-disable node/shebang */
import yargs from 'yargs';

import { checkForUpdate } from './checkForUpdate';
import type { TroubleshootingOptions } from './troubleshooting';
import { troubleshootingCommand } from './troubleshooting';
import { DEFAULT_TIMEOUT } from './troubleshootingOptions';
import { getAsyncHandler } from './yargsHelper';

// Check for a new version
checkForUpdate();

yargs
  .command<TroubleshootingOptions>({
    command: '$0 [options]',
    // @ts-expect-error Yargs throws type errors here
    desc: 'Run troubleshooting for webpack WRM plugin.',
    builder: (yargs) =>
      yargs
        .option('webpack', {
          describe: 'The path to webpack configuration file\ne.g. ./webpack.config.js',
          type: 'string',
          alias: 'w',
        })

        .option('pom', {
          describe: 'The path to pom.xml file\ne.g. ./pom.xml',
          type: 'string',
          alias: 'p',
        })

        .option('applicationUrl', {
          describe: 'Full application URL we will use to verify WRM runtime\ne.g. http://localhost:2990/jira',
          type: 'string',
          alias: 'u',
        })

        .option('resourceKey', {
          describe: 'A web-resource key we will use to verify WRM runtime\ne.g. name-of-my-web-resource-key',
          type: 'string',
          alias: 'r',
        })

        .option('noWrm', {
          describe: 'Skip running in-app WRM check',
          type: 'boolean',
          alias: 'nw',
          default: false,
        })

        .option('yes', {
          describe: 'Skip confirmation and run troubleshooting immediately',
          type: 'boolean',
          alias: 'y',
          default: false,
        })

        .option('verbose', {
          describe: 'Display verbose output',
          type: 'boolean',
          alias: 'v',
          default: false,
        })

        .option('timeout', {
          describe: 'A timeout in seconds for executing external commands.',
          type: 'number',
          alias: 't',
          default: DEFAULT_TIMEOUT,
          // Convert the seconds to milliseconds
          coerce(seconds) {
            if (!seconds || Number.isNaN(seconds) || seconds < 1) {
              seconds = DEFAULT_TIMEOUT;
            }

            return seconds * 1000;
          },
        }),
    handler: getAsyncHandler<TroubleshootingOptions>(troubleshootingCommand),
  })
  .strict()
  .help().argv; // for --help and -h to work
