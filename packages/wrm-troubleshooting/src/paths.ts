import path = require('path');

export const getRelativePath = (location: string): string => path.relative(process.cwd(), location);

export const isSubDirectory = (parent: string, child: string): boolean => {
  const relative = path.relative(parent, child);

  return Boolean(relative && !relative.startsWith('..') && !path.isAbsolute(relative));
};
