import { promises as fs } from 'fs';

import { globWithOptions } from './globWithOptions';
import { getAbsoluteScanFoldersConfig } from './pom';
import type { ParsedXmlWithAttrs, PomXml } from './types';
import { parseXmlFromFile } from './xmlParser';

export type WebResourceDefinition = {
  key: string;
};

export async function getPluginWebResourceDefinitions(
  pomFile: string,
  pomXml: PomXml,
): Promise<WebResourceDefinition[] | Error> {
  const generatedXmlPaths = await getGeneratedModuleDescriptorPaths(pomFile, pomXml);

  const wrmWebResources: WebResourceDefinition[] = [];

  for (const xmlFile of generatedXmlPaths) {
    const webResources = await getWebResourcesFromFile(xmlFile);

    if (webResources instanceof Error) {
      return webResources;
    }

    wrmWebResources.push(...webResources);
  }

  return wrmWebResources.sort((a, b) => a.key.localeCompare(b.key));
}

const WEB_RESOURCE_XML_KEY = 'web-resource';

async function getWebResourcesFromFile(xmlFile: string): Promise<WebResourceDefinition[] | Error> {
  const xml = await parseXmlFromFile(xmlFile);

  if (xml instanceof Error) {
    return xml;
  }

  const rootKey = Object.keys(xml)[0];
  const project = (xml[rootKey] as ParsedXmlWithAttrs[])[0];

  if (!(WEB_RESOURCE_XML_KEY in project)) {
    return [];
  }

  const webResourcesXml = project[WEB_RESOURCE_XML_KEY] as ParsedXmlWithAttrs[];

  return webResourcesXml.map<WebResourceDefinition>((webResourceXml) => {
    const webResource: WebResourceDefinition = {
      key: webResourceXml.attrs.key,
    };

    return webResource;
  });
}

export async function getGeneratedModuleDescriptorPaths(pomFile: string, pomXml: PomXml): Promise<string[]> {
  const pomScanFolders = getAbsoluteScanFoldersConfig(pomXml, pomFile);

  return getXmlFilesFromDirectory(pomScanFolders);
}

export async function removeOldModuleDescriptors(pomFile: string, pomXml: PomXml): Promise<void> {
  const generatedXmlPaths = await getGeneratedModuleDescriptorPaths(pomFile, pomXml);

  // Delete files in parallel
  await Promise.all(generatedXmlPaths.map<Promise<void>>((filePath) => silentlyDeleteFile(filePath)));
}

async function silentlyDeleteFile(filePath: string): Promise<void> {
  try {
    await fs.unlink(filePath);
  } catch (e) {
    // gracefully skip the error
  }
}

async function getXmlFilesFromDirectory(directory: string): Promise<string[]> {
  return await globWithOptions('**/*.xml', {
    cwd: directory,
    ignore: [], // Remove default ignored directories so we can scan within "target" directory
  });
}

export function getFullWebResourceKey(pluginKey: string, webResourceKey: string): string {
  return `${pluginKey}:${webResourceKey}`;
}
