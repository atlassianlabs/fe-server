export class MavenError extends Error {
  private readonly mavenOutput: string;

  constructor(message: string, mavenOutput: string) {
    super(message);

    this.mavenOutput = mavenOutput;
  }

  getMavenOutput(): string {
    return this.mavenOutput;
  }
}
