import cliTruncate from 'cli-truncate';
import terminalLink from 'terminal-link';

// Decrease the output size by 10% to avoid spanning the url link across full terminal width.
// We do that because a) the URLs can be quite long b) we indent the URLs when we display them in the terminal.
const maxColumns = process.stdout.columns - Math.floor(process.stdout.columns / 10);

export function cliLink(url: string, label: string = url): string {
  const shortLabel = cliTruncate(label, maxColumns, { position: 'middle' });

  return terminalLink(shortLabel, url, {
    fallback(text, url) {
      return url;
    },
  });
}
