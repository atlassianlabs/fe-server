// eslint-disable-next-line node/no-missing-require
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const path = require('path');

const outputPath = path.resolve(__dirname, './target/classes');
const xmlDescriptors = path.join(outputPath, './META-INF/plugin-descriptors/wr-bundles.xml');

module.exports = {
  entry: {
    'my-entrypoint': path.resolve(__dirname, './src/my-entrypoint.js'),
  },
  output: {
    path: outputPath,
  },
  plugins: [
    new WrmPlugin({
      pluginKey: 'some.valid.plugin.with-a-valid-artifact-id',
      xmlDescriptors,
    }),
  ],
};
