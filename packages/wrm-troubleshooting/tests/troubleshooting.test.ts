import getPort from 'get-port';
import ServerMock from 'mock-http-server';
import * as path from 'path';

import pkg from '../package.json';
import type { IWrmResource } from '../src/wrm/wrmTypes';
import { execute, getLastLine } from './helpers/cli';
import { compileProject } from './helpers/project';
import { configureFetchingWebResource } from './helpers/wrmRestApiHelper';

describe('Troubleshooting CLI integration tests', () => {
  const root = path.resolve(__dirname, '..');
  let server: ServerMock;
  let fakeApplicationUrl: string;

  // Create fake server
  beforeAll(async () => {
    const FAKE_SERVER_PORT = await getPort();
    const FAKE_SERVER_HOST = 'localhost';

    fakeApplicationUrl = `http://${FAKE_SERVER_HOST}:${FAKE_SERVER_PORT}`;
    server = new ServerMock({ host: FAKE_SERVER_HOST, port: FAKE_SERVER_PORT });

    console.debug('Compiling TS project...');
    await compileProject();
    console.debug('TS project compiled');

    console.debug('Starting mock HTTP server...');
    await new Promise((resolve) => {
      server.start(() => {
        console.debug(`[Mock HTTP Server]: Server started at ${FAKE_SERVER_HOST}:${FAKE_SERVER_PORT}`);

        resolve(void '');
      });
    });
  });

  afterAll(async () => {
    console.debug('Stopping mock HTTP server...');
    await new Promise((resolve) => server.stop(resolve));
    console.debug('Mock HTTP server stopped');
  });

  it('should return a CLI version', async () => {
    const response = await execute('./dist/cli.js', ['--version'], {
      cwd: root,
    });

    expect(response.trim()).toEqual(pkg.version);
  });

  it('should run all troubleshooting checks and pass them', async () => {
    // given
    const webpackConfigFile = path.resolve(__dirname, './cases/valid/webpack.config.js');
    const pomFile = path.resolve(__dirname, './cases/valid/pom.xml');

    const resourceKey = 'entrypoint-my-entrypoint';

    const webResources: IWrmResource[] = [
      {
        url: '/my-resource.js',
        resourceType: 'JAVASCRIPT',
        key: 'my-resource',
        batchType: 'resource',
      },
    ];

    configureFetchingWebResource(server, webResources);

    // when
    const args: string[] = [
      ['--webpack', webpackConfigFile],
      ['--pom', pomFile],
      ['--applicationUrl', fakeApplicationUrl],
      ['--resourceKey', resourceKey],
      ['--yes'], // Don't wait for user input when running CLI tool
      ['--timeout', '15'], // Let's use 15 seconds as default timeout
      ['--verbose'],
    ].flat();

    // then
    const output = await execute('./dist/cli.js', args, {
      cwd: root,
      env: {
        // We are forwarding the location of webpack so the webpack-cli command can find it
        // https://github.com/webpack/webpack-cli/blob/6a9aac99665f0d2f2f0c58c757c6befbc7734c8f/packages/webpack-cli/lib/webpack-cli.js#L8
        WEBPACK_PACKAGE: process.env.WEBPACK_PACKAGE as string,
      },
    });

    // Check WRM results
    expect(output).toContain(
      `We have managed to load the "${resourceKey}" web-resource with WRM runtime and the response resulted in ${webResources.length} resource(s)`,
    );

    // Check last message
    expect(getLastLine(output)).toMatch(
      "✅ We have finished all the checks and we haven't found any issues with your webpack or webpack WRM plugin configuration.",
    );
  });
});
