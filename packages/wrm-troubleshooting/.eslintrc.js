module.exports = {
  env: {
    browser: false,
    es6: true,
    node: true,
  },

  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module',
    ecmaFeatures: {
      modules: true,
    },
  },

  overrides: [
    // Node modules
    {
      files: ['*.js'],
      rules: {
        '@typescript-eslint/no-var-requires': 'off',
      },
    },
  ],

  extends: ['plugin:import/recommended', 'plugin:import/typescript', 'plugin:node/recommended'],
  rules: {
    // We disable those import rules since TS provide the same checks
    // https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/FAQ.md#eslint-plugin-import
    'import/named': 'off',
    'import/namespace': 'off',
    'import/default': 'off',
    'import/no-named-as-default-member': 'off',

    // This rules are super slow
    'no-implied-eval': 'off',
    '@typescript-eslint/no-implied-eval': 'off',

    // Enforce types import where possible
    '@typescript-eslint/consistent-type-imports': 'error',

    // TypeScript support
    'node/no-missing-import': [
      'error',
      {
        // https://github.com/mysticatea/eslint-plugin-node/issues/236#issue-655234174
        tryExtensions: ['.js', '.ts', '.d.ts'],
      },
    ],

    // Node ESLint
    'node/no-unsupported-features/es-syntax': [
      'error',
      {
        ignores: [
          // Modules are supported by TypeScript
          'modules',

          // Dynamic imports are supported by TypeScript
          'dynamicImport',
        ],
      },
    ],

    // We do use process.exit(<code>)
    'no-process-exit': 'off',

    // Keep hosting in order!
    'no-use-before-define': ['error', 'nofunc'],
  },
};
