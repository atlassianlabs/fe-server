const supportedMobileBrowsers = ['last 2 iOS version', 'last 2 ChromeAndroid version', 'last 2 Android version'];

module.exports = supportedMobileBrowsers;
