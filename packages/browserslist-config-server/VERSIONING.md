# Versioning

Effectively following semver where:

- removing support for a browser is considered major since a project maintainer must be aware in case they have to support older browsers
- adding support for a browser or version of a browsers is considered minor since this does not break functionality
- a patch is a patch
