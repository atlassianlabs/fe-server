# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### 0.3.2 (2024-04-05)

### Bug Fixes

- BSP-4677 updated the event ([dd2deba](https://bitbucket.org/atlassianlabs/fe-server/commits/dd2debace3221dcb31d85f6677ba9b0cf390ecdd))
- revomed wrongly added package-lock.json ([42d5f1a](https://bitbucket.org/atlassianlabs/fe-server/commits/42d5f1a64321527f6f28c41164a5d04213cf6779))

### 0.3.1 (2022-02-10)

**Note:** Version bump only for package @atlassian/browserslist-config-server

## [0.3.0](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/browserslist-config-server@0.3.0..@atlassian/browserslist-config-server@0.2.5) (2021-10-26)

### ⚠ BREAKING CHANGES

- Update to supported browsers for products in FY22

### Features

- Update to supported browsers for products in FY22 ([37eee2c](https://bitbucket.org/atlassianlabs/fe-server/commits/37eee2c1852836b67d04fb2690ef3fc62d3b60a2))

### [0.2.5](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/browserslist-config-server@0.2.5..@atlassian/browserslist-config-server@0.2.4) (2021-10-14)

**Note:** Version bump only for package @atlassian/browserslist-config-server

### [0.2.4](https://bitbucket.org/atlassianlabs/fe-server/branches/compare/@atlassian/browserslist-config-server@0.2.4..@atlassian/browserslist-config-server@0.2.3) (2021-09-27)

**Note:** Version bump only for package @atlassian/browserslist-config-server

### 0.2.3 (2021-09-24)

**Note:** Version bump only for package @atlassian/browserslist-config-server

# CHANGELOG

## 0.2.1 Empty change

- Changed version to test package publishing

## 0.2.0 Breaking change

- Remove mobile support from default list

## 0.1.0 Initial release

- Initial release with default, desktop, browser, and testing
