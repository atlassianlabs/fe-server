const pkg = require('./package.json');
const baseConfig = require('../../jest.config.js');

module.exports = {
  ...baseConfig,
  projects: ['.'],
  displayName: pkg.name,
  modulePathIgnorePatterns: ['/dist/'],
};
