const fs = require('fs');
const path = require('path');
const { spawn } = require('child_process');
const tmp = require('tmp');
const debounce = require('lodash/debounce');
const { getOptions } = require('loader-utils');
const { validate } = require('schema-utils');
const transformSoy = require('./transform-soy');
const optionsSchema = require('./optionsSchema.json');

const cwd = process.cwd();
const queues = new Map();

const compilerJarPath = require.resolve('@atlassian/soy-template-plugin-js/dist/jar/atlassian-soy-cli-support.jar');

const DEFAULT_OPTIONS = {
  expose: true,
};

const resolvePath = (filePath, cwd) => path.resolve(cwd, filePath);

const pathToFileUrl = (filePath, cwd) => `file:///${encodeURI(resolvePath(filePath, cwd))}`;

function _getJava() {
  return process.env.JAVA_HOME ? path.join(process.env.JAVA_HOME, '/bin/java') : 'java';
}

function _runProcess(cmd, args, debug, on_cmd_close) {
  if (debug) {
    console.log(`atlassian-soy-loader: Spawning ${cmd} ${args.join(' ')}`);
  }

  const spawned_cmd = spawn(cmd, args);
  let output = '';
  let errors = '';

  spawned_cmd.stderr.on('data', (data) => {
    errors += data;
  });

  spawned_cmd.stdout.on('data', (data) => {
    output += data;
  });

  spawned_cmd.on('close', () => {
    if (debug) {
      console.log('stdout:\n', output);
      console.log('stderr:\n', errors);
    }

    on_cmd_close(output, errors);
  });
}

// eslint-disable-next-line sonarjs/cognitive-complexity
function _processQueues() {
  for (const queue of queues.values()) {
    const items = queue.items.slice();

    queue.items = [];

    if (queue.debug) {
      console.log('Processing Soy queue of', items.length, 'items');
    }

    const tmpdir = tmp.dirSync();

    items.forEach((item, index) => {
      const tmpfile = path.join(tmpdir.name, `template${index}.soy`);

      fs.writeFileSync(tmpfile, item.source);
    });

    const args = [
      '-XX:+IgnoreUnrecognizedVMOptions',
      '--add-opens=java.base/java.lang=ALL-UNNAMED',
      '-jar',
      resolvePath(compilerJarPath, cwd),
      '--type',
      'js',
      '--basedir',
      resolvePath(tmpdir.name, cwd),
      '--glob',
      '*.soy',
      '--outdir',
      resolvePath(tmpdir.name, cwd),
      '--extension',
      'soy.js',
      '--use-ajs-i18n',
      '--use-ajs-context-path',
    ];

    if (queue.options.functions) {
      // We need to convert file path to file url actually
      args.push('--functions', pathToFileUrl(queue.options.functions, cwd));
    }

    if (queue.options.data) {
      args.push('--data', queue.options.data);
    }

    const javaExec = _getJava();

    _runProcess(javaExec, args, queue.debug, (output, errors) => {
      items.forEach((item, index) => {
        const soyFile = path.join(tmpdir.name, `template${index}.soy`);
        const jsFile = `${soyFile}.js`;

        // The Soy compiler will abort on the first file it fails to compile, so we can assume that the output
        // is the error for that source file
        if (/ERROR|Error|Exception/.test(output)) {
          item.callback(new Error(output + errors));
        } else {
          let compiled;

          try {
            compiled = fs.readFileSync(jsFile).toString();
          } catch (error) {
            item.callback(new Error(`Could not read ${jsFile}. Another file in this queue probably failed`));

            return;
          }

          const transformed = transformSoy({
            soySource: item.source,
            context: item.context,
            soyJs: compiled,
            options: queue.options,
          });

          item.callback(null, transformed);
        }
      });
    });
  }
}

const processQueues = debounce(_processQueues, 100);
/**
 * @this {import("webpack").loader.LoaderContext}
 */
function soyLoader(source) {
  const callback = this.async();

  this.cacheable();

  const options = Object.assign({}, DEFAULT_OPTIONS, getOptions(this));
  validate(optionsSchema, options, { name: 'soy-loader' });

  // SPFE-774: Deprecate the modularize option
  if (options.modularize) {
    // We emit error since that's what loader wants from us: https://webpack.js.org/api/loaders/#thisemitwarning
    this.emitWarning(new Error('The "modularize" option is deprecated and will be removed in the next major release.'));
  }

  // this is not entirely correct e.g. if there was function being passed
  const optionsString = JSON.stringify(options, (key, value) =>
    typeof value === 'function' ? value.toString() : value,
  );

  if (!queues.has(optionsString)) {
    // Don't store per-request information here...
    queues.set(optionsString, {
      debug: this.debug,
      options: options,
      items: [],
    });
  }

  const queue = queues.get(optionsString);

  // ...instead, store the request information per-item
  queue.items.push({
    source,
    callback,
    context: this.context,
  });

  processQueues();
}

module.exports = soyLoader;
