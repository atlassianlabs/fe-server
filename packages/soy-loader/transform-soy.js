const exposeSoy = require('./expose-soy');
const modularizeSoy = require('./modularize-soy');

const parseSoy = require('./parse-soy');

/**
 * @param {Object} params
 * @param {String} params.soySource - The original content of the soy template
 * @param {String} params.soyJs - A JS compiled version of the soy template
 * @param {String} params.context - The directory of the module. Can be used as a context for resolving other stuff
 * @param {Object} params.options
 * @param {Boolean} params.options.modularize - Check README.md
 * @param {Boolean} params.options.expose - Check README.md
 * @param {Object} params.options.customModuleMap - Check README.md
 * @param {Function} params.options.modulePathResolver - Check README.md
 * @return {string} - A JS version of soy template
 */
function transformSoy({ soySource, soyJs, context, options }) {
  const parsedSoy = parseSoy(soySource, { namespaceOnly: !options.modularize });

  if (options.modularize) {
    soyJs = modularizeSoy({ parsedSoy, soyJs, context, options });
  } else {
    soyJs += `
        module.exports = ${parsedSoy.namespace};`;
  }
  if (options.expose) {
    soyJs = exposeSoy(parsedSoy, soyJs);
  }

  return soyJs;
}

module.exports = transformSoy;
