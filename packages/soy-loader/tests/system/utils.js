const { readFileSync, writeFileSync } = require('fs');

function modifyPackageJson(packageJsonFilepath, modifyOperation) {
  let testPackageJsonContents = JSON.parse(readFileSync(packageJsonFilepath, 'utf8'));
  modifyOperation(testPackageJsonContents);
  writeFileSync(packageJsonFilepath, JSON.stringify(testPackageJsonContents, null, 2));

  return testPackageJsonContents;
}

module.exports = {
  modifyPackageJson,
};
