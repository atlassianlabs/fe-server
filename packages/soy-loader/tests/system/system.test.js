const { exec } = require('child_process');
const { existsSync } = require('fs');
const { mkdir, mkdtemp, writeFile } = require('fs').promises;
const { copy } = require('fs-extra');
const { tmpdir } = require('os');
const path = require('path');
const rimraf = require('rimraf');
const { promisify } = require('util');
const { runServer } = require('verdaccio');
const { getVerdaccioConfig } = require('./verdaccio-config');
const { modifyPackageJson } = require('./utils');

const execAsync = promisify(exec);

const shouldSkip = process.env.SKIP_SOY_LOADER_SYSTEM_TESTS === 'true';

(shouldSkip ? describe.skip : describe)('soy-loader system', () => {
  const packageJsonFilename = 'package.json';
  let sandboxDir = undefined;
  let serverInstance = undefined;
  const serverPort = 4873;
  let packagePublishedVersion = undefined;

  beforeAll(async () => {
    const sourcePackageDir = path.resolve(__dirname, '..', '..');
    const sourcePackageJsonFilepath = path.resolve(sourcePackageDir, packageJsonFilename);
    if (!existsSync(sourcePackageJsonFilepath)) {
      throw new Error(`Unable to find ${sourcePackageJsonFilepath}. CWD=${process.cwd()}`);
    }

    await createSandbox();
    const publishPackageDir = path.resolve(sandboxDir, 'package');
    await copy(sourcePackageDir, publishPackageDir);
    await rimraf.rimraf(path.join(publishPackageDir, '.npmrc'));
    await rimraf.rimraf(path.join(publishPackageDir, 'node_modules'));

    let testPackageJsonContents = modifyPackageJson(
      path.resolve(publishPackageDir, packageJsonFilename),
      (contents) => {
        contents.publishConfig['registry'] = `http://localhost:${serverPort}/`;
        contents.version = `${contents.version}-test`;
      },
    );

    if (testPackageJsonContents.name !== '@atlassian/soy-loader') {
      throw new Error(`Invalid package (${testPackageJsonContents.name})!`);
    }

    console.log('Starting NPM registry...');
    const config = getVerdaccioConfig(sandboxDir);
    await mkdir(config.storage, { recursive: true });
    serverInstance = await runServer(config);
    serverInstance.listen(serverPort);
    const address = serverInstance.address();
    console.log(`Verdaccio server running at ${address.address}, ${address.family}, ${address.port}`);

    const { stderr, stdout } = await execAsync('npm publish', getSandboxExecOptions(publishPackageDir));
    packagePublishedVersion = testPackageJsonContents.version;
    console.log(['Package published:', `stdout:${stdout}`, `stderr:${stderr}`].join('\n'));
  }, 1 * 60 * 1000);

  afterAll(() => {
    if (process.env.CONTINUE_RUNNING_VERDACCIO === 'true') {
      console.log(
        'Keeping Verdaccio server running...\nTo use the .npmrc configured for it use:\nexport HOME=${sandboxDir}',
      );
      return;
    }

    return stopServer();
  });

  it.each(['test-no-webpack-install', 'test-webpack4-install', 'test-webpack5-install'])(
    'should install within %p',
    async (testName) => {
      const testDir = await prepareInstallTest(testName);

      const { stdout } = await execAsync('npm install && node hello-mike.js', getSandboxExecOptions(testDir));
      expect(stdout).toContain('<div>Hello Mike</div>');
    },
    5 * 60 * 1000,
  ); // The test usually run in a bit over a minute but this may need to be extended on mobile etc

  it.each(['test-webpack3-install'])(
    'should NOT install within %p',
    async (testName) => {
      const testDir = await prepareInstallTest(testName);
      let installError = undefined;
      let installCompleted = undefined;

      try {
        const { stderr, stdout } = await execAsync('npm install', getSandboxExecOptions(testDir));
        installCompleted = true;
        console.log(`Unexpected success:\nstdout: ${stdout}\nstderr: ${stderr}`);
      } catch (error) {
        installCompleted = false;
        installError = error;
      }

      expect(installCompleted).toBe(false);
      expect(installError).toBeDefined();
      expect(installError.stderr).toMatch(/ERESOLVE unable to resolve dependency tree/);
      expect(installError.stderr).toMatch(/peer webpack/);
    },
    1 * 60 * 1000,
  ); // The test usually run in a bit over a minute but this may need to be extended on mobile etc

  async function prepareInstallTest(testName) {
    expect(packagePublishedVersion).toBeTruthy();

    const testDir = getTestDir(testName);

    // Update package-under-test version
    modifyPackageJson(path.resolve(testDir, packageJsonFilename), (packageJsonContents) => {
      packageJsonContents.dependencies['@atlassian/soy-loader'] = packagePublishedVersion;
    });

    // Copy additional files needed for hello-mike.js sanity tests
    await copy(path.resolve(testDir, '..', 'common'), testDir);
    await copy(path.resolve(testDir, '..', '..', 'helpers.js'), path.resolve(testDir, 'helpers.js'));
    await copy(path.resolve(testDir, '..', '..', 'soy-runtime.js'), path.resolve(testDir, 'soy-runtime.js'));

    return testDir;
  }

  function stopServer() {
    expect(sandboxDir).toBeDefined();

    return new Promise((resolve, reject) => {
      if (serverInstance && serverInstance.close) {
        serverInstance.close(() => {
          removeSandbox();
          serverInstance = undefined;
          resolve('Verdaccio server has been stopped.');
        });
      } else {
        reject('Server instance is not running or lacks a close method.');
      }
    });
  }

  function getSandboxExecOptions(cwd) {
    expect(sandboxDir).toBeDefined();

    const environment = Object.assign({}, process.env);
    environment.HOME = sandboxDir;
    Object.keys(environment).forEach((key) => {
      if (key.startsWith('npm_') || key.startsWith('PAC_')) {
        delete environment[key];
      }
    });

    return {
      stdio: 'inherit',
      cwd: cwd,
      env: environment,
    };
  }

  async function createSandbox() {
    expect(sandboxDir).toBeUndefined();
    sandboxDir = await mkdtemp(path.resolve(tmpdir(), 'soy-loader-system-tests-'));
    console.log(`Creating sandbox: ${sandboxDir} ...`);

    await rimraf.rimraf(sandboxDir);
    await mkdir(sandboxDir, { recursive: true });
    await writeFile(
      path.resolve(sandboxDir, '.npmrc'),
      [
        `registry=http://localhost:${serverPort}/`,
        `@atlassian:registry=http://localhost:${serverPort}/`,
        `//localhost:${serverPort}/:_authToken="Ensure npm publish will carry the auth token as required"`,
      ].join('\n'),
    );
  }

  function removeSandbox() {
    expect(sandboxDir).toBeDefined();

    console.log(`Removing sandbox: ${sandboxDir} ...`);
    rimraf.sync(sandboxDir);
  }

  function getTestDir(testName) {
    expect(sandboxDir).toBeDefined();

    return path.resolve(sandboxDir, 'package', 'tests', 'system', testName);
  }
});
