# Overview

This is a collection of system level tests

They include publishing the soy-loader package to a local npm registry and running the tests from it

# Running

To run the tests issue the following command in fe-server repo root:

```
npm run test system
```

# Skipping

To skip these tests and improve turnaround time when running other tests please set the `SKIP_SOY_LOADER_SYSTEM_TESTS` environment variable to `true`:

```
env SKIP_SOY_LOADER_SYSTEM_TESTS=true npm run test
```

You can do the same by using the ease of life helper in fe-server:

```
npm run test:only:ut
```

# Debugging

To debug some of the tests manually you can keep the local npm repository and the sandbox around via:

```
env CONTINUE_RUNNING_VERDACCIO=true npm run test system --verbose --silent=false
```

Then in some other shell you can run the tests manually by setting HOME to the sandbox dir and issuing the commands you need, ie:

```
export HOME=SOME-SANDBOX-DIR
cd ~/package/tests/system/TEST-NAME
npm install
node hello-mike.js
```

Please make sure to clean the sandbox dir when you're done.
