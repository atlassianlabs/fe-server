// This file is used for sanity tests of the soy-loader in various system-testing scenarios
// To reproduce the test manually check the debugging section of [README.md](../../README.md)
// Please note that this part gets copied into the specific test sandboxes and executed there
// so the paths are relative that sandbox as it gets created and may not work in the context of the package itself

const { bundleEntryPoint } = require('./helpers');
const path = require('path');

const testSuiteDir = path.basename(__dirname);
const getWebpackConfig = require('./webpack.config');

(async () => {
  try {
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: '../module.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({
      name: 'Mike',
    });

    console.log(result);
  } catch (e) {
    console.error(e);
  }
})();
