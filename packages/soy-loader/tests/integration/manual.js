// This file is used to manually test the loader in a real-world scenario and be able to see the full debug output (`debug` option in webpack.config.js).
// You can run the manual test using `node tests/integration/manual.js` or `node packages/soy-loader/tests/integration/manual.js` commands depending on your location.
// This file is not part of the test suite.

const { bundleEntryPoint } = require('../helpers');
const path = require('path');

const testSuiteDir = path.basename(__dirname);
const getWebpackConfig = require('./webpack.config');

(async () => {
  try {
    const { modulePath } = await bundleEntryPoint(getWebpackConfig(), {
      entrypointPath: './src/simple/module.js',
      testSuiteDir,
    });
    const { template } = require(modulePath);

    // when
    const result = template({
      name: 'Mike',
    });

    console.log(result);
  } catch (e) {
    console.error(e);
  }
})();
