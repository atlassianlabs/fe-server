const webpack = require('webpack');
const path = require('path');

const runWebpack = (config) => {
  const compiler = webpack(config);

  return new Promise((resolve, reject) => {
    compiler.run((err, stats) => {
      if (err) {
        reject(err.toString());
        return;
      }

      if (stats.hasErrors()) {
        reject(
          stats
            .toJson()
            .errors.map((err) => {
              // In webpack 4 error is only a string but in webpack 5 we have more details information
              return typeof err === 'string' ? err : `Webpack error: ${err.message}\n\n${err.details}`;
            })
            .join('\n'),
        );
        return;
      }

      resolve(stats);
    });
  });
};

/**
 * Bundles given module and return bundled path and bundling stats
 *
 * @param {Object} webpackConfig - webpack configuration object
 * @param {Object} options
 * @param {string} options.entrypointPath - a path to the entrypoint JS module (with extension)
 * @param {string} options.testSuiteDir - a name of the test suite directory
 * @returns {Promise<{ modulePath: string, stats: Object }>} - Returns a relative path to a bundled module (without the JS extension) and
 *                                                             webpack bundling stats object
 */
const bundleEntryPoint = async (webpackConfig, { entrypointPath, testSuiteDir }) => {
  const entryPointName = Date.now().toString(32);

  webpackConfig.entry[entryPointName] = path.join(__dirname, testSuiteDir, entrypointPath);

  const stats = await runWebpack(webpackConfig);
  const bundledModulePath = path.join(testSuiteDir, 'dist', entryPointName);

  return {
    stats,
    modulePath: `../${bundledModulePath}`,
  };
};

module.exports = {
  bundleEntryPoint,
};
