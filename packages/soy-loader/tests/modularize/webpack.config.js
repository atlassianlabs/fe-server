const path = require('path');

const ROOT = path.resolve(__dirname);
const SRC = path.join(ROOT, './src');
const DIST = path.join(ROOT, './dist');

const soyLoader = path.resolve(ROOT, '../../index');

module.exports = () => ({
  devtool: false,
  mode: 'development',
  entry: {},

  output: {
    path: DIST,
    filename: '[name].js',
    libraryTarget: 'commonjs2',
  },

  resolve: {
    modules: [SRC],
  },

  module: {
    rules: [
      {
        test: /\.soy/,
        include: [SRC],
        use: [
          {
            loader: soyLoader,
            options: {
              expose: true,
              modularize: true,
              customModuleMap: {
                'test.custom.mapping': 'custom-mapping',
                'test.custom.mapping.global': false,
              },
              modulePathResolver: (namespace) => {
                switch (namespace) {
                  case 'test.module.path':
                    return 'module-path';
                  case 'test.module.path.global':
                    return false;
                  default:
                    return null;
                }
              },
            },
          },
        ],
      },
    ],
  },
});
