# Development

All guides in the document requires having a proper Node and NPM versions installed in your local environment.

You can use the [`nvm`][nvm] command to install the required version of Node.

The monorepo is using:

- [`lerna` tooling][lerna]
- [Conventional Commits][conventional-commits]

You should get familiar with those tools before doing any changes in the repository or releases.

## Releasing

All packages in the monorepo are being published as independent versions into a public [NPM registry](https://registry.npmjs.org).

> ## WARNING! Don't try to release the changes from your local environment!

Use the Bitbucket Pipelines to prepare and perform the release from the `master` branch.

> ## WARNING! Before running a release read the next paragraphs!

### Pre-release step

Before we publish the packages to NPM we need bump the versions and update the `CHANGELOG.md` files first. This is done automatically when you run a release steps on Pipelines.

We will run a **Pre-release** step before publishing. This step will show you proposed versions that are based on the GIT commits' history. The commits are following the [Conventional Commits][conventional-commits] conventions.

This means that if you want to release, e.g. a new major version that is a breaking change, you need to follow the conventional commits and use e.g. this commit message:

```text
feat!: drop support for Node 12

BREAKING CHANGE: We are dropping support for Node 12. Please use Node 14.
```

## Release step

During the release step **lerna** will do all those steps:

- update changelog files
- update versions in `package.json` files
- commits the GIT changes
- create GIT tags
- push changes and tags back to a `master` branch
- deploy the new versions to public NPM

## Post-release step

As the **Post-release** step will run a smoke test to verify if the packages were successfully published into public NPM.

This check ensures we haven't accidentally published any packages into the private Atlassian repository at http://packages.atlassian.com page.

## Performing a release with Pipelines

To perform a release:

1. Merge all your changes you want to release into the `master` branch and ensure that the builds are not failing.
2. Go to [Pipelines](https://bitbucket.org/atlassianlabs/fe-server/addon/pipelines/home), from the branches' dropdown select the `master` branch.
3. Now, locate the **Pre-release** manual step and run it. The Pipeline will display you what versions will be released.
4. Next, if the proposed versions are correct, run the **Release** step.
5. Lastly, during the **Post-release** step, the Pipelines will verify if the packages were successfully published into public NPM.

## Quirks

Ensure to always keep "webpack" versions in sync across all packages. This is necessary as otherwise tests might start to fail as packages that rely on each other will resolve different versions of webpack which causes compatibility issues.

[nvm]: https://github.com/nvm-sh/nvm
[lerna]: https://github.com/lerna/lerna
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/
[semver]: https://docs.npmjs.com/about-semantic-versioning
