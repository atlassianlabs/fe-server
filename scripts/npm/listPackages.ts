import access from 'libnpmaccess';

export enum PermissionType {
  READ_ONLY = 'read-only',
  READ_WRITE = 'read-write',
}

export type NpmPackages = Map<string, PermissionType>;

/**
 * Show all the packages a user or a team is able to access, along with the access level,
 * except for read-only public packages (it won't print the whole registry listing)
 *
 * https://docs.npmjs.com/cli/v7/commands/npm-access see "ls-packages"
 */
export async function listPackages(username: string): Promise<NpmPackages> {
  // Don't use the authorization token here since we are interested in publicly visitable packages only
  const response = await access.lsPackages(username);

  if (!response) {
    throw new Error("Couldn't retrieve any NPM package from the public NPM registry");
  }

  return new Map(Object.entries(response));
}
