import npmFetch from 'npm-registry-fetch';

export interface WhoAmIResponse extends Record<string, string> {
  username: string;
}

export async function getWhoAmI(token: string): Promise<WhoAmIResponse> {
  // Enforce authorization with a token
  const opts: npmFetch.Options = {
    forceAuth: {
      alwaysAuth: true,
      token,
    },
  };

  const response = await npmFetch.json('/-/whoami', opts);

  return response as WhoAmIResponse;
}
