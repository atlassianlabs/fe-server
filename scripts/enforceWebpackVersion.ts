// This check ensures we only ever have a single webpack version as resolution within the monorepo.
// Having multiple versions of webpack could lead to quirks between the packages.

import * as process from 'process';
import { $, cd, chalk } from 'zx';

import type { NpmLs } from './types/npm';

// Disable verbose output
$.verbose = false;

const retrieveWebpackDeps = async () => {
  try {
    // npm ls might fail but still contain the valid output
    // both the error thrown and the valid response contain what we need in `stdout`
    // we therefor just throw whatever we receive if it doesnt fail by itself and handle "everything" in catch
    cd('../');
    const { stdout } = await $`npm ls webpack --json`;
    const { dependencies } = JSON.parse(stdout as string) as NpmLs;
    return dependencies;
  } catch ({ stderr }) {
    console.error(chalk.red(`❌ Failed to run "npm ls webpack" make sure your dependencies are up-to-date.`));
    console.log(stderr);
    process.exit(1);
  }
};

const dependencies = await retrieveWebpackDeps();

console.log(chalk.bold('Verifying that all packages depend on the same webpack version…'));

const webpackVersions = new Set<string>();

for (const [name, pkg] of Object.entries(dependencies)) {
  const { webpack } = pkg.dependencies;
  if (!webpack) {
    console.log(`Package "${name}" does not have a dependency on webpack. Skipping…`);
  } else {
    console.log(`Package "${name}" depends on version ${webpack.version} of webpack.`);
    webpackVersions.add(webpack.version);
  }
}

if (webpackVersions.size > 1) {
  console.error(
    `❌ More than one webpack version found across packages. Please dedupe! (${Array.from(webpackVersions).join(
      ', ',
    )}).`,
  );
  process.exit(1);
}

console.log(`✅ All packages use the same webpack version (${Array.from(webpackVersions)[0]}).`);
