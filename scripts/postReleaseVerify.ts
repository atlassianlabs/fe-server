import * as process from 'process';
import { $, chalk } from 'zx';

import type { LernaPackageInfo } from './types/lernaCmd';

// Disable verbose output
$.verbose = false;

const { stdout: rawOutput } = await $`npx lerna list --json --no-private`;
const packages: LernaPackageInfo[] = JSON.parse(rawOutput) as LernaPackageInfo[];

console.log(chalk.bold('Verifying if all packages were successfully published to a public NPM registry...'));

let hasError = false;

for await (const pkg of packages) {
  try {
    const { stdout: packageVersionRaw } = await $`npm info ${pkg.name}@latest version --no-color`;
    const { stdout: registryLocation } = await $`npm info ${pkg.name}@latest dist.tarball --no-color`;
    const packageVersion = packageVersionRaw.trim();

    if (!registryLocation.includes('https://registry.npmjs.org/')) {
      throw new Error(
        `The latest version of "${pkg.name}" package can't be found in a public NPM registry. Please ensure you are using valid Node/NPM config to access public NPM registry (current registry "${registryLocation}").`,
      );
    }

    if (packageVersion !== pkg.version) {
      throw new Error(
        `The latest version of "${pkg.name}" package (${packageVersion}) does not match the requested version (${pkg.version})`,
      );
    }

    console.log(
      chalk.green(`✅ Package "${chalk.bold(pkg.name)}" version ${chalk.bold(pkg.version)} can be found in public NPM`),
    );
  } catch (error) {
    hasError = true;
    console.log(
      chalk.red(
        `❌ Package "${chalk.bold(pkg.name)}" version ${chalk.bold(
          pkg.version,
        )} can't be found in a public NPM registry`,
      ),
    );

    console.log(error);
  }
}

if (hasError) {
  process.exit(1);
}

console.log('Done.');
