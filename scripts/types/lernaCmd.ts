export interface LernaPackageInfo {
  name: string;
  version: string;
  private: boolean;
  location: string;
}
