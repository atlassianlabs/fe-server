declare module 'libnpmaccess' {
  /**
   * libnpmaccess uses npm-registry-fetch. All options are passed through directly to that library, so please refer to its own opts documentation for options that can be passed in.
   *
   * A couple of options of note for those in a hurry:
   * opts.token - can be passed in and will be used as the authentication token for the registry. For other ways to pass in auth details, see the n-r-f docs.
   * opts.otp - certain operations will require an OTP token to be passed in. If a libnpmaccess command fails with err.code === EOTP, please retry the request with {otp: <2fa token>}
   */
  export interface NpmRegistryFetchOpts {
    token?: string;
    otp?: string;
  }

  export type LsPackagesResponse = Record<string>;

  /**
   * Lists out packages a user, org, or team has access to, with corresponding permissions. Packages that the access token does not have access to won't be listed.
   *
   * @param entity - entity must be either a valid org or user name, or a fully-qualified team name in the scope:team format, with or without the @ prefix.
   * @param opts - @see Opts
   */
  export function lsPackages(entity: string, opts?: NpmRegistryFetchOpts): Promise<LsPackagesResponse | null>;
}
