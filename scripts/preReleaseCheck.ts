import * as process from 'process';
import { $, chalk } from 'zx';

import type { NpmPackages } from './npm/listPackages';
import { listPackages, PermissionType } from './npm/listPackages.js';
import { getWhoAmI } from './npm/whoAmI.js';
import type { LernaPackageInfo } from './types/lernaCmd';

// Disable verbose output
$.verbose = false;

const BOT_ACCOUNT_USERNAME = 'sfp-release-bot';
const NPM_RELEASE_BOT_TOKEN = process.env.NPM_RELEASE_BOT_TOKEN;

if (!NPM_RELEASE_BOT_TOKEN) {
  console.error(
    chalk.red(
      `❌ The environmental variable "NPM_RELEASE_BOT_TOKEN" was not provided or it's empty. Ensure the valid "NPM_RELEASE_BOT_TOKEN" variables is exposed.`,
    ),
  );

  process.exit(1);
}

console.log(
  chalk.bold('Verifying if all packages can be published to a public NPM registry using automation token...'),
);

// Get list of our packages in the repository
const { stdout: rawOutput } = await $`npx lerna list --json --no-private`;
const packages: LernaPackageInfo[] = JSON.parse(rawOutput) as LernaPackageInfo[];

// Verify if we are using a correct automation token
let npmUsername: string;

try {
  const user = await getWhoAmI(NPM_RELEASE_BOT_TOKEN);

  npmUsername = user.username;
} catch (e) {
  console.error(
    chalk.red(
      `❌ Can't determine the NPM username based on the access token. Ensure the provided automation token is valid.`,
    ),
    e,
  );
  process.exit(1);
}

if (npmUsername !== BOT_ACCOUNT_USERNAME) {
  console.error(
    chalk.red(
      `❌ The automation token does not belong to the "${BOT_ACCOUNT_USERNAME}" NPM account but to the "${npmUsername}". Unsure that provided automation token is belongs to the "${BOT_ACCOUNT_USERNAME} account.`,
    ),
  );
  process.exit(1);
}

// Get list of all packages we can publish using the automation token account
let npmPackages: NpmPackages;

try {
  npmPackages = await listPackages(npmUsername);
} catch (e) {
  console.error(
    chalk.red(`❌ We couldn't find any NPM packages in public NPM registry using the automation token.`),
    e,
  );
  process.exit(1);
}

let hasError = false;

for (const pkg of packages) {
  try {
    // https://docs.npmjs.com/cli/v7/commands/npm-access
    if (npmPackages.has(pkg.name) && npmPackages.get(pkg.name) !== PermissionType.READ_WRITE) {
      throw new Error(
        `The given automation token can't be used to publish "${pkg.name}" NPM package to a public NPM registry. Ensure that "${npmUsername}" NPM username has required permissions to publish this package.`,
      );
    }

    console.log(chalk.green(`✅ Package "${chalk.bold(pkg.name)}" can be published to a public NPM registry`));
  } catch (error) {
    hasError = true;
    console.error(chalk.red(`❌ Package "${chalk.bold(pkg.name)}" can't be published to a public NPM registry`));

    console.error(error);
  }
}

if (hasError) {
  process.exit(1);
}

console.log('Done.');
