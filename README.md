# Atlassian Front-End Server Monorepo

A monorepo for the utilities and libraries that can help with building modern front-end projects for Atlassian DC and Server products:

- [@atlassian/i18n-properties-loader](./packages/i18n-properties-loader)
- [@atlassian/soy-loader](./packages/soy-loader)
- [@atlassian/soy-template-plugin-js](./packages/soy-template-plugin-js)
- [@atlassian/wrm-react-i18n](./packages/wrm-react-i18n)
- [@atlassian/wrm-troubleshooting](./packages/wrm-troubleshooting)
- [@atlassian/browserslist-config-server](./packages/browserslist-config-server)
- [atlassian-webresource-webpack-plugin](./packages/webresource-webpack-plugin)
- [eslint-plugin-atlassian-wrm-i18n](./packages/eslint-plugin-atlassian-wrm-i18n)

## Help and Support

If you have found a bug, or you have any other issue related to any of those NPM packages you can [report the issue in Atlassian Ecosystem issue tracker][issuetracker].

Click on the "**Create**" issue button and provide the reproduction steps. Please include as much information as you can, including:

- DC/Server application name and version
- A name and a version of the package you are using
- Browser vendor and/or Node version you are using

### Developer Community

You can also ask a question on [Developer Community Portal][cdac] under the **Atlassian Developer Tools** category.

## Development

This monorepo is using:

- [`nvm`][nvm]
- [`lerna` tooling][lerna]
- [Conventional Commits][conventional-commits]

You should get familiar with those tools before doing any changes in the repository.

## Releasing

Refer to [development](./DEVELOPMENT.md) document to learn more about development and releasing process.

[nvm]: https://github.com/nvm-sh/nvm
[lerna]: https://github.com/lerna/lerna
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/
[issuetracker]: https://ecosystem.atlassian.net/jira/software/c/projects/SPFE/issues
[cdac]: https://community.developer.atlassian.com/ 'Developer Community Portal'
