const config = require('./jest.config');

module.exports = {
  ...config,
  silent: false,
  // configure Jest to produce coverage and execution reports
  collectCoverage: true,
  // this will make sure to collect coverage from all file, even if they don't have any test
  // https://jestjs.io/docs/en/configuration.html#collectcoveragefrom-array
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}',
    '!**/__tests__/**',
    '!**/dist/**',
    '!**/node_modules/**',
    '!**/target/**',
    '!jest.config.js',
    '!scripts/**', // Ignore the scripts director completely
  ],
  maxWorkers: 2,
  coverageDirectory: '<rootDir>/coverage/jest',
  coveragePathIgnorePatterns: ['/webresource-vite-plugin/'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        // https://support.atlassian.com/bitbucket-cloud/docs/test-reporting-in-pipelines/
        outputDirectory: './test-reports/',
        outputName: 'jest-coverage-report.xml',
      },
    ],
  ],
};
